<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Configuration options
$config['stripe_test_mode']   = TRUE;
$config['stripe_key_public']  = ( $config['stripe_test_mode']  == TRUE ) ? 'pk_test_9Z8bZKnvOEUtSp5YZTh5uoxn' : 'pk_live_z4mwX34CBzWaYe3bgbmu7z33';
$config['stripe_key_secret']  = ( $config['stripe_test_mode']  == TRUE ) ? 'sk_test_XlkK9owZKHEIlzjMKJeijZpw' : 'sk_live_XQmYY7eHVNTIYvKzOMoae1EF';
$config['stripe_verify_ssl']  = FALSE;
