 <?php
defined('BASEPATH') OR exit('No direct script access allowed');


$config['USR001'] = 'Email required';
$config['USR002'] = 'Confirm Password doesnot match Password field';
$config['USR003'] = 'There was a problem creating new account.';
$config['USR004'] = 'Email address is not registered.';
$config['USR005'] = 'Incorrect Password';
$config['USR006'] = 'No admin privileges.';
$config['USR007'] = 'Problem deleting user.';
$config['USR008'] = 'User not found.';
$config['USR009'] = 'Problem listing users.';
$config['USR010'] = 'You don\'t have permission.';
$config['USR011'] = 'There was a problem while updating.';
$config['USR012'] = 'Unable to send the reset token.';
$config['USR013'] = 'Unable to update the password.';
$config['USR014'] = 'reset token did not match.';
$config['USR015'] = 'Email address already exists.';
$config['USR016'] = 'Invalid Email';
$config['USR017'] = 'Password required';
$config['USR018'] = 'Password field length must be greater than 8 characters';
$config['USR019'] = 'Confirm Password field is required';
$config['USR020'] = 'Congratulation! Your account has been created.';
$config['USR021'] = 'Password Updated';
$config['USR022'] = 'Profile Updated';
$config['USR023'] = 'User Deleted';
$config['USR024'] = 'Reset Token sent to email';
$config['USR025'] = 'Required Reset Token';

$config['USR026'] = 'User updated';
$config['USR027'] = 'User not updated';

/* saved token, link with customer and plan(stripe) and saved in db*/
$config['USR028'] = 'Subscribed sucessfully';
$config['USR029'] = 'Unable to subscribed';

/* in db*/
$config['USR030'] = 'Unable to save token';

/* creating customer with relation to token*/
$config['USR031'] = 'Invalid credentials';


/* assign customer with plan ( stripe) */
$config['USR032'] = 'Unable to assign user to plan';

$config['USR033'] = 'User listed successfully';

$config['USR034'] = 'Your account has not been deactivated by administrator.';
$config['USR035'] = 'Logged in successfully';
$config['USR036'] = 'User is activated';

$config['USR037'] = 'Please check your email for verfication token.';

$config['USR038'] = 'unable to Registration email';
$config['USR039'] = 'User is Inactived';

$config['USR040'] = 'Password Changed Successfully.';
$config['USR041'] = 'Old password mis matched.';

$config['USR042'] = 'User is deactivated';
$config['USR043'] = 'Staff Created Successfully.';

$config['USR044'] = 'User Email is verified';
$config['USR045'] = 'Unable to verified email. Please Try again';
$config['USR046'] = 'Token mismatch';
$config['USR047'] = 'Opps..looks like your email address is not verified';

$config['USR048'] = 'Verification code not matched';

$config['USR049'] = 'Verification code cannot be empty';

$config['USR050'] = 'User not logged in';
$config['USR051'] = 'Verification code is sent';
$config['USR052'] = 'Unable to sent verification code ';

$config['USR053'] = 'Paypal payment approved';
$config['USR054'] = 'Paypal payment not approved';

$config['USR055'] = 'Staff account has created';

$config['USR056'] = 'Payment has already been done';


$config['MM001'] = 'MM Address List';
$config['MM002'] = 'MM Address not listed';

$config['MM003'] = 'MM Address is saved';
$config['MM004'] = 'MM Address not saved';

$config['MM005'] = 'MM Address deleted';
$config['MM006'] = 'MM Address not deleted';

$config['MM007'] = 'MM Address ID missing';

$config['MM008'] = 'Permission denied';

$config['MM009'] = 'MM Address is already assigned';


$config['SRV001'] = 'There was a problem creating service.';
$config['SRV002'] = 'You don\'t have permission.';
$config['SRV003'] = 'Service not found.';
$config['SRV004'] = 'There was a problem while updating.';
$config['SRV005'] = 'There was a problem deleting service.';
$config['SRV006'] = 'There was a problem while listing services.';

$config['SRV007'] = 'Title field is required';
$config['SRV008'] = 'Description field is required';
$config['SRV009'] = 'Cost field is required';
$config['SRV010'] = 'Note field is required';
$config['SRV011'] = 'Service Added Successfully.';
$config['SRV012'] = 'Service deleted successfully.';
$config['SRV013'] = 'Service updated successfully.';
$config['SRV014'] = 'Invalid form data.';
$config['SRV015'] = 'Unable to delete.';

$config['MSG001'] = 'Message Sent';
$config['MSG002'] = 'Message not sent';
$config['MSG003'] = 'Message Listed';
$config['MSG004'] = 'Unable to list';

$config['MSG005'] = 'Message Deleted';
$config['MSG006'] = 'Message not deleted';
$config['MSG007'] = 'Message Id not exists';

$config['MSG008'] = 'Staff Id is required';
$config['MSG009'] = 'Attachment Field Id is required';
$config['MSG010'] = 'Message Field s required';
$config['MSG011'] = 'Status Field is required';
$config['MSG012'] = 'Customer Id is required';

$config['MSG013'] = 'Message is seen';

$config['MSG014'] = 'Unseen message count generated';
$config['MSG015'] = 'Unable to generate';

$config['MSG016'] = 'Message Thread fetch';
$config['MSG017'] = 'Unable to fetch message threading';
$config['MSG018'] = 'Group ID is missing';


$config['ATT001'] = 'Attachments uploaded';
$config['ATT002'] = 'Validation Error(s)';

$config['ATT003'] = 'Attachment is deleted ';
$config['ATT004'] = 'Attachment not deleted ';

$config['ATT005'] = 'Attachment Id not found ';
$config['ATT006'] = 'Attachments Listed';
$config['ATT007'] = 'Unable to list Attachment';

$config['ATT008'] = 'User ID is required';
$config['ATT009'] = 'Is Paid field is required';
$config['ATT010'] = 'File field is required';

$config['ATT011'] = 'File Upload Failed';
$config['ATT012'] = 'Attachments not saved in db';

$config['ATT013'] = 'Invalid Attachments file';

$config['ATT014'] = 'Attachment not found. Please contact Administrator';


$config['OPT001'] = 'New Setting Added';
$config['OPT002'] = 'No Settings were change';

$config['OPT003'] = 'Setting removed';
$config['OPT004'] = 'Unable to delete setting';

$config['OPT005'] = 'Setting key not found';

$config['OPT006'] = 'Settings Listed';
$config['OPT007'] = 'Unable to list setting';

$config['OPT008'] = 'Setting Key name is required';
$config['OPT009'] = 'Setting value is required';

$config['OPT010'] = 'Setting updated';


$config['ACT001'] = 'Activity added';
$config['ACT002'] = 'Activity not added';

$config['ACT003'] = 'Activity is deleted ';
$config['ACT004'] = 'Activity not deleted ';

$config['ACT005'] = 'Activity Id not found ';
$config['ACT006'] = 'Activity Listed';
$config['ACT007'] = 'Unable to list Activity';

$config['ACT008'] = 'User ID is required';
$config['ACT009'] = 'Is Paid field is required';
$config['ACT010'] = 'File field is required';



$config['STH001'] = 'Something went wrong';

$config['REQ001'] = " This Field is required";

$config['SUB001'] = 'Subscription saved';
$config['SUB002'] = 'Subscription not saved';

$config['SUB003'] = 'Unable to add Subscription in Stripe';
$config['SUB004'] = 'Subscription ID must be numberic or missing';
$config['SUB005'] = 'Subscription ID doesnot exists';
$config['SUB006'] = 'Unable to update Subscription in Stripe';

$config['SUB007'] = 'Unable to delete Subscription in Stripe';
$config['SUB008'] = 'Subscription deleted';
$config['SUB009'] = 'Unable to delete Subscription';

$config['PAY001'] = 'Balance deducted';
$config['PAY002'] = 'Unable to process transaction.Please try again';
$config['PAY003'] = 'Credit balance is low.';
$config['PAY004'] = 'Unable to find attachment(s)';

$config['PAY005'] = 'Balance recharge';
$config['PAY006'] = 'Unable to recharge please try agian';
$config['PAY007'] = 'Invalid Attachment Id(s)';
$config['PAY008'] = 'Unable to create token';
$config['PAY009'] = 'Unable to save token. Please try again';
$config['PAY010'] = 'Already Paid.';
$config['PAY011'] = 'Please Subscribe First.';

$config['TRAN001'] = 'Transactions history listed';
$config['TRAN002'] = 'Unable List the hisory. Please try again';


$config['NOT_FOUND'] = 'Not Found';
$config['DENIAL']  = "Not Authorized";
