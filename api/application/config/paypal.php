<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['Sandbox']               = TRUE;
$config['APIVersion']            = '78.0';
$config['APIUsername']           = $config['Sandbox'] ? 'SANDBOX_USERNAME_GOES_HERE'  : 'PRODUCTION_USERNAME_GOES_HERE';
$config['APIPassword']           = $config['Sandbox'] ? 'SANDBOX_PASSWORD_GOES_HERE'  : 'PRODUCTION_PASSWORD_GOES_HERE';
$config['APISignature']          = $config['Sandbox'] ? 'SANDBOX_SIGNATURE_GOES_HERE' : 'PRODUCTION_SIGNATURE_GOES_HERE';
$config['DeviceID']              = $config['Sandbox'] ? 'SANDBOX_DEVICE_ID_GOES_HERE' : 'PRODUCTION_DEVICE_ID_GOES_HERE';
$config['ApplicationID']         = $config['Sandbox'] ? 'APP-80W284485P519543T'       : 'PRODUCTION_APP_ID_GOES_HERE';
$config['DeveloperEmailAccount'] = $config['Sandbox'] ? 'SANDBOX_DEV_EMAIL_GOES_HERE' : 'PRODUCTION_DEV_EMAIL_GOES_HERE';



$config['email']         = $config['Sandbox'] ? 'cloudashokm-business@gmail.com' : '';
$config['client_id']     = $config['Sandbox'] ? 'AcpqYFFwPTeIsW2BDC82D8bfl_zOGeLqOn-qePoRxlEIg6C5yttyXjLdSIGBDIaOdaCLxDbfkV-Dny-x' : '';
$config['client_secret'] = $config['Sandbox'] ? 'ED-lFcFQCHq-sayNpwUWGuan2EIeuFzZFQtdw4s5rNtOuPNIkOoXT8X14g8dH_JO8_QYKLHnOZV3UeHC' : '';


$is_https = false;
if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {		
	$is_https = true;
}
$url = 'http';
$url .= ( ($is_https === true) ?'s':'') . '://' . $_SERVER['HTTP_HOST'];
if(  $_SERVER['HTTP_HOST'] == "localhost" ) {
  	$url .= '/mailsman/public';
}
$url .= '/#/';

$config['returnUrl']     = $url . 'paypal-payment';
/*$config['returnUrl']     = site_url('paypal/success');*/
$config['cancelUrl']     = site_url( 'paypal/fail' );