<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * configuration file for file upload
 */

$types = array (
				'txt', 'xml', 'png', 'jpg', 'jpeg', 'ase', 'gif', 'ico', 'pjpeg', 'css',
				'pdf', "x3d", "psd", "pic", "mseq", "swf", "apk", "tpl", "js", "java",
				"jar", "jpeg", "doc", "docm", "dotm", "exe", "xls", "sldx", "ppsx",
				"potx", "xltx", "docx", "dotx", "obd", "xlsx", "pptx", "ppt", "thmx",
				"ppam", "pcf", "pfr", "png", "png", "vcf", "7z", "zip" ,"rar"
			  );

$config['upload_path']   =  BASEPATH . '../attachments/';
$config['allowed_types'] = implode( "|", $types );
$config['encrypt_name']  = TRUE;
$config['max_size']      = '0';
$config['max_width']     = '0';
$config['max_height']    = '0';