<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Stripe\Stripe;
use \Stripe\Charge;
use \Stripe\Customer;
use \Stripe\Refund;

class Stripe_lib {

	protected $CI;
	protected $stripe_config;

	public function __construct() {
		$this->CI = & get_instance();
		$this->stripe_config = $this->CI->load->config('stripe', TRUE );			
		Stripe::setAPiKey( $this->stripe_config['stripe_key_secret'] );
	}

	public function charge( $amount, $postedData ) {
		if( isset( $postedData['stripeToken'] ) ) {

			try {

				$charge = Charge::create(array(
				    "amount" => $amount, // Amount
				    "currency" => "usd",
				    "source" => $postedData['stripeToken'],
				    "description" => $postedData['description']
			    ));

			    return $charge;
			} catch ( Stripe\Error\Card $e ){
				echo $e;
			}

		} else {
			return false;
		}
	}

	public function charge_retrieve( $charge_id ){
			
		try{			
			$retrieve  = Charge::retrieve( $charge_id );
			return $retrieve;
		} catch(Exception $e ){
			printf("Error on refund: %s",$e->getMessage() );
		}
	}

	public function charge_update( $charge_id, $param ) {
		try{
			$charge = $this->charge_retrieve( $charge_id );
			
			if( isset( $param['description'] ) ){
				// default is null
				$charge->description = $param['description'];
			}

			if( isset( $param['fraud_details']) ) {
				 // default is { }
				$charge->fraud_details = $param['fraud_details'];
			}

			if( isset( $param['metadata']) ) {
				// default is { }
				$charge->metadata = $param['metadata'];
			}
			
			if( isset( $param['receipt_email']) ) {
				// default is null
				$charge->receipt_email = $param['receipt_email'];
			}

			if( isset( $param['shipping']) ) {
				// default is {}
				$charge->shipping = $param['shipping'];
			}

			$charge->save();
			// $charge = $this->charge_retrieve( $charge_id );
			return $charge;	
		} catch(Exception $e ){
			return sprintf("Error on refund: %s",$e->getMessage() );
		}

	}

	public function charge_all( $param ){
		/* 
		more info : https://stripe.com/docs/api#list_charges
		charge : Only return refunds for the charge specified by this charge ID.
		customer : Only return refunds for the charge specified by this charge ID.
		created: pagination object
		limit: limit on the number of objects to be returned. Limit can range between 1 and 100 items.
		starting_after: pagination object
		 */
		try{			
			$list  = Charge::all( $param );
			return $retrieve;
		} catch(Exception $e ){
			return sprintf("Error on refund: %s",$e->getMessage() );
		}
	}


	public function refund( $param ) {
		// more info : https://stripe.com/docs/api#create_refund
		/*param:
		amount
		charge : ch_19Us56APR8WeGXmCXifRBO84  comes from charge::create()
		metadata // optional
		reason => duplicate, fraudulent, and requested_by_customer
		*/
		try {


			$refund = Refund::create(array(
				  'charge' => $param['charge'],
				  'amount'  => $param['amount'],
				  'reason' =>  $param['reason'] 
				));

		}catch( Exception $e) {
			return sprintf("Error on refund: %s",$e->getMessage() );
		}
	}

	public function refund_retrieve( $refund_id ){
		// more info https://stripe.com/docs/api#retrieve_refund
		// re_19UspDAPR8WeGXmCI83KM4aK
		try{
			$retrieve = Refund::retrieve( $refund_id );			
			return $retrieve;
		} catch( Exception $e ) {
			return sprintf("Error on refund retrieve: %s",$e->getMessage() );

		}

	} 

	public function refund_list( $param ){
		/* 
		more info : https://stripe.com/docs/api#list_refunds
		created: Only return charges for the customer specified by this customer ID
		customer: Only return charges for the customer specified by this customer ID
		ending_before: pagination object
		limit: limit on the number of objects to be returned. Limit can range between 1 and 100 items.
		starting_after: pagination object
		source: A filter on the list based on the source of the charge
		 */
		try{
			$list = Refund::all( $param );			
			return $list;
		} catch( Exception $e ) {
			return sprintf("Error on refund listing: %s",$e->getMessage() );

		}
	}	

}