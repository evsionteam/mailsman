<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \PayPal\Rest\ApiContext;
// use \PayPal\Api;
use \PayPal\Auth\OAuthTokenCredential;

use \PayPal\Api\Amount;
use \PayPal\Api\Details;
use \PayPal\Api\FundingInstrument;
use \PayPal\Api\Item;
use \PayPal\Api\ItemList;
use \PayPal\Api\Payer;
use \PayPal\Api\PayerInfo;
use \PayPal\Api\Payment;
use \PayPal\Api\PaymentCard;
use PayPal\Api\PaymentExecution;
use \PayPal\Api\Transaction;
use \PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;

use \PayPal\Api\CreditCard;

use \PayPal\Api\Patch;

use \PayPal\Exception\PayPalConnectionException;



class Paypal_lib {

	protected $CI;
	protected $config;
	protected $apiContext;

	public function __construct() {
		$this->CI = & get_instance();
		$this->config = $this->CI->load->config('paypal', TRUE );			
		$this->apiContext = new ApiContext( new OAuthTokenCredential( $this->config['client_id'], $this->config['client_secret'] ) );
	}

	public function setPayment( $args = array() ) {
		$data = array(
					'currency'    => 'USD',
					'amount'      => 1,
					'description' => 'Subscription',
					'saleIntent'  => 'sale',
					'custom'      => '',
					'returnUrl'   =>  $this->config['returnUrl'],
					'cancelUrl'   =>  $this->config['cancelUrl']
				);
		$args = array_merge( $data, $args );

		$payer_info = new PayerInfo;
		if( isset($args['email'] ) && '' != $args['email'] ) {
			$payer_info->setEmail( $args['email'] );			
		}
		
		/**
		 * Set up the payment information object
		 */
		/*Create new payer and method*/
		$payer = new Payer();
		$payer->setPaymentMethod("paypal")->setPayerInfo( $payer_info );
		// $payer->setEmail('')

		/*Set redirect urls*/
		$redirectUrls = new RedirectUrls();
		$redirectUrls->setReturnUrl( $args['returnUrl'] )
		  ->setCancelUrl( $args['cancelUrl'] );

		/*Set payment amount*/
		$amount = new Amount();
		$amount->setCurrency( $args['currency'] )->setTotal( $args['amount'] );

		/*Set transaction object*/
		$transaction = new Transaction();
		$transaction->setAmount($amount)
		->setCustom( $args['custom'] )
		->setNoteToPayee( 'Hello this is note to payee' )
		->setDescription( $args['description'] )
		->setInvoiceNumber(uniqid());



		/*Create the full payment object*/
		$payment = new Payment();
		$payment->setIntent( $args['saleIntent'] ) 
		->setPayer($payer) 
		->setRedirectUrls($redirectUrls) 
		->setTransactions( array( $transaction ) );

		/**
		 * Initialize the payment and redirect the user
		 */
		  /* Create payment with valid API context*/
		  try {
		    $payment->create( $this->apiContext );

		    /* Get PayPal redirect URL and redirect user*/
		    $approvalUrl = $payment->getApprovalLink();
		    return array( 		    		
		    		'code'   => 200,
		    		'message' => $approvalUrl
		    	);
		     /*REDIRECT USER TO $approvalUrl*/
		  } catch (PayPalConnectionException $ex) {
		    /*echo $ex->getCode();
		    echo $ex->getData();*/
		    return array( 		    		
		    		'code' => $ex->getCode(),
		    		'message' => $ex->getData()
		    	);		    
		  } catch (Exception $ex) {		    
		    return array( 		    		
		    		'code' => 400,
		    		'message' => $ex
		    	);
		  }
		 
	}

	public function completePayment( $data ) {
		/*Get payment object by passing paymentId*/
		$paymentId = $data['paymentId'];
		$payment = Payment::get($paymentId, $this->apiContext);
		$payerId = $data['PayerID'];

		/*Execute payment with payer id*/
		$execution = new PaymentExecution();
		$execution->setPayerId($payerId);

		try {
		  	/*Execute payment*/
		  	$result = $payment->execute( $execution, $this->apiContext );
		  	/* get detail of payment*/
			$result = $payment->get( $paymentId, $this->apiContext );
			
			return array( 
						'code' => 200,
						'result' => $result->toJSON()
					);
		  
		} catch ( PayPalConnectionException $ex) {
		  /*echo $ex->getCode();
		  echo $ex->getData();*/
		  return array(
		  				'code' => $ex->getCode(),
		  				'message' => $ex->getData()
			  		);
		  die($ex);
		} catch (Exception $ex) {
		   	return array(
			   				'code' => 400,
					  		'message' => $ex
				  		);
		  die($ex);

		}
	}


	

}