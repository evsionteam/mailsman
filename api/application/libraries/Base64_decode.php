<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
*  Base64 Decode Codeigniter libarary
*/
class Base64_decode {
	protected $CI;

	public function __construct() {
		$this->CI = & get_instance();
	}


	public function string_decoder( $string = NULL ) {
		if ( !is_null( $string ) ) {
			return base64_decode($string);
		}
		return $string;
	}

	public function base64_decoder( $inputfile, $outputfile ) { 
		/* read data (binary) */ 
		$ext = $this->_getMimeType( $inputfile );
		$data = '';
		if( is_null($ext) ){			
			//text
			$ext  = 'txt';
			$data = $inputfile;
		} else {
			// removing 'data:image/(.*);base64' from encode file
			$data = preg_replace('/^data:([A-Za-z-+\/]+)\/([A-Za-z-+\/]+);base64,/', "", $inputfile);		
			/* encode & write data (binary) */ 

		}	
		$outputfile = $outputfile . '.' . $ext;

		$ifp = fopen( $outputfile, "wb" ); 
		fwrite( $ifp, $this->string_decoder( $data ) ); 		
		fclose( $ifp ); 
		
		return basename( $outputfile );
		
	}

	protected function _getMimeType($data){

		$lowercase64 = strtolower( $data );
		$pattern     = '/^data:([A-Za-z-+\/]+)\/(.*);base64/i';

		preg_match( $pattern, $lowercase64, $match );
		
		return $this->_getExtension ( $match[ count($match) - 1 ] );
		 
		return NULL;
	}

	protected function _getExtension( $extension = 'text' ) {
		$mime_type = $this->_mime_type_array();
		if( array_key_exists( $extension, $mime_type ) ) {
			return $mime_type[$extension];
		} else {
			return NULL;
		}

	}

	public function is_base64_encoded( $aata ) {
        if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    protected function _mime_type_array(){
    		$mime_types = array(
								'text'                                                           =>  'txt',
								'xml'                                                            =>  'xml',
								'png'                                                            =>  'png',
								'jpg'	                                                         =>  'jpg',
								'jpeg'                                                           =>  'jpeg',
								'bmp'                                                            =>  'ase',
								'gif'                                                            =>  'gif',
								'ico'                                                            =>  'ico',
								'pjpeg'                                                          =>  'pjpeg',
								'css'                                                            =>  'css',
								'pdf'                                                            =>  'pdf',
								"vnd.hzn-3d-crossword"                                           =>  "x3d",
								"vnd.adobe.photoshop"                                            =>  "psd",
								"x-pict"                                                         =>  "pic",
								"vnd.mseq"                                                       =>  "mseq",
								"x-shockwave-flash"                                              =>  "swf",
								"vnd.android.package-archive"                                    =>  "apk",
								"vnd.groove-tool-template"                                       =>  "tpl",
								"javascript"                                                     =>  "js",
								"x-java-source,java"                                             =>  "java",
								"java-archive"                                                   =>  "jar",
								"x-citrix-jpeg"                                                  =>  "jpeg",
								"msword"                                                         =>  "doc",
								"vnd.ms-word.document.macroenabled.12"                           =>  "docm",
								"vnd.ms-word.template.macroenabled.12"                           =>  "dotm",
								"x-msdownload"                                                   =>  "exe",
								"vnd.ms-excel"                                                   =>  "xls",
								"vnd.openxmlformats-officedocument.presentationml.slide"         =>  "sldx",
								"vnd.openxmlformats-officedocument.presentationml.slideshow"     =>  "ppsx",
								"vnd.openxmlformats-officedocument.presentationml.template"      =>  "potx",
								"vnd.openxmlformats-officedocument.spreadsheetml.template"       =>  "xltx",
								"vnd.openxmlformats-officedocument.wordprocessingml.document"    =>  "docx",
								"vnd.openxmlformats-officedocument.wordprocessingml.template"    =>  "dotx",
								"x-msbinder"                                                     => "obd",
								"vnd.openxmlformats-officedocument.spreadsheetml.sheet"          =>  "xlsx",
								"vnd.openxmlformats-officedocument.presentationml.presentation"  =>  "pptx",
								"vnd.ms-powerpoint"                                              =>  "ppt",
								"vnd.ms-officetheme"                                             =>  "thmx",
								"vnd.ms-powerpoint.addin.macroenabled.12"                        =>  "ppam",
								"x-font-pcf"                                                     =>  "pcf",
								"font-tdpfr"                                                     =>  "pfr",
								"x-citrix-png"                                                   =>  "png",
								"x-png"                                                          =>  "png",
								"x-vcard"                                                        =>  "vcf",
								"x-7z-compressed"                                                =>  "7z",
								"zip"                                                            =>  "zip"
    			
			);

			return $mime_types;
    }
}