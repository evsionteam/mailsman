<?php
class MY_Form_validation extends CI_Form_Validation {

    public function error_array() {
        return $this->_error_array;
    }

    protected function _get_error_message($rule, $field) {
    	
		// check if a custom message is defined through validation config row.
		if (isset($this->_field_data[$field]['errors'][$rule])) {
			if( is_array( $this->_field_data[$field]['errors'][$rule] ) ){
				return array(
								'code' => $this->_field_data[$field]['errors'][$rule]['code'],
								/* 'message' => $this->_field_data[$field]['errors'][$rule]['message'],*/
								'field' => $this->_field_data[$field]['errors'][$rule]['field']
							);
			} else {
				return $this->_field_data[$field]['errors'][$rule];
			}
		} elseif (isset($this->_error_messages[$rule])) {
			// check if a custom message has been set using the set_message() function
			return $this->_error_messages[$rule];
		} elseif (FALSE !== ($line = $this->CI->lang->line('form_validation_'.$rule))) {
			return $line;
		} elseif (FALSE !== ($line = $this->CI->lang->line($rule, FALSE))) {
			// DEPRECATED support for non-prefixed keys, lang file again
			return $line;
		}

		return $this->CI->lang->line('form_validation_error_message_not_set').'('.$rule.')';
	}

		/**
	 * Build an error message using the field and param.
	 *
	 * @param	string	The error message line
	 * @param	string	A field's human name
	 * @param	mixed	A rule's optional parameter
	 * @return	string
	 */
	protected function _build_error_msg($line, $field = '', $param = '') {
		
		// Check for %s in the string for legacy support.
		if ( is_array( $line ) ){
			/*if (strpos($line['message'], '%s') !== FALSE ) {
				$line['message'] = sprintf($line['message'], $field, $param);
			} */
			return $line;
		} else if( strpos($line, '%s') !== FALSE ) {
			return sprintf($line, $field, $param);
		}
		
		return str_replace(array('{field}', '{param}'), array($field, $param), $line);
	}


	/**
	 * Set Rules
	 *
	 * This function takes an array of field names and validation
	 * rules as input, any custom error messages, validates the info,
	 * and stores it
	 *
	 * @param	mixed	$field
	 * @param	string	$label
	 * @param	mixed	$rules
	 * @param	array	$errors
	 * @return	CI_Form_validation
	 */
	public function set_rules($field, $label = '', $rules = array(), $errors = array()) {

		// No reason to set rules if we have no POST data
		// or a validation array has not been specified
		if ($this->CI->input->method() !== 'post' && $this->CI->input->method() !== 'put' &&  empty($this->validation_data)) {
			return $this;
		}

		// If an array was passed via the first parameter instead of individual string
		// values we cycle through it and recursively call this function.
		if (is_array($field)) {

			foreach ($field as $row) {
				
				// Houston, we have a problem...
				if ( ! isset($row['field'], $row['rules'])) {
					continue;
				}

				// If the field label wasn't passed we use the field name
				$label = isset($row['label']) ? $row['label'] : $row['field'];

				// Add the custom error message array
				$errors = (isset($row['errors']) && is_array($row['errors'])) ? $row['errors'] : array();

				// Here we go!
				$this->set_rules($row['field'], $label, $row['rules'], $errors);
			}

			return $this;
		}

		// No fields or no rules? Nothing to do...
		if ( ! is_string($field) OR $field === '' OR empty($rules)) {
			return $this;
		} elseif ( ! is_array($rules)) {
			// BC: Convert pipe-separated rules string to an array
			if ( ! is_string($rules)) {
				return $this;
			}

			$rules = preg_split('/\|(?![^\[]*\])/', $rules);
		}

		// If the field label wasn't passed we use the field name
		$label = ($label === '') ? $field : $label;

		$indexes = array();

		// Is the field name an array? If it is an array, we break it apart
		// into its components so that we can fetch the corresponding POST data later
		if (($is_array = (bool) preg_match_all('/\[(.*?)\]/', $field, $matches)) === TRUE) {
			sscanf($field, '%[^[][', $indexes[0]);

			for ($i = 0, $c = count($matches[0]); $i < $c; $i++) {
				if ($matches[1][$i] !== '')
				{
					$indexes[] = $matches[1][$i];
				}
			}
		}

		// Build our master array
		$this->_field_data[$field] = array(
			'field'		=> $field,
			'label'		=> $label,
			'rules'		=> $rules,
			'errors'	=> $errors,
			'is_array'	=> $is_array,
			'keys'		=> $indexes,
			'postdata'	=> NULL,
			'error'		=> ''
		);

		return $this;
	}

}