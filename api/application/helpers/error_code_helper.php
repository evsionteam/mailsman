<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function get_error_message( $code = NULL ){
	$ci = & get_instance();
	$message = $ci->load->config('code', TRUE );
	if( !is_null( $code ) ){
		return $message[ $code ];
	}
	return;
}