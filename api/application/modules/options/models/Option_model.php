<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Option_model class.
 * 
 * @extends Base_model
 */
class Option_model extends Base_model {

	
	public function __construct() {
		parent::__construct();		
		$this->table = $this->prefix . "options";
	}

	public function insert_batch( $insertArr ) {		
		return $this->db->insert_batch( $this->table , $insertArr );
	}

	public function update_batch( $updatetArr = NULL, $whereField = NULL ) {		
		if( !is_null( $updatetArr ) && !is_null( $whereField ) ) {
			return $this->db->update_batch( $this->table , $updatetArr, $whereField );
		} 

		return FALSE;
			
	}

	
}

/* End of file*/
