<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Options class.
 * 
 * @extends CI_Controller
 */
class Options extends MY_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();		
		$this->load->model('option_model');		

	}
	public function index() {}
	

	public function options_get( $option_id = false, $page = false, $search = false ) {
		$this->load->config('pagination');
		
		// Listing
		// check if user have admin role or not
		$current_user = $this->current_user;
	   	$data = array();
	   	
	   	$where = NULL;
	   	if(  $option_id !== false  && ( is_numeric( $option_id ) && $option_id != 0 )  ) {
	   		$where['id'] = $option_id ;
	   	}

	   	$order_by = array(
	   	                'id' => 'DESC'
	   	            );

	   	$limit = array();
	   	if(  false !== $page ) {
	   	    $page = ( $page < 0 ) ? 1: $page;
	   	    $limit['per_page'] =  $this->config->item('per_page');
	   	    $limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
	   	}

	   	$like = NULL;

	   	if( false !== $search ) {
	   		$like = array(
	   				'option_name' => $search
	   			);
	   	}
	   	
	   	$response = $this->option_model->get( $where, $order_by, $limit, $like  );
        $count = $this->option_model->get_count( $where, $order_by, $like );
        $result = array();
        foreach( $response as $row ){        	
        	$result[ $row->option_name ] = $row->option_value;
        }

	   	if( count($result) > 0 ){
	   		$data['status'] = 200;
	   		$data['code']   = 'OPT006';
            $data['total_rows'] = $count;
	   		$data['data']   = $result;
	   	}  else if( is_null( $result )){
	   		$data['status']   = 200;
	   		$data['code'] = 'NOT_FOUND';
	   		$data['data']   = NULL;

	   	} else {
	   		$data['status'] = 400;
	   		$data['code']   = 'OPT007';
	   	}		

		$this->response( $data );
	}

	public function options_post(){ 

	
		$current_user  = $this->current_user;
		// posted data
		$data = array();
		$_POST = json_decode( trim( file_get_contents('php://input') ), true );		
		
	 	$response = false;
		$insertArr  = array();
		$posted_data = $this->input->post();

		if( count($posted_data ) > 0 ) {
			
			foreach( $posted_data as $key => $value ){
				$insertArr[] = array( 
										'option_name'  => $key,
										'option_value' => $value,
										'created_at '  => date('Y-m-d H:i:s'),
										'updated_at'   => date('Y-m-d H:i:s')
									);
			}
			
			
			$response = $this->option_model->insert_batch( $insertArr );
		}

		if ( $response ) {
			$data['status'] = 200;
			$data['code'] = 'OPT002';
			
		} else {
			//creation failed, this should never happen 
			$data['status'] = 400;
			$data['code'] = 'OPT002';
		}		 	
		

		$this->response( $data );
	}

	public function options_put(){ 		
		
		$current_user  = $this->current_user;
		// posted data
		$data = array();
		$_POST = json_decode( trim( file_get_contents('php://input') ), true );
		
		// $option_id = $this->uri->segment( $this->uri->total_segments(), false );
		$posted_data = $this->input->post();

		$where = NULL;
		$response = false;
		$updateArr = array();
			
		if( count($posted_data ) > 0 ) {
		
			foreach( $posted_data as $key => $value ){
				$updateArr[] = array( 
										'option_name'  => $key,
										'option_value' => $value
									);
			}
			$response = $this->option_model->update_batch( $updateArr, 'option_name' );
		}
		

		if ( $response ) {
			$data['status'] = 200;
			$data['code'] = 'OPT010';
			$data['data'] = $posted_data;
			
		} else {
			//creation failed, this should never happen 
			$data['status'] = 400;
			$data['code'] = 'OPT002';
		}		

		$this->response( $data );
	}

	public function options_delete( $option_id = FALSE){

		// delete
	   	$data = array();
		if ( is_numeric( $option_id ) && $option_id != false ) {
			//  is_archive  is set to 1 to flagged as deleted
			$deleteRes = $this->option_model->delete( array('id' => $option_id ) ); 

			if( $deleteRes ){
				$data['status'] = 200;
				$data['code']   = 'OPT003';
			} else {
				$data['status'] = 400;
				$data['code'] = 'OPT004';
			}
			
		} else {
			$data['status'] = 400;
			$data['code'] = 'OPT005';
		}
		$this->response( $data );
	}

	private function _form_rules() {
	    $rules = array(
	        array(
	            'field' => 'option_name',
	            'label' => 'Option Name',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'SRV007',
	                                        'field'   => 'option_name'
	                                    )
	            )
	        ),array(
	            'field' => 'option_value',
	            'label' => 'Option Value',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'SRV008',
	                                        'field'   => 'option_value'
	                                    )
	            )
	        )
	        
	    );

	    return $rules;
	}

}
/* End of file*/