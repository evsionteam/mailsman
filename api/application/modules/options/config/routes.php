<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['options']['GET'] = "options/options/options";

$route['options/(:num)']['GET'] = "options/options/options/$1";

/* options/{noID=0}/{page}/ =>pagination */
$route['options/(:num)/(:any)']['GET'] = "options/options/options/$1/$2";

/* options/{noID=0}/{page}/{search}  =>search */
$route['options/(:num)/(:any)/(:any)']['GET'] = "options/options/options/$1/$2/$3";

$route['options']['POST'] = "options/options/options";

$route['options']['PUT'] = "options/options/options";
// $route['options/(:num)']['PUT'] = "options/options/options/$1";

$route['options/(:num)']['DELETE'] = "options/options/options/$1";