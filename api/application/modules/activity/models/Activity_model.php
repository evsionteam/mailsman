<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Attachment_model class.
 * 
 * @extends Base_Model
 */
class Activity_model extends Base_model {

	
	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . "activities";
	}

	public function get( $where = NULL, $order_by = NULL, $limit = NULL, $like = NULL ) {
		
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}
		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}
		
		$this->db->select( '*,  UNIX_TIMESTAMP( `created_at`) as `created_on` ,UNIX_TIMESTAMP( `updated_at`) as `updated_on`' );
		
		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}

		$query = $this->db->get( $this->table );


		if( $query->num_rows() > 0 ){			
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	public function get_count( $where = NULL, $order_by = NULL, $like = NULL ) {
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}

		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	

		$this->db->select( 'count( * ) as total_rows' );
		
		$query = $this->db->get( $this->table );

		if( $query->num_rows() > 0 ){			
			return $query->row('total_rows');			
		} else {
			return 0;
		}
	}
}

/* End of file*/
