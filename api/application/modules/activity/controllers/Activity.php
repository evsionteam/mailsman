<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Attachments class.
 * 
 * @extends CI_Controller
 */
class Activity extends MY_Controller {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();		
		$this->load->model('activity_model');
		$this->load->model('users/user_model','user_model');
		$this->load->model('message/message_model','message_model');
	}
	public function index() {}
	

	public function activity_get( $activity_id = false, $page = false, $search = false ) {
		
		$this->load->config('pagination');
		
		$current_user = $this->current_user;
	  	$current_user_role = $current_user->role;
	   	$data = array();
	   	
	   	$where = NULL;
	   	
	   	if( $activity_id !== false  && ( is_numeric( $activity_id ) && $activity_id != 0 ) ) {
	   		$where['id'] = $activity_id ;
	   	}

	   	if(  "administrator" != $current_user_role ) {
	   		$where['user_id'] = $current_user->userId;
	   	}   		

	   	$order_by = array(
	   					'id' => 'DESC'
	   				);

		$limit = array();
		if(  false !== $page ) {
			$page = ( $page < 0 ) ? 1: $page;
    		$limit['per_page'] = $this->config->item('per_page');
    		$limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
		}

		$like = NULL;

		if( false !== $search ) {
			$where['user_id'] = $search;
		}
		$not_to_include = "(`modules` != 'balance_load' AND `modules` != 'balance_deduct' )";
		if(  "administrator" == $current_user_role ) {
			$not_to_include .= "AND ( `modules` != 'users' AND `modules` != 'message' AND `modules` != 'compose' AND `modules` != 'compose-attachment' )";			
		}
		//$res->modules == "users" || $res->modules == 'compose' ||  $res->modules == 'compose-attachment'
	   	
		$this->db->where( $not_to_include );
	   	$response = $this->activity_model->get( $where, $order_by, $limit, $like );
	   	// $data['query'] = $this->db->last_query();
	   	
		$this->db->where( $not_to_include );
	   	$count = $this->activity_model->get_count( $where, $order_by, $like );

	   	if( $response ){
	   		$this->__activities_processing( $response, $count, $current_user );	   		
	   		$data['status']   = 200;
	   		$data['code'] = 'ACT006';
	   		$data['total_rows'] = $count;
	   		$data['data']   = $response;
	   	} else if( is_null( $response )){
	   		$data['status']   = 200;
	   		$data['code'] = 'NOT_FOUND';
	   		$data['data']   = NULL;

	   	} else {
	   		$data['status']   = 400;
	   		$data['code'] = 'ACT007';
	   	}		

		$this->response( $data );
	}

	public function recent_get( $how_many ){

			$current_user = $this->current_user;

		   	$data = array(
		   		'status' => 404,
		   		'code'   => 'ACT007',
		   		'data'   => []
		   	);

		   	$order_by = array( 'id' => 'DESC' );

		   	$limit = array(
		   		'per_page' => $how_many,
		   		'offset'   => 0
		   	);

		   	$where = array( 'user_id' => $current_user->userId );

		   	$response = $this->activity_model->get( $where, $order_by, $limit );

		   	if( $response ){
		   		$this->__activities_processing( $response, $how_many, $current_user );	   		

		   		$data['code'] = 'ACT006';
		   		$data['data'] = $response;
		   		$data['status'] = 200;
		   	}

		   	$this->response( $data );
		}

	protected function __activities_processing( &$response ,&$count, $current_user ){
		$current_user_role = $current_user->role;
   		foreach( $response as $key => $res ):
    		$get_user = $this->user_model->get_user( array( '`usr`.`id`'  => $res->user_id ) );
    		$userDetail = $get_user->row();
   			if(  "administrator" != $current_user_role ) {
   					
	   			if ( $res->modules == "users" ) {
		   			$response[$key]->activity = sprintf( $res->activity, 'You', 'your' );

	   			} else if ( $res->modules == "payment" ) {
		   			$response[$key]->activity = sprintf( $res->activity, 'You', 'USD' );

	   			}  else if ( $res->modules == 'compose' ||  $res->modules == 'compose-attachment' ) {
	   				$act = unserialize( $res->activity );
	   				$user_id = $act['staff_id'];
	   				if( 'staff' == $current_user_role ){
	   					$user_id =  $act['customer_id'];
	   				}
	   				$act_user = $this->user_model->get_user( array( '`usr`.`id`'  => $user_id ) )->row();

					if( count( $act_user ) > 0 ){
		   				if( $act['staff_id']  == $current_user->userId ) {
		   					$response[$key]->activity = sprintf( '%s have composed new message%s for %s', 'You',( ('compose' == $res->modules )?'':' with attachment'), $act_user->email );

		   				} else if( $act['customer_id'] == $current_user->userId ) {
		   					$response[$key]->activity = sprintf( '%s have received new message%s from %s', 'You' ,( ('compose' == $res->modules )?'':' with attachment'), $act_user->email );
		   				}
		   				if( isset( $act['message'] ) ){
							$response[$key]->message = $act['message'];		   					
		   				}

					}
	   			} else if( 'subscribe' == $res->modules ) {
	   				$response[$key]->activity = sprintf( $res->activity, 'You');

	   			} else if( 'balance_deduct' == $res->modules ) {
	   				$response[$key]->activity = sprintf( $res->activity, 'You','USD');

	   			} else if( 'balance_load' == $res->modules ) {
	   				$response[$key]->activity = sprintf( $res->activity, 'You','USD');

	   			} else if( 'download' == $res->modules ) {
	   				$text= 'You had';
	   				if( 'staff' == $current_user_role ){
	   					$text = $userDetail->email .' had';
	   				}
	   				$activity = explode( '|', $res->activity );
	   				if ( count( $activity ) > 1 ){
						$response[$key]->activity = sprintf( $activity[0], $text );
						
						$getMessage = $this->message_model->get( array( '`msg`.`attachment_id`' => $activity[1] ) );
						
						if( count( $getMessage ) > 0 ) {
							$response[$key]->message =$getMessage[0]->message;
						}
 	   				} else {
		   				$response[$key]->activity = sprintf( $res->activity, $text ); 
		   			}
	   			} else if ('message' == $res->modules){
	   				
	   				$act = unserialize( $res->activity );
	   				
   					if( 'pending' == $act['status'] ) {
						$response[$key]->activity = sprintf( '%s replied to message', 'You had' );
   					} else if( 'completed' == $act['status'] ) {
   						$response[$key]->activity = sprintf( '%s paid for messages', 'You had' );
   					}

	   				if( isset( $act['message'] ) ){
						$response[$key]->message = $act['message'];		   					
	   				}
	   				
	   			}
   			} else {
        		/* Administrator*/
				$act_user = $this->user_model->get_user( array( '`usr`.`id`'  => $res->user_id ) )->row();
        		if( count( $userDetail ) > 0  && count($act_user) > 0 ){

		   			if ( "payment"  == $res->modules ) {
			   			$response[$key]->activity = sprintf( $res->activity, $userDetail->email, 'USD' );

		   			} else if ($res->modules == "users" || $res->modules == 'message' || $res->modules == 'compose' ||  $res->modules == 'compose-attachment' ) {
		   				
		   				unset( $response[$key] );
		   				$count--;

		   			} else if( 'balance_deduct' == $res->modules ) {
		   				$response[$key]->activity = sprintf( $res->activity, $act_user->email,'USD' );

		   			} else if( 'balance_load' == $res->modules ) {
		   				$response[$key]->activity = sprintf( $res->activity, $act_user->email,'USD' );

		   			} else if( 'download' == $res->modules ) {

		   				$activity = explode( '|', $res->activity );
		   				if ( count( $activity ) > 1 ){
							$response[$key]->activity = sprintf( $activity[0], $act_user->email.' had' );
						
							$getMessage = $this->message_model->get( array( '`msg`.`attachment_id`' => $activity[1] ) );
							
							if( count( $getMessage ) > 0 ) {
								$response[$key]->message =$getMessage[0]->message;
							}
	 	   				} else {
			   				$response[$key]->activity = sprintf( $res->activity, $act_user->email.' had' );
			   			}

		   			} else if( 'subscribe' == $res->modules ) {
		   				$response[$key]->activity = sprintf( $res->activity, $act_user->email );

		   			}
        		}
   			}
   		endforeach;	
	}

}
/* End of file*/