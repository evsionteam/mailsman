<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['activity']['GET'] = "activity/activity/activity";

$route['activity/recent/(:num)']['GET'] = "activity/activity/recent/$1";

$route['activity/(:num)']['GET'] = "activity/activity/activity/$1";

/* activity/{noID=0}/{page}/ =>pagination */
$route['activity/(:num)/(:any)']['GET'] = "activity/activity/activity/$1/$2";

/* activity/{noID=0}/{page}/{search}  =>search */
$route['activity/(:num)/(:any)/(:any)']['GET'] = "activity/activity/activity/$1/$2/$3";

$route['activity']['POST'] = "activity/activity/activity";

$route['activity/(:num)']['DELETE'] = "activity/activity/activity/$1";