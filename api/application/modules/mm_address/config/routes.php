<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['mmaddress']['GET']           = 'mm_address/mm_address/mm_address';

$route['mmaddress/(:num)']['GET']    = 'mm_address/mm_address/mm_address/$1';


/* mm_address/{noID=0}/{page}/ =>pagination */
$route['mmaddress/(:num)/(:any)']['GET'] = "mm_address/mm_address/mm_address/$1/$2";

/* mm_address/{noID=0}/{page}/{search}  =>search */
$route['mmaddress/(:num)/(:any)/(:any)']['GET'] = "mm_address/mm_address/mm_address/$1/$2/$3";

/* mm_address/{noID=0}/{page}/{ search / 0 }{onlyMM}  =>MM adresses not assigned list */
$route['mmaddress/(:any)/(:any)/(:any)/(:any)']['GET'] = "mm_address/mm_address/mm_address/$1/$2/$3/$4";

$route['mmaddress']['POST']          = 'mm_address/mm_address/mm_address';

$route['mmaddress/(:num)']['PUT']    = 'mm_address/mm_address/mm_address/$1';

$route['mmaddress/(:num)']['DELETE'] = 'mm_address/mm_address/mm_address/$1';

$route['mmaddress/assign_user/(:num)']['PUT']    = 'mm_address/mm_address/assign_user/$1';