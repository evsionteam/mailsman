<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users class.
 *
 * @extends CI_Controller
 */
class Mm_address extends MY_Controller {

    /**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	*/

	public function __construct() {
		parent::__construct();
        $this->load->model( 'mm_address_model', 'mm_model' );
        $this->load->model('users/user_model','user_model');
	}

	public function index() {}

    public function mm_address_get( $id = false, $page = false, $search = false , $onlyMM = false ) {

        // check if user have admin role or not
        $current_user = $this->current_user;
        $current_user_role = $current_user->role;
        $data = array();
        $where = NULL;
        if ( 'administrator' != $current_user_role  ) {
            $data['status'] = 400;
            $data['code'] = 'MM008';
        } else {
            if(  $id !== false  && ( is_numeric( $id ) && $id != 0 )  ) {
                $where['mm.id'] = $id ;
            }

            $order_by = array('`mm`.`id`' => 'DESC');

            $limit = array();
            if(  false !== $page ) {
                $page = ( $page < 0 ) ? 1: $page;
                $limit['per_page'] =  $this->config->item('per_page');
                $limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
            }

            $like = NULL;
            
            if( false != $search  ) {
                $like = array ( 
                        '`mm`.`address_line1`' => $search ,
                        '`mm`.`address_line2`' => $search,
                        '`mm`.`city`' => $search,
                        '`mm`.`state`' => $search,
                        '`mm`.`country`' => $search,
                        '`mm`.`zip`' => $search,
                        '`mm`.`phone`' => $search
                    );
            }

            if( false !== $onlyMM ){
                $where['`usr`.`mm_address`'] = NULL;
            }

            $response = $this->mm_model->get( $where, $order_by, $limit, $like  ); 
            // echo $this->db->last_query();die;
            $count = $this->mm_model->get_count( $where, $order_by, $like );

            $result = array();

            if( $response > 0 ) {
                $data['status'] = 200;
                $data['code']   = 'MM001';
                $data['total_rows'] = $count;
                $data['data']   = $response;
              
            }else if( is_null( $response )){
                
                $data['status']   = 200;
                $data['code'] = 'NOT_FOUND';
                $data['data'] = [];

            }  else {
                $data['status'] = 400;
                $data['code']   = 'MM002';
            }

        }

        $this->response( $data );
    }

    public function mm_address_post(){

        $current_user  = $this->current_user;
        $current_user_role = $current_user->role;
        // posted data
        $data = array();
        $_POST = json_decode( trim( file_get_contents('php://input') ), true );
        $response = false;

        if ( 'administrator' == $current_user_role  ) {

            $posted_data = $this->input->post();

            if( $posted_data ){

                    $ins_data = array(
                                    'mm_address'    => isset( $posted_data['mm_address'] )?$posted_data['mm_address']:'',
                                    'address_line1' => $posted_data['address_line1'],
                                    'address_line2' => isset($posted_data['address_line2'])?$posted_data['address_line2']:'',
                                    'city'          => $posted_data['city'],
                                    'state'         => $posted_data['state'],
                                    'country'       => $posted_data['country'],
                                    'zip'           => $posted_data['zip'],
                                    'phone'         => $posted_data['phone'],
                                    'created_at '   => date('Y-m-d H:i:s'),
                                    'updated_at'    => date('Y-m-d H:i:s')
                                );

                $response = $this->mm_model->insert( $ins_data );
            }

            if ( $response ) {
                $ins_data['id'] = $this->db->insert_id();
                $data['status'] = 200;
                $data['code'] = 'MM003';
                $data['data'] = $ins_data;

            } else {
                //creation failed, this should never happen
                $data['status'] = 400;
                $data['code'] = 'MM004';
            }
        } else {
            $data['status'] = 400;
            $data['code'] = 'MM008';
        }

        $this->response( $data );

    }

    public function mm_address_put( $id = FALSE ){

       $current_user  = $this->current_user;
       $current_user_role = $current_user->role;
       
       $data = array();
       $_POST = json_decode( trim( file_get_contents('php://input') ), true );
       if( ( is_numeric( $id ) && $id !== false ) && 'administrator' == $current_user_role  ){
           $posted_data = $this->input->post();

           $where = NULL;
           $response = false;
          

            if( count( $posted_data) > 0 ) {

                $updateArr = array(
                                'mm_address'    => isset( $posted_data['mm_address'] )?$posted_data['mm_address']:'',
                                'address_line1' => $posted_data['address_line1'],
                                'address_line2' => isset($posted_data['address_line2'])?$posted_data['address_line2']:'',
                                'city'          => $posted_data['city'],
                                'state'         => $posted_data['state'],
                                'country'       => $posted_data['country'],
                                'zip'           => $posted_data['zip'],
                                'phone'         => $posted_data['phone']
                            );

                $response = $this->mm_model->update( $updateArr, array( 'id' => $id ) );
           }

           if ( $response ) {
            $data['status'] = 200;
            $data['code'] = 'MM003';

           } else {
            //creation failed, this should never happen
            $data['status'] = 400;
            $data['code'] = 'MM004';
           }
       } else if ( 'administrator' != $current_user_role ) {
            $data['status'] = 400;
            $data['code'] = 'MM008';
       } else {
            $data['status'] = 400;
            $data['code'] = 'MM007';
       }




        $this->response( $data );

    }

    public function mm_address_delete( $id = false ){
        $current_user  = $this->current_user;
        $current_user_role = $current_user->role;
        // delete
        $data = array();
        if ( is_numeric( $id ) && $id != false && 'administrator' == $current_user_role  ) {
            //  is_archive  is set to 1 to flagged as deleted
            $deleteRes = $this->mm_model->delete( array('id' => $id ) );

            if( $deleteRes ){
                $data['status'] = 200;
                $data['code']   = 'MM005';
            } else {
                $data['status'] = 400;
                $data['code'] = 'MM006';
            }

        } else if ( 'administrator' != $current_user_role ) {
            $data['status'] = 400;
            $data['code'] = 'MM008';
        } else {
            $data['status'] = 400;
            $data['code'] = 'MM007';
        }
        $this->response( $data );
    }

    public function assign_user_put( $mm_id = false ){

        $current_user  = $this->current_user;
        $current_user_role = $current_user->role;

        $_POST = json_decode( trim( file_get_contents( 'php://input' ) ) , TRUE );

        if( $mm_id != false && is_numeric( $mm_id ) && 'administrator' == $current_user_role ){

            $user_id = $this->input->post('id');
            $assign_val = $this->input->post('value');

            if( is_numeric( $user_id ) ) {

                $get_user = $this->user_model->get_user( array( '`usr`.`id`' => $user_id ) );

                if( count( $get_user ) > 0 ) {
                   
                    try {

                        $updateArr = array('mm_address' =>  $mm_id  );
                        $response = $this->user_model->update( $updateArr, array('id' => $user_id ) );

                        $query = $this->mm_model->get( array( 'mm.id' => $mm_id ) );

                        $data[ 'data' ] = $query[0];
                        $data['status'] = 200;
                        $data['code']   = 'USR026';

                    } catch( Exception $e) {
                        $data['status'] = 400;
                        $data['code']   = 'USR027';
                        $data['data']   = null;
                    }
                    
                }else{
                    $data['status'] = 400;
                    $data['code'] = 'USR008'; 
                }
                
            }else{
                $data['status'] = 400;
                $data['code'] = 'MM007';
            }
        }else {
            $data['status'] = 400;
            $data['code'] = 'MM007';
        }

        $this->response( $data );
    }
}
/* End of file*/
