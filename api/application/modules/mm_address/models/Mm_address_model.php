<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * mm_address_model class.
 *
 * @extends Base_model
 */
class Mm_address_model extends Base_model {

	public function __construct() {		
		parent::__construct();		
		$this->table =  $this->prefix . "mm_address";

		$this->join_table =  $this->prefix . "users as `usr`";		
	}	

	public function get( $where = NULL, $order_by = NULL, $limit = NULL, $like = NULL ) {
		
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}
		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}
		
		$this->db->select( ' `mm`.id, `mm`.`address_line1`, `mm`.`address_line2`, `mm`.`city`, `mm`.`state`, `mm`.`country`, `mm`.`zip`, `mm`.`phone`, UNIX_TIMESTAMP( `mm`.`created_at`) as `created_on` ,UNIX_TIMESTAMP( `mm`.`updated_at`) as `updated_on`' );
		$this->db->from($this->table . ' as `mm` ');
			
		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			if( count( $like ) > 1 ){
				foreach( $like as $key => $val ):
					$this->db->or_like( $key, $val ,'both');
				endforeach; 				
			} else {
				$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
			}

		}	

		$query = $this->db->get();

		if( $query->num_rows() > 0 ){			
			return $query->result();
		} else {
			return NULL;
		}
	}

	public function get_count( $where = NULL, $order_by = NULL, $like = NULL ) {
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}

		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	

		$this->db->select( 'count( `mm`. `id` ) as `total_rows`', false );
		$this->db->from($this->table . ' as `mm` ');
		$this->db->join($this->join_table,'`usr`.`mm_address` = `mm`.`id` ','left');
		
		$query = $this->db->get();

		if( $query->num_rows() > 0 ){			
			return $query->row('total_rows');			
		} else {
			return 0;
		}
	}


	public function insert_batch( $insertArr ) {		
		return $this->db->insert_batch( $this->table , $insertArr );
	}

	public function update_batch( $updatetArr = NULL, $whereField = NULL ) {		
		if( !is_null( $updatetArr ) && !is_null( $whereField ) ) {
			return $this->db->update_batch( $this->table , $updatetArr, $whereField );
		} 
		return FALSE;			
	}	

 }