<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Notification_model class.
 * 
 * @extends Base_model
 */
class Message_model extends Base_model {

	
	
	public function __construct() {		
		parent::__construct();		
		$this->table =  $this->prefix . "message";
		$this->join_table = $this->prefix . "attachments as `att`";
		$this->join_table2 = $this->prefix . "users as `usr`";
		$this->join_table3 = $this->prefix . "services as `svr`";
	}	

	public function get( $where = NULL, $order_by = NULL, $limit = NULL, $like = NULL ) {
		
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}
		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}
		
		$this->db->select( '`msg`.`id`,`msg`.`group_id`, `msg`.`sender_id`,`msg`.`receiver_id`,`msg`.`attachment_id`,`msg`.`service_id`,`msg`.`message`, `msg`.`special_message`, `msg`.`status`,`msg`.`is_seen`,`msg`.`has_delivery`,`msg`.`is_home_address`,`msg`.`address` , UNIX_TIMESTAMP( `msg`.`created_at`) as `created_on` ,UNIX_TIMESTAMP( `msg`.`updated_at`) as `updated_on`, IFNULL( `att`.`charge`, 0 ) as `charge` , `att`.`filename` , `att`.`is_paid`, `svr`.`cost` as `service_cost` ' );

		$this->db->from( $this->table.' as `msg` ' );
		$this->db->join( $this->join_table,' `att`.`id` = `msg`.`attachment_id` ', 'left');
		$this->db->join( $this->join_table3,' `msg`.`service_id` = `svr`.`id` ', 'left');


		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	

		$query = $this->db->get();


		if( $query->num_rows() > 0 ){			
				return $query->result();
		}
		else {
			return NULL;
		}
	}


	public function get_all( $user_id = null, $where = null, $order_by = null, $limit = null, $like = null ) {
		
		$this->db->where_in( '`msg`.`id`', $this->get_compiled_select_query( $user_id ), false );
		
		$this->db->select( '`msg`.`id`,`msg`.`group_id`, `msg`.`sender_id`,`msg`.`receiver_id`,`msg`.`attachment_id`,`msg`.`service_id`,`msg`.`message`, `msg`.`special_message`,`msg`.`status`,`msg`.`is_seen`,`msg`.`has_delivery`,`msg`.`is_home_address`,`msg`.`address`, UNIX_TIMESTAMP( `msg`.`created_at`) as `created_on` ,UNIX_TIMESTAMP( `msg`.`updated_at`) as `updated_on`, IFNULL( `att`.`charge`, 0 ) as `charge` , `att`.`filename` , `att`.`is_paid`, `usr`.`email`, `svr`.`cost` as `service_cost`  ');
		
		$this->db->from( $this->table.' as `msg` ' );
		$this->db->join( $this->join_table,' `att`.`id` = `msg`.`attachment_id` ', 'left');
		
		$this->db->join( $this->join_table2,' `usr`.`id` = `msg`.`receiver_id` ', 'left');
		$this->db->join( $this->join_table3,' `msg`.`service_id` = `svr`.`id` ', 'left');


		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	
		
		if( !is_null( $where ) && ( is_array( $where ) && count( $where ) > 0 ) ) {
			$this->db->where( $where );
		}		

		if( !is_null( $order_by )  ) {		

			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		} else {
			$this->db->order_by( '`msg`.`id`','desc' );			
		}

		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}
	
		$query = $this->db->get();

		if( $query->num_rows() > 0 ){
			
			return $query->result();
		}
		else {
			return NULL;
		}

	}

	public function get_all_count( $user_id = null, $where = null, $order_by = null, $like = null, $where_text = null ) {

		$this->db->or_where_in( 'id',  $this->get_compiled_select_query( $user_id ) , false );		

		$this->db->select( 'count( * ) as total_rows');
		$this->db->from( $this->table );

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}
		
		if( !is_null( $where ) && ( is_array( $where ) && count( $where ) > 0 ) ) {
			$this->db->where( $where );
		}

		if( !is_null( $where_text ) ) {
			$this->db->where( $where_text );

		}

		if( !is_null( $order_by )  ) {		

			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		} else {
			$this->db->order_by( 'id','desc' );			
		}
		
		
		$query = $this->db->get( /*$this->table */ );


		if( $query->num_rows() > 0 ){			
			return $query->row('total_rows');			
		} else {
			return 0;
		}
	}

	private function get_compiled_select_query( $user_id = NULL ){		
		$this->db->select_max( 'id' );
		$this->db->where( 'sender_id', $user_id );
		$this->db->or_where( 'receiver_id', $user_id );
		$this->db->group_by( 'group_id' );

		return $this->db->get_compiled_select( $this->table, TRUE );
	}

	public function get_thread( $group_id = NULL ) {
		
		/*$this->db->where( 'sender_id', $user_id );
		$this->db->or_where( 'receiver_id', $user_id );*/
		$this->db->order_by( 'id', 'ASC');
		$query = $this->db->get_where( $this->table, array('group_id' => $group_id) );
		if( $query->num_rows() > 0 ){			
			return $query->result();
		}
		else {
			return NULL;
		}
	}
	
}

/* End of file*/
