<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * message class.
 * 
 * @extends CI_Controller
 */
class Message extends MY_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {		
		parent::__construct();
		$this->load->helper( 'users/user' );				
		$this->load->model('message_model');
		$this->load->model('attachment/attachment_model','attachment_model');
		$this->load->model('services/service_model','service_model');

	}
	public function index() {}
	

	public function message_get( $message_id = false, $status = 'unread', $page = false, $search = false ) {
		$this->load->config('pagination');

		$current_user      = $this->current_user;
        $current_user_role = $current_user->role;
        $current_user_id   = $current_user->userId;

	   	$data = array();
	   	$notifyRes = NULL;

        if( is_null( $current_user_role ) ){
        	
            $data['status'] = 403;
            $data['code']   = "USR010";
            $this->response( $data );
            return;
        }

        $row = NULL;

    	if( $message_id !== false  && ( is_numeric( $message_id ) && $message_id != 0 ) ){

    		$notifyRes = $this->message_model->get( array( '`msg`.`id`' => $message_id ) );
    		if( $notifyRes[0]->address != '') {
	    		$address = $notifyRes[0]->address;
	    		$temp = array();
	    		foreach( unserialize( $address ) as $key => $val ) {
	    			$temp[] = array( $key => $val );
	    		}
	    		$notifyRes[0]->address = $temp;
    			
    		}

    		$row = 1;
    	}else {
    		$where = NULL;
    		if( 'all' != $status ){
	    		$where = array(
			    			'status' => $status
		    			);
    		}

    		$order_by = array(
    						'`msg`.`id`' => 'DESC'
    					);

    		$limit = array();

    		if(  false !== $page ) {
    			$page = ( $page < 0 ) ? 1: $page;
	    		$limit['per_page'] = $this->config->item('per_page');
	    		$limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
    		}

    		$like = NULL;

    		if( false !== $search ) {
    			$like = array(
    					'message' => $search
    				);
    		}

	   		$notifyRes = $this->message_model->get_all( $current_user_id, $where, $order_by, $limit, $like );
	   		if( count( $notifyRes ) > 0 ) {
		   		foreach( $notifyRes as $key => $val ) {
		   			if( $val->address != '' ) {
			   			
			   			$address = unserialize( $val->address );
			   			$temp = array();
			   			foreach( $address as $k => $v ) {
			   				$temp[] = array( 
			   							 'title'	=> $k ,
			   							  'value'   => $v
			   							);
			   			}	   				

		   				$notifyRes[$key]->address = $temp;
		   			}
		   		}
	   			
	   		}


	   		$order_by = array(
    						'id' => 'DESC'
    					);
	   		$row = $this->message_model->get_all_count( $current_user_id, $where, $order_by, $like );

    	}
	   	
	   	if( $notifyRes ){
	   		$data['status']     = 200;
	   		$data['code']       = "MSG003";
	   		$data['total_rows'] = $row;
	   		$data['data']       = $notifyRes;
	   	} elseif ( is_null( $notifyRes ) ) {
	   		$data['status']     = 200;
	   		$data['code']       = "NOT_FOUND";
	   		$data['data']       = [];
	   		$data['total_rows'] = 0;
	   	} else {
	   		$data['status']     = 400;
	   		$data['code']       = "MSG004";
	   		$data['data']       = [];
	   		$data['total_rows'] = 0;
	   		// $data['data']     = 'There was a problem listing Notifications. Please try again.';
	   	}
	   	
		$this->response( $data );
	}

	public function message_post(){

        $current_user = $this->current_user;
        $current_user_role = $current_user->role;
        $current_user_id = $current_user->userId;
        $current_user_email = $current_user->email;
        
        $response = '';
		$data = $insertArr = array();
		/*$_POST = json_decode( trim( file_get_contents('php://input') ), true );*/
		/*file_put_contents( APPPATH . '../logs/post.txt',print_r( $_POST, true ) );*/
		
	 	$upload_arr  = array();
	 	$uploaded_data = $this->uploader();
	 	
	 	$attachment_id = NULL;
		$is_has_note = false;
		$has_delivery = false;
		
		$service_id = $this->input->post('service_id');
		$service_detail = $this->service_model->get( array('id' => $service_id ) );

		if( count($service_detail ) > 0 ){
			if( $service_detail[0]->has_note == 1 ){
				$is_has_note = true;
			}
		 	if( $service_detail[0]->has_delivery == 1 ) {
		 		$has_delivery = true;
		 	}

		}
		$charge = NULL;
	 	if ( is_array( $uploaded_data) && count( $uploaded_data ) > 0 ) {
	 		$charge = $this->input->post('charge');

			$upload_arr['is_paid']       = 0; // 1 paid | 0 not paid
			$upload_arr['charge']        = isset( $charge ) ? $charge : 0;
			$upload_arr['is_archive']    = 0;  // 1 archive  | 0  not archived
			$upload_arr['filename'] = $uploaded_data['file_name'];
			
			if( count($service_detail ) > 0 ){
				
				if( $upload_arr['charge'] == 0 ){
					$upload_arr['charge'] = $charge = $service_detail[0]->cost;
				}
			}
			
			if( $this->attachment_model->insert( $upload_arr ) ){
				$attachment_id = $this->db->insert_id() ;
			} 
		} 

		if( 'completed' == $this->input->post('status') && is_null( $attachment_id ) ) {
			/* does nothing*/		  
			$response = FALSE;
			$data['data'] = $uploaded_data;

		} else {
		 	$group_id = $this->input->post('group_id');

	        /*Check for user role and message status and insert data accordingly*/ 
	        $insertArr = array(		         
	            'sender_id'     => $this->input->post('staff_id'),
	            'receiver_id'   => $this->input->post('customer_id'),
	            'attachment_id' => $attachment_id,
	            'service_id'    => $this->input->post('service_id'),
	            'group_id'      => $group_id,
	            'message'       => $this->input->post('message'),
	            'is_seen'       => 0, /*0 => unseen  and 1: seen */
	            'status'        => $this->input->post('status')
	        );

	        if( $is_has_note ){
	        	$insertArr['special_message'] = $this->input->post('special_message');		        	
	        }

	        if( $has_delivery ) {

	        	$insertArr['has_delivery'] = $has_delivery;
	        	$address = NULL;
	        	$userAddress = $this->input->post('userAddress');
	        	if ( strcmp( 1, intval( $userAddress['isHomeAddress'] ) ) == '0' ){
	        		/* checking if home ad dress completed or not*/
    				$insertArr['is_home_address'] = 1;
		        	$address = array(
		        						'address_line1'   => isset( $userAddress['address'] )? $userAddress['address'] : '',
		        						'address_line2'   => isset( $userAddress['address2'] )? $userAddress['address2'] : '',
		        						'city'            => isset( $userAddress['city'] )? $userAddress['city'] : '',
		        						'state'           => isset( $userAddress['state'] )? $userAddress['state'] : '',
		        						'country'         => isset( $userAddress['country'] )? $userAddress['country'] : '',
		        						'zip'             => isset( $userAddress['zip'] )? $userAddress['zip'] : '',
		        						'phone'           => isset( $userAddress['contact_no'] )? $userAddress['contact_no'] : ''
		        					);	      
	        		        		
	        		
	        	} else {
    				$insertArr['is_home_address'] = 0;	        		
		        	$address = array(				        				
		        						'address_line1'        => isset( $userAddress['address'] )? $userAddress['address'] : '',
		        						'address_line2'        => isset( $userAddress['address2'] )? $userAddress['address2'] : '',
		        						'city'                 => isset( $userAddress['city'] )? $userAddress['city'] : '',
		        						'state'                => isset( $userAddress['state'] )? $userAddress['state'] : '',
		        						'country'              => isset( $userAddress['country'] )? $userAddress['country'] : '',
		        						'zip'                  => isset( $userAddress['zip'] )? $userAddress['zip'] : '',
		        						'phone'                => isset( $userAddress['contact_no'] )? $userAddress['contact_no'] : ''
		        					);	        		
	        	}

	        	$insertArr['address'] = serialize( $address );

	        }

	        if(!empty($insertArr) ){
	            $response = $this->message_model->insert( $insertArr );

	            /*Set group id when its null */
	            if( $group_id == NULL || $group_id == 0 ) {

	            	$id = $this->db->insert_id();
	            	if( $id ){
	            		$this->message_model->update( array( 'group_id' => $id ), array( 'id' => $id )  );
	            		$insertArr[ 'group_id' ] = $id;
	            	}
	            }	            
	            /*sending email*/
	            
	            $get_user = NULL;
	            if( 'staff' == $current_user_role ){
		            $get_user = $this->user_model->get( array( 'id' => $insertArr['receiver_id'] ));

	            } else {
		            $get_user = $this->user_model->get( array( 'id' => $insertArr['sender_id'] ));
	            }
	            if( count( $get_user ) > 0 ) {
	            	$receiver_email = $get_user[0]->email;
	            }
	            $url = $this->get_url_http();
	            $username = get_meta_data( $get_user[0]->id, 'nick_name', true );
	            $template_data = NULL;
	            if( 'pending' == $this->input->post('status') ) {
    		            $template_data = array(
    		            		'page_title'         => 'New Message Arrived',
    		            		'name'				 => ( $username != '' ) ? $username : $receiver_email,
    		            		'body_data'  		 => 'A Customer (' . $current_user_email . ') has requested you following service.',
    		            		'service'			 =>	$service_detail[0],
    		            		'charge' 			 => ( $charge > 0 ) ? $charge : $service_detail[0]->cost,
    		            		'message'            => $insertArr['message'],
    		            		'show_token'         => true,
    	                        'verification_url'   => $url,
    	                        'verification_label' => 'Click here to login',
    	                        'footer_text'        => $this->get_email_footer_section()
    		            	);
    		            if( $is_has_note ){
    		             	$template_data['special_title'] = 'Here is the special message snippet';
				        	$template_data['special_message'] = $insertArr['special_message'];		        	
				        }
				        if( $has_delivery ) {
				        	$template_data['delivery_title'] = 'Delivered to:';
				        	$template_data['address'] = unserialize( $insertArr['address'] );
				        }

	            } else if ('unpaid' == $this->input->post('status') ) {
	            	 $template_data = array(
	            		'page_title'         => 'Ready for Download',
	            		'name'				 => ( $username != '' ) ? $username : $receiver_email,
	            		'body_data'  		 => 'Your request is ready for download',
	            		'is_completed'		 => 1,
	            		'service'			 =>	$service_detail[0],
	            		'charge' 			 => ( $charge > 0 ) ? $charge : $service_detail[0]->cost,
	            		'message'            => $insertArr['message'],
	            		'show_token'         => true,
	                    'verification_url'   => $url,
	                    'verification_label' => 'Click here to login',
	                    'footer_text'        => $this->get_email_footer_section()
	            	);
	            } else {

		            $template_data = array(
		            		'page_title'         => 'New Message Arrived',
		            		'name'				 => ( $username != '' ) ? $username : $receiver_email,
		            		'body_data'  		=> 'You had received a new message. Here is the message snippet below',
		            		'message'            => $insertArr['message'],
		            		'show_token'         => true,
	                        'verification_url'   => $url,
	                        'verification_label' => 'Click here to login',
	                        'footer_text'        => $this->get_email_footer_section()
		            	);
	            }
	            $message_email_view = $this->load->view( 'message_email_view', $template_data, true );
	            $emailRes = $this->_send_email( $message_email_view, $receiver_email , $template_data['page_title'] ); 
	            $data['email'] = ($emailRes)?'ok':'404';

	        }
		}

		if ( $response ) {
			
			$data['status'] = 200;
			$data['code']  = 'MSG001';
			if( isset( $inserAdr['address'] ) ) {
				$insertArr['address'] = unserialize( $insertArr['address'] );				
			}
			$data['data'] = $insertArr;
		} else if( is_null( $attachment_id) ){
			$data['status'] = 400;
			$data['code'] = 'ATT012';			
		} else {
			$data['status'] = 503;
			$data['code'] = 'MSG002';			
		}		 	
		
		$this->response( $data );
	}

	public function message_put( $message_id = false ){

		if( !$message_id ){

			$this->response( array( 'status' => 404 , 'code' => 'MSG008', 'data' => [] ) );
		}

        $current_user = $this->current_user;
        $current_user_role = $current_user->role;
        $current_user_id = $current_user->userId;

        $response = '';
		$data = $updateArr = array();
		$_POST = json_decode( trim( file_get_contents('php://input') ), true );

	    $this->load->library('form_validation');

  		$rules = $this->_form_rules();

  		$this->form_validation->set_rules($rules);

	  	
        /*Check for user role and message status and insert data accordingly*/ 
        $updateArr = array(
           // 'parent_id'      => $this->input->post('parent_id'),
            'sender_id'     => $this->input->post('staff_id'),
            'receiver_id'   => $this->input->post('customer_id'),
            'attachment_id' => $this->input->post('attachment_id'),
            'service_id'    => $this->input->post('service_id'),
            'group_id'      => $this->input->post('group_id'),
            'message'       => $this->input->post('message'),
            'is_seen'        => 0, /*0 => unseen  and 1: seen */
            'status'        => $this->input->post('status') 
        );

        if(!empty($updateArr)){
            $response = $this->message_model->update( $updateArr, array( 'id' => $message_id ) );
        }

		if ( $response ) {
			$data['status'] = 200;
			$data['code']  = 'MSG001';
			$data['data'] = $updateArr;
		} else {
			$data['status'] = 503;
			$data['code'] = 'MSG002';			
		}

  		
		$this->response( $data );
	}

	public function message_delete(){
		// delete
	   	$data = array();

		$message_id = $this->uri->segment( $this->uri->total_segments(), false  );

		if ( is_numeric( $message_id ) && $message_id != false) {
			$deleteRes = $this->message_model->delete( array('id' => $message_id ) ); 

			if( $deleteRes ){
				$data['status']   = 200;
				$data['code'] = 'MSG005';
			} else {
				$data['status'] = 400;
				$data['code'] = 'MSG006';
			}
			
		} else {
			$data['status'] = 403;
			$data['code'] = 'MSG007';
		}
		$this->response( $data );
	}

	public function message_read_put( $message_id = NULL ){
		$data = array();
		if( is_null( $message_id ) &&!is_numeric( $message_id ) ){

			$data['status'] = 400;
			$data['code']   = 'MM007';
		} else {
			$notifyRes = $this->message_model->get( array( '`msg`.`id`' => $message_id ) );
			
			if( !is_null( $notifyRes ) ||  $notifyRes ){
				$updateArr = array( 'is_seen' => 1 );
				$response = $this->message_model->update( $updateArr, array( 'id' => $message_id )  );
				$data['status'] = 200;
				$data['code']   = 'MSG013';
				
			} else {
				$data['status'] = 400;
				$data['data'] = 'asdf';
				$data['code']   = 'MM007';
			}


		}
		$this->response( $data );
	}

	public function message_count_get() {
		$data = array();
		$current_user = $this->current_user;
		$current_user_role = $current_user->role;
		$current_user_id = $current_user->userId;

		$data['status'] = 200;
		$data['code'] = 'MSG014';
		$data['data'] = array(
										'total'     => $this->_message_count_calc('total'),
										'completed' => $this->_message_count_calc('completed'),
										'unpaid' 	=> $this->_message_count_calc('unpaid'),
										'pending'   => $this->_message_count_calc('pending'),
										'unseen'    => $this->_message_count_calc('unseen'),
										'unread'    => $this->_message_count_calc('unread')
								);
		$this->response( $data );
	}

	private function _message_count_calc( $option = false ){
		$where = $where_text = NULL;
		$current_user = $this->current_user;
		$current_user_role = $current_user->role;
		$current_user_id = $current_user->userId;

   		$order_by = array(
						'id' => 'DESC'
					);

   		if ( "completed" == $option ){
   			
   			$where['status'] = 'completed';

   		} else if ( "unread" == $option ){

   			$where['status'] = 'unread';

   		} else if ( "pending" == $option ){

   			$where['status'] = 'pending';
   		} else if ( "unpaid" == $option ){

   			$where['status'] = 'unpaid';
   		} else if ( "unseen" ==  $option ){
	   		
	   		$where['is_seen'] = 0 ;

	   		if ( "staff" == $current_user_role ){

	   			$where['status'] = 'pending';

	   		} else if ( "customer" == $current_user_role ){
	   			$where_text = " (`status`= 'unread' OR `status`='unpaid' OR `status`='completed')";	   			
	   		}

   		}
   			$query = " (`sender_id`= '" . $current_user_id . "' OR `receiver_id`='" . $current_user_id . "')";   			
   		
   		if ( is_null ( $where_text ) ) {
   			$where_text = $query;
   		} else {
   			$where_text .= ' AND '. $query;
   		}
		$this->db->reset_query();
		$row = $this->message_model->get_all_count( $current_user_id, $where,null,null, $where_text );		
		if( "total" == $option ) {			
			// echo $this->db->last_query();
		}

		return $row;
	}

	private function _form_rules() {
	    $rules = array(
	        array(
	            'field' => 'staff_id',
	            'label' => 'Staff ID',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'MSG008',
	                                        'field'   => 'customer_id'
	                                    )
	            )
	        ), array(
	            'field' => 'customer_id',
	            'label' => 'Customer ID',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'MSG008',
	                                        'field'   => 'MSG012'
	                                    )
	            )
	        ),array(
	            'field' => 'attachment_id',
	            'label' => 'Attachment Id',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'MSG009',
	                                        'field'   => 'attachment_id'
	                                    )
	            )
	        ),array(
	            'field' => 'message',
	            'label' => 'Message',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'MSG010',
	                                        'field'   => 'message'
	                                    )
	            )
	        ),array(
	            'field' => 'status',
	            'label' => 'Status',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'MSG011',
	                                        'field'   => 'status'
	                                    )
	            )
	        )
	        
	    );
	    
	    return $rules;
	}

	public function recent_get( $how_many ){

		$current_user      = $this->current_user;
        $current_user_id   = $current_user->userId;

		$data = array(
			'status' => 404,
			'code'	 => '',
			'data'   => []
		);

		$limit = array(
			'per_page' => $how_many,
			'offset'   => 0 
		);

		$order_by = array( 'id' => 'DESC' );

		$res = $this->message_model->get_all( $current_user_id, null, $order_by, $limit );

		if( $res ){

			$data['status'] = 200;
			$data['code'] = '';
			$data['data'] = $res;  
		}

		$this->response( $data );
	}

	public function message_thread_get( $group_id ){
		$data = NULL;

		if( !is_null( $group_id ) && is_numeric( $group_id ) ) {
			$thread = $this->message_model->get_thread( $group_id );
			
			if( $thread ) {

				foreach( $thread as $msg ):
					$msg->sender = $this->_getAppenUserDetail( $msg->sender_id );
					$msg->receiver = $this->_getAppenUserDetail( $msg->receiver_id );
					if( !is_null( $msg->attachment_id ) ) {
						$get_attach = $this->attachment_model->get( array('id' => $msg->attachment_id ));
						if( count( $get_attach ) > 0 ){
							$get_attach = $get_attach[0];
							$filename = $get_attach->filename;
							$msg->filename = $filename;
							$msg->file_cost = $get_attach->charge;
							$msg->file_size = $this->human_readable_filesize( $filename );
						}
					}

					
				endforeach;


				$data['status'] = 200;
				$data['code'] = 'MSG016';
				$data['data'] = $thread;
			} else {
				$data['status'] = 400;
				$data['code'] = 'MSG017';
			}

		} else {
			$data['status'] = 400;
			$data['code'] = 'MSG018';
		}

		$this->response( $data );
	}

	private function _getAppenUserDetail( $user_id ) {

		$this->load->model( 'users/user_model', 'user_model' );
		$detail= $this->user_model->get( array( 'id' => $user_id ));
		
		if( count( $detail ) > 0 ) {
			$detail[0]->meta = $this->get_meta_by_id( $user_id );
			return $detail[0];
		}	
		return NULL;
	}

}
/* End of file*/
