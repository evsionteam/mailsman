<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['message']['GET'] = "message/message/message";

$route['message/recent/(:num)']['GET'] = "message/message/recent/$1";

$route['message/(:num)']['GET'] = "message/message/message/$1";

/*message/{noID=0}/{status}*/
$route['message/(:num)/(:any)']['GET'] = "message/message/message/$1/$2";

/*message/{noID=0}/{status}/{page}*/
$route['message/(:num)/(:any)/(:any)']['GET'] = "message/message/message/$1/$2/$3";

/*message/{noID=0}/{status}/{page}/{search}*/
$route['message/(:num)/(:any)/(:any)/(:any)']['GET'] = "message/message/message/$1/$2/$3/$4";

$route['message']['POST'] = "message/message/message";

$route['message/(:num)']['PUT'] = "message/message/message/$1";

$route['message_read/(:num)']['PUT'] = "message/message/message_read/$1";

$route['message_count']['GET'] = "message/message/message_count";

$route['message/(:num)']['DELETE'] = "message/message/message/$1";

$route['message/message_thread/(:any)']['GET'] = "message/message/message_thread/$1";
