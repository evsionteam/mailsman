<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * payment class.
 * 
 * @extends MY_Controller
 */

use \Stripe\Stripe;
use \Stripe\Plan;

class Subscribe extends MY_Controller {
	protected $stripe_config;
	public function __construct() {
		parent::__construct();

		if( 'administrator' != $this->current_user->role ){
			$this->response( array( 'status' => 400 , 'code' => 'DENIAL' ) );
		}

		$this->stripe_config = $this->load->config('stripe', TRUE );			
		Stripe::setAPiKey( $this->stripe_config['stripe_key_secret'] );

		$this->load->model('subscribe_model');
	}

	public function subscribe_get(){}

	public function subscribe_post(){

		$_POST = json_decode( trim( file_get_contents('php://input') ), true );
		$data = array();
		/*"amount" => 5000,
		  "interval" => "month",
		  "interval_count" => 1 
		  "name" => "Ruby large",
		  "currency" => "usd",
		  "id" => "ruby-large"*/
		  /* 'day','week','month','year'*/
		$stripe_args = array(
								'id'       => strtolower( str_replace( ' ', '-', $this->input->post('name') ) ) ,
								'name'     => $this->input->post('name'),
								'amount'   => $this->_stripe_amount_calc ( $this->input->post('amount') ), /* for stripe*/
								'currency' => $this->input->post('currency'),
								'interval' => $this->input->post('interval'),
								'interval_count'  => $this->input->post('interval_count')
							 );
		
		$interval_count = $this->input->post('interval_count');
		 
		if( isset( $interval_count ) && is_numeric( $interval_count ) && $interval_count > 0 ) {
 				$stripe_args['interval_count']  = $this->input->post('interval_count');
		 }
		$stripe_subscribe_add = $this->_stripe_plan_create(  $stripe_args );
		
		if( FALSE !== $stripe_subscribe_add && is_object( $stripe_subscribe_add )  ) {
			
			$insertArr = array(
				'name'  => $this->input->post('name'),
				'description'  => $this->input->post('description'),
				'amount'  => $this->input->post('amount'),
				'currency'  => $this->input->post('currency'),
				'interval_count'  => $this->input->post('interval_count'),
				'interval'  => $this->input->post('interval'),
				'stripe_subscribe_id'  => $stripe_subscribe_add->id,
				'paypal_subscribe_id'  =>  NULL,
				'status'  => $this->input->post('status') /*1: enable  0 enable*/
				);

			$response = $this->subscribe_model->insert( $insertArr );

			if ( $response ) {
				$data['status'] = 200;
				$data['code'] = 'SUB001';
				$data['data'] = $insertArr;
			} else {
				$data['status'] = 400;
				$data['code'] = 'SUB002';
			}
			
		} else {
			$data['status'] = 400;
			$data['code'] = 'SUB003';
		}


		$this->response( $data );
	}

	public function subscribe_put( $sid = FALSE ){
		$data = NULL;
		$_POST = json_decode( trim( file_get_contents('php://input') ), true );
		
		if( FALSE != $sid && is_numeric( $sid ) ){
			$retrieve = $this->subscribe_model->get( array( 'id' => $sid ) );
			if( $retrieve ){
				$stripe_plan_id = $retrieve[0]->stripe_subscribe_id; 
				$stripe_args = array(										
										'name'     => $this->input->post('name')										
									 );				
				/* only name/ metadata /trial_period_days / statement_descriptor can be updated not others*/
				$stripe_subscribe_update = $this->_stripe_plan_update(  $stripe_args, $stripe_plan_id );

				// $this->response( $stripe_subscribe_update );
				if( FALSE !== $stripe_subscribe_update && is_object( $stripe_subscribe_update )  ) {
					
					$updateArr = array(
						'name'  => $this->input->post('name'),						
						'status'  => $this->input->post('status') /*1: enable  0 enable*/
						);

					$response = $this->subscribe_model->update( $updateArr, array( 'id' => $sid ) );

					if ( $response ) {
						$data['status'] = 200;
						$data['code'] = 'SUB001';
						$data['data'] = $updateArr;
					} else {
						$data['status'] = 400;
						$data['code'] = 'SUB002';
					}
					
				} else {
					$data['status'] = 400;
					$data['code'] = 'SUB006';
				}

			} else {
				$data['status'] = 400;
				$data['code'] = 'SUB005';
			}
		} else {
			$data['status'] = 400;
			$data['code'] = 'SUB004';
		}
		$this->response( $data );
	}

	public function subscribe_delete( $sid = NULL ){		
		$data = NULL;
		if( FALSE != $sid && is_numeric( $sid ) ){
			$retrieve = $this->subscribe_model->get( array( 'id' => $sid ) );
			if( $retrieve ){

				$stripe_plan_id = $retrieve[0]->stripe_subscribe_id; 				
				$stripe_plan_id = 'plan-a'; 				
				$stripe_subscribe_delete = $this->_stripe_plan_delete( $stripe_plan_id );
				if( TRUE ==  $stripe_subscribe_delete['deleted'] ){
					$response=  $this->subscribe_model->delete(  array('id' => $sid ) );
					if( $response ){
						$data['status'] = 200;
						$data['code'] = 'SUB008';
					} else {
						$data['status'] = 400;
						$data['code'] = 'SUB009';
					}
				} else {
					$data['status'] = 400;
					$data['code'] = 'SUB007';
					$data['data'] = $stripe_subscribe_delete;
				}

			} else {
				$data['status'] = 400;
				$data['code'] = 'SUB005';
			}
		} else {
			$data['status'] = 400;
			$data['code'] = 'SUB004';
		}
		$this->response( $data );
	}


	protected function _stripe_plan_create( $data = NULL ){
		try{			
			$sub = Plan:: create( $data );
			return $sub;
		} catch( Exception $e ){
			 return FALSE;
		}
	}

	protected function _stripe_plan_update( $data = NULL, $plan_id = NULL ){
		/* only name/ metadata /trial_period_days / statement_descriptor can be updated not others*/
		try{			

			if( is_null( $plan_id ) ) {
				return FALSE;
			}		
			$p = Plan::retrieve( $plan_id );
			
			foreach( $data as $key => $value ){
				$p->{$key} = $value;				
			}			
			return $p->save();			
			
		} catch( Exception $e ){			
			 return FALSE;
		}
	}

	protected function _stripe_plan_delete( $plan_id = NULL ) {
		try {
			if( is_null( $plan_id ) ) {
				return FALSE;
			}	
			$plan = Plan::retrieve( $plan_id);
			 return $plan->delete();
		} catch( Exception $e) {
			return FALSE;
		}
	}

	protected function _stripe_amount_calc( $amount = NULL ){
		if( !is_null( $amount ) && is_numeric( $amount ) && $amount > 0 ){
			return $amount * 100;
		} 
		return 0;
	}
}
