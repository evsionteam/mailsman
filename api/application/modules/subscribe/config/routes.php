<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['subscribe']['POST'] = 'subscribe/subscribe/subscribe';
$route['subscribe/(:num)']['PUT'] = 'subscribe/subscribe/subscribe/$1';
$route['subscribe/(:num)']['DELETE'] = 'subscribe/subscribe/subscribe/$1';