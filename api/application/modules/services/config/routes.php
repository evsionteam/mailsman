<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['services']['GET'] = "services/services/services";

$route['services/(:num)']['GET'] = "services/services/services/$1";

/* services/{noID=0}/{page}/ =>pagination */
$route['services/(:num)/(:any)']['GET'] = "services/services/services/$1/$2";

/* services/{noID=0}/{page}/{search}  =>search */
$route['services/(:num)/(:any)/(:any)']['GET'] = "services/services/services/$1/$2/$3";

$route['services']['POST'] = "services/services/services";

$route['services/(:num)']['PUT'] = "services/services/services/$1";

$route['services/(:num)']['DELETE'] = "services/services/services/$1";