<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Service_model class.
 * 
 * @extends Base_model
 */
class Service_model extends Base_model {

    
    public function __construct() {
        parent::__construct();
	   $this->table = $this->prefix . "services";
	}
	

    /**
     * get_service function.
     *
     * @access public
     * @param  [array] $where    [description]
     * @param  [array] $order_by [description]
     * @return object the service object
     */
    public function get_service( $where = NULL, $order_by = NULL) {

        if( !is_null( $where ) ) {
            $this->db->where( $where );
        }
        if( !is_null( $order_by ) ) {
            $this->db->order_by( $order_by );
        }

        $query = $this->db->get( $this->table );

        if( $query->num_rows() >= 1 )
            if( $query->num_rows() == 1 )
                return $query->row();
            else
                return $query->result();
        else
            return NULL;
    }
	
}

/* End of file*/
