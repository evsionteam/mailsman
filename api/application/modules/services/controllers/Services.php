<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Services class.
 * 
 * @extends CI_Controller
 */
class Services extends MY_Controller {
	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();		
		$this->load->model('service_model');		
	}

	public function index() {}


  
    /*Insert Service*/
    public function services_post(){

        $data = array();
        $current_user = $this->current_user;
        $current_user_role = $current_user->role;
        $_POST = json_decode( trim( file_get_contents('php://input') ), true );

        $this->load->library('form_validation');

        $rules = $this->_form_rules();

        $this->form_validation->set_rules($rules);

        if ( $this->form_validation->run() == FALSE ) {
            $errors = $this->form_validation->error_array();
            $err = array();

            foreach( $errors as $error => $err_val  ):
                $err[] = $err_val;
            endforeach;

            $data['status'] = 400;
            $data['data']  = $err;

         } else {
            /*Check for user role. Only admin staff can enter the services.*/
            if( "administrator" == $current_user_role || "staff" == $current_user_role ){
                
                $insertArr  = array();
                $insertArr['title']         = $this->input->post('title');
                $insertArr['description']   = $this->input->post('description');
                $insertArr['cost']          = $this->input->post('cost');
                $insertArr['has_note']      = $this->input->post('has_note');
                $insertArr['has_delivery']  = $this->input->post('has_delivery');
                if( $this->input->post('position') ){
                    $insertArr['position']  = $this->input->post('position');
                } else {
                    $insertArr['position']  =  $this->_check_position() + 1;
                }

                $response = $this->service_model->insert( $insertArr );

                $ins_id = $this->db->insert_id();

                if( $ins_id ){

                    $res = $this->service_model->get( array( 'id' => $ins_id ) );
                    $row = $res[0];
                }

                if ( $response ) {
                    $data['status'] = 201;
                    $data['data'] = $row;
                    $data['code'] = "SRV011";
                } else {
                    $data['status'] = 503;
                    $data['code'] = "SRV001";
                    $data['data'] = [];
                }
            }else{
                $data['status'] = 403;
                $data['code'] = 'SRV002';
                $data['data'] = [];
            }
            
         }
        /**/

        $this->response( $data );
    }
    /**/


  
    /*Update Service*/
    public function services_put( $service_id = NULL){

        $data = array();
        $current_user  = $this->current_user;
        $current_user_role = $current_user->role;

        $_POST = json_decode( trim( file_get_contents('php://input') ), true );

        $this->load->library('form_validation');

        $rules = $this->_form_rules();

        $this->form_validation->set_rules($rules);

        if ( $this->form_validation->run() == FALSE ) {
            $errors = $this->form_validation->error_array();
            $err = array();

            foreach( $errors as $error => $err_val  ):
                $err[] = $err_val;
            endforeach;

            $data['status'] = 400;
            $data['code'] = 'SRV014';
            $data['data']  = $err;

        } else {

            /*Check for user role. Only admin staff can update the services.*/
            /*$service_id = $this->uri->segment( $this->uri->total_segments(), false );*/
        
            if( (is_numeric( $service_id ) && $service_id !== false) && ( "administrator" == $current_user_role ) ){
               
                $checkServiceExists = $this->service_model->get_service( array('id'=> $service_id ) );
               
                if( $checkServiceExists ) {
                    $updateArr  = array(
                        'title'        => $this->input->post('title'),
                        'description'  => $this->input->post('description'),
                        'cost'         => $this->input->post('cost'),
                        'has_note'     => $this->input->post('has_note'),
                        'has_delivery' => $this->input->post('has_delivery')

                    );
                    if( $this->input->post('position') ){
                        $updateArr['position']  = $this->input->post('position');
                    } else {
                        /* assign ol d position*/
                        $updateArr['position']  =  $checkServiceExists->position;
                    }
                    $where = array('id'=>$service_id);
                    $response = $this->service_model->update( $updateArr, $where );
                    
                    $res = $this->service_model->get( array( 'id' => $service_id) );

                    if($response){
                        $data['status'] = 200;
                        $data['data'] = $res[0];
                        $data['code'] = "SRV013";
                    } else {
                        $data['status'] = 503;
                        $data['code'] = 'SRV004';
                        $data['data'] = 'There was a problem while updating. Please try again.';
                    }

                } else {
                    $data['status'] = 404;
                    $data['code'] = 'SRV003';
                    $data['data'] = 'Service not found. Please try again.';
                }
            } else {
                $data['status'] = 403;
                $data['code'] = 'SRV002';
                $data['data'] = 'You don\'t have permission.';
            }
            
        }

        $this->response( $data );
    }
    /**/


  
    /*Delete Service*/
    public function services_delete(  $service_id = NULL ){

        $data = array();
        $current_user = $this->current_user;
        $current_user_role = $current_user->role;

      /*  $service_id = $this->uri->segment( $this->uri->total_segments(),false);*/

        if ( (is_numeric( $service_id ) && $service_id !== false) && ( "administrator" == $current_user_role ) ) {
            //  is_archive  is set to 1 to flagged as deleted
            $checkServiceExists = $this->service_model->get_service( array('id'=> $service_id ) );

            $this->load->model('message/message_model','message_model');

            $msg_res = $this->message_model->get( array( '`msg`.`service_id`' => $service_id ) );

            if( $checkServiceExists && is_null( $msg_res ) ){
                $deleteRes = $this->service_model->delete( array('id' => $service_id ) );
                if( $deleteRes ){
                    $data['status'] = 200;
                    $data['code']   = "SRV012";
                }else{
                    $data['status'] = 503;
                    $data['code'] = 'SRV005';
                    $data['data'] = 'There was a problem deleting service. Please try again.';
                }
            }else{
                $data['status'] = 404;

                $data['code'] = 'SRV003';

                if( !is_null( $msg_res ) ){
                    $data['code'] = 'SRV015';
                }

                $data['data'] = 'Service not found.';
            }
        }else{
            $data['status'] = 404;
            $data['code'] = 'SRV002';
            $data['data'] = 'You don\'t have permission.';
        }
        $this->response( $data );
    }
    /**/
   
	public function services_get( $service_id = false, $page = false, $search = false ) {
        $this->load->config('pagination');

	   	$data = array();
	   	
        $where = NULL;
        $like = NULL;
        
        if(  $service_id !== false  && ( is_numeric( $service_id ) && $service_id != 0 ) ) {
            $where['id'] = $service_id ;
        }

        $order_by = array( 'position' => 'ASC' );

        $limit = array();
        if( false !== $page ) {
            $page = ( $page < 0 ) ? 1: $page;
            $limit['per_page'] = $this->config->item('per_page');
            $limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
        }

        if( false !== $search ) {
            $like = array( 'title' => $search );
        }

	   	$response = $this->service_model->get( $where, $order_by, $limit, $like );
        $count = $this->service_model->get_count( $where, $order_by, $like );

	   	if( is_null( $response ) ){
            $data['status'] = 200;
            $data['code'] = 'NOT_FOUND';
            $data['data'] = NULL;            
	   	} elseif( $response ){
            foreach( $response as $row){
               
                $row->name = $this->convertStringToCamelCase( $row->title );
            }
            $data['status'] = 200;
            $data['total_rows'] = $count;            
            $data['data']   = $response;
        } else{
            $data['status'] = 500;
            $data['code'] = 'SRV006';
            $data['data'] = 'There was a problem while listing services. Please try again.';
	   	}		

		$this->response( $data );
	}

    private function _form_rules() {
        $rules = array(
            array(
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'SRV007',
                                            'field'   => 'title'
                                        )
                )
            ),array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'SRV008',
                                            'field'   => 'description'
                                        )
                )
            ),array(
                'field' => 'cost',
                'label' => 'Cost',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'SRV009',
                                            'field'   => 'cost'
                                        )
                )
            ),array(
                'field' => 'has_note',
                'label' => 'Note',
                'rules' => 'trim|required',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'SRV010',
                                            'field'   => 'has_note'
                                        )
                )
            ),
            
        );

        return $rules;
    }

    private function _check_position() {        
        return $this->service_model->get_count();
    }

    private function convertStringToCamelCase( $string = NULL ) {
        $string = ucwords( $string );
        return  lcfirst ( str_replace( ' ', '', $string ) );
    }
}
