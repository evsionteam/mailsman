<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * payment class.
 * 
 * @extends MY_Controller
 */

use \Stripe\Stripe;
use \Stripe\Charge;
use \Stripe\Customer;
use \Stripe\Subscription;

class Payment extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper( 'users/user' );				

		$this->load->model('users/user_model','user_model');
		$this->load->model('message/message_model','message_model');
		$this->load->model('services/service_model','service_model');


		$this->load->model('attachment/attachment_model','attachment_model');

		$this->load->model('payment_model');
		

		$this->stripe_config = $this->load->config('stripe', TRUE );            
		Stripe::setAPiKey( $this->stripe_config['stripe_key_secret'] );

		$this->load->library('paypal_lib');
	}	

	public function balance_check_get( ) {
		$data = NULL;	   
		/**
		 * date(DATE_ISO8601, strtotime("now"));
		 */

		$current_user = $this->current_user;
		$_POST = json_decode( trim( file_get_contents( 'php://input' ) ), TRUE );
		$get_user = $this->user_model->get( array( 'id' => $current_user->userId ));

		if( count( $get_user ) > 0 ) {
			$this->db->group_by('id');
			$get_transaction = $this->payment_model->get( array( 'user_id' => $current_user->userId ),array( 'id' => 'DESC' ) );
			
			$transaction = $get_transaction[0];
			$data['status'] = 200;
			$data['code']='';
			$data['data'] = $transaction;
		} else {
			$data['status'] = 400;
	        $data['code'] = 'USR008';
		}	

		$this->response( $data );
	}

	public function balance_load_post(){
	   $data = NULL;	   
	   /**
	    * date(DATE_ISO8601, strtotime("now"));
	    */

	   $current_user = $this->current_user;
	   $_POST = json_decode( trim( file_get_contents( 'php://input' ) ), TRUE );
	   $get_user = $this->user_model->get( array( 'id' => $current_user->userId ));
	   $flag = TRUE;
	   if( count( $get_user ) > 0 ) {
	        $user  =  $get_user[0];	   
	        $customerToken = $user->token;
        	$new_token = $this->input->post('token');
        	$new_token = ($new_token) ? $new_token :'';        	
	        /* balance amount to load*/
        	$load_amount = $this->input->post('load_amount');

	        if( "stripe" ==  $this->input->post('payment_mode') ) {
	        	if ( $customerToken != $new_token ) {
        			/*new token */        		
        			$customerToken = $this->input->post('token');
					$response = $this->_stripe_create_new_card ( $user->customer_id, $customerToken );

					if( $response['status'] ){
						/*update new response in db*/
						$update_token = $this->user_model->update(
						                                array(
						                                    'token'        => $customerToken,
						                                    'payment_mode' => $this->input->post('payment_mode')
						                                    ),
						                                array( 'id' => $current_user->userId )
						                            );
						if( $update_token ){
							$flag = TRUE;
						} else {
							$flag = FALSE;							
						}
						//$data['data'] = "I am here 1";

					} else {
						$create = array(
						                  "description" => "Subscriber=>". $user->email ."@". date('Y-m-d H:i:s'),
						                  "source" => $customerToken /*obtained with Stripe.js*/
						                );
						$response = $this->_stripe_new_create_customer( $create );
						$response = $response['result'];
						/*update new response in db*/
						$update_token = $this->user_model->update(
						                                array(
						                                	'customer_id'  => $response->id,
						                                    'token'        => $customerToken,
						                                    'payment_mode' => $this->input->post('payment_mode')
						                                    ),
						                                array( 'id' => $current_user->userId )
						                            );
						$user->customer_id = $response->id;
						if( $update_token ){
							$flag = TRUE;
						} else {
							$flag = FALSE;							
						}
					}
						
	        	}
					
	        	if( $flag ){
	    	        /* charge amount*/		    	       
	                $result = $this->_stripe_charge( array(
	        	        						'amount'      => $load_amount * 100, /* multiply by 100 only in stripe*/
	        	        						'currency'    => 'usd',
	        	        						'customer'      => $user->customer_id,
	        	        						'description' => 'Balance Loaded from '. $user->email .'@'. date('Y-m-d H:i:s')
	        						        ));

	                if( $result['status'] ) {
	                	$result = $result['result'];
	                	/*cheking if current user have last transaction or not*/
	                	$this->db->group_by('id');
	                	$get_transaction = $this->payment_model->get( array( 'user_id' => $current_user->userId ),array( 'id' => 'DESC' ) );
	                	
	                	if( count( $get_transaction ) > 0 ){
	                		$transaction = $get_transaction[0];
	                		$transaction_amount = $transaction->balance + $load_amount;	
	                	} else{
	                		// $transaction_amount = 0;
	                		$transaction_amount = $load_amount;
	                	}

	                	// saving loaded in db
	                	$insertArr = array(
	                		'user_id' => $current_user->userId,
	                		'transaction_type'=>'load',
	                		'balance' => $transaction_amount,
	                		'payment_method' =>  $this->input->post('payment_mode'),
	                		'transaction_id' => $result->id,
	                		'transaction_amount' => $load_amount,
	                		'transaction_status' => $result->status,
	                		'remarks' => date(DATE_ISO8601, strtotime("now"))
	                		);
	                	$insertArr['created_at'] = $insertArr['updated_at'] = date('Y-m-d H:i:s');
	                	$result = $this->payment_model->insert( $insertArr );

		    			/* email deduction*/
		    			$username = get_meta_data( $current_user->userId, 'nick_name', true );
		    			$url = $this->get_url_http();
			            $template_data = array(
			            		'page_title'         => 'Balance Loaded',
			            		'name'				 => ( $username != '' ) ? $username : $current_user->email,
			            		'body_data'  		 => 'Your have loaded balance of $' . $load_amount . '(USD).<br /> Your balance is $'.$transaction_amount .'(USD).',
			            		'show_token'         => true,
		                        'verification_url'   => $url,
		                        'verification_label' => 'Click here to login',
		                        'footer_text'        => $this->get_email_footer_section()
			            	);
			            $balance_email_view = $this->load->view( 'balance-email-view', $template_data, true );
			            $emailRes = $this->_send_email( $balance_email_view, $current_user->email , $template_data['page_title'] ); 
			            $data['email'] = ($emailRes)?'ok':'404';

	                	if( $result ) {
	                		$data['status'] = 200;
	                		$data['code'] = 'PAY005';
	                		$data['data'] = $insertArr;
	                	} else {
	                		$data['status'] = 400;
	                		$data['code'] = 'PAY006';
	                	}	

	                } else {
	                	$data['status'] = 400;
	                	$data['code'] = 'PAY002';
	                	$data['data'] = $result['result'];
	                }
	        		
	        	} else {
	        		$data['status'] = 400;
	        		$data['code'] = 'PAY009';
	        	}


	        }  else {
	        	/* paypal payment options*/
	        	$load_amount = $this->input->post('load_amount');

	        	$is_https = false;
	        	if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {		
	        		$is_https = true;
	        	}
	        	$url = 'http';
	        	$url .= ( ($is_https === true) ?'s':'') . '://' . $_SERVER['HTTP_HOST'];
	        	if(  $_SERVER['HTTP_HOST'] == "localhost" ) {
	        	  	$url .= '/mailsman/public';
	        	}
	        	$url .= '/#/';

	        	$returnUrl     = $url . 'payment';
        			
    			$paypal_args = array(
    			            'custom'      => json_encode( array('email' => $current_user->email,'flag' => $flag ) ),
    			            'currency'    => 'USD',
    			            'amount'      => (float)$load_amount,
    			            'description' => 'Balance load',
    			            'returnUrl'   =>  $returnUrl
      			        );
    	       	$payment = $this->paypal_lib->setPayment( $paypal_args );
	        	$data['status'] = $payment['code'];
	        	$data['code'] = 'PAY005';
	        	$data['data'] = $payment['message'];
	        }
	        
	        
	        /*$data['status'] = 200;
	        $data['code'] = '';
	        $data['data'] = $user;*/

	   } else {
	        $data['status'] = 400;
	        $data['code'] = 'USR008';
	   } 
	   $this->response( $data );
	}

	public function balance_deduct_post(){

		$current_user = $this->current_user;
		$_POST = json_decode( trim( file_get_contents( 'php://input' ) ), TRUE );
		$get_user = $this->user_model->get( array( 'id' => $current_user->userId ));

		if( count( $get_user ) > 0 ) {
		     $user  =  $get_user[0];	   
		     $customerToken = $user->token;

		    $attachment_id = $this->input->post('attachment_id');
		    
		    if( !is_null($attachment_id) && is_numeric($attachment_id) && $attachment_id > 0 ) {
		    	/*attachment*/
		    	$attach = $this->attachment_model->get( array('id' => $attachment_id ) );
		    	if( count( $attach ) >= 1 ){

		    		$amount = $attach[0]->charge;
		    		/**
		    		 *  donot remove it.
		    		 *  Used in activity monitor
		    		*/
		    		$_POST['load_amount'] = $amount;
    		    	$this->db->group_by('id');

    		    	$get_transaction = $this->payment_model->get( array( 'user_id' => $current_user->userId ),array( 'id' => 'DESC' ) );

    		    	if( $get_transaction ){
    		    		$transaction = $get_transaction[0];
    		    	}

		    		if( $attach[0]->is_paid == 1 && $get_transaction ){
		    			//Aready Paid
		    			$data['status'] = 200;
		    			$data['code']   = 'PAY010';
		    			$data['data'] = array(
			    							'balance' => $transaction->balance,
			    							'attachment_id' => $attachment_id
			    							);

		    		} else {
						$config_upload = $this->load->config('upload', TRUE );

		    			if ( $user->is_subscribe == 0 ){

		    				$data['status'] = 400;
		    				$data['code'] = 'PAY011';
		    			}  elseif('' != $attach[0]->filename && !file_exists(  $config_upload['upload_path'] . '/' . $attach[0]->filename ) ){
		    				$data['status']	 = 400;
		    				$data['code'] = 'ATT014';		    				
		    			} elseif ( $get_transaction && $transaction->balance >= $amount ) {
	    					$transaction_amount = $transaction->balance - $amount;
	    			    	// saving loaded in db
	    			    	$insertArr = array(
	    			    		'user_id' => $current_user->userId,
	    			    		'transaction_type'=>'deduct',
	    			    		'balance' => $transaction_amount,
	    			    		'payment_method' => 'on-system',
	    			    		'transaction_id' => NULL,
	    			    		'transaction_amount' => $amount,
	    			    		'transaction_status' => NULL,
	    			    		'remarks' => date(DATE_ISO8601, strtotime("now"))
	    			    		);
	    			    	$insertArr['created_at'] = $insertArr['updated_at'] = date('Y-m-d H:i:s');

	    			    	$result = $this->payment_model->insert( $insertArr );
	    			    	if( $result ) {
	    			    		$last_id = $this->db->insert_id();
	    			    		$res = $this->payment_model->update( array( 'transaction_status' => 'succeeded' ), array( 'id' => $last_id ) );
	    			    		if( $res ){	
	    			    			/* attachment updated*/
	    			    			$updateAttah = $this->attachment_model->update( array('is_paid'=> 1 ),array('id' => $attachment_id )  );

	    			    			/* email deduction*/

	    			    			$message = $this->message_model->get( array( '`msg`.`attachment_id`' => $attachment_id ) );
	    			    			
	    			    			/* added message  on deduction*/
	    			    			$this->payment_model->update( array( 'remarks' => $message[0]->message ), array( 'id' => $last_id,'transaction_type' => 'deduct', ) );
	    			    			
	    			    			$service_detail = $this->service_model->get( array('id' => $message[0]->service_id ) );

	    			    			$this->message_model->update( array( 'is_seen' => 1,'status' => 'completed'), array( '`id`' => $message[0]->id ) );

	    			    			$username = get_meta_data( $current_user->userId, 'nick_name', true );
	    			    			$url = $this->get_url_http();
	    				            $template_data = array(
	    				            		'page_title'         => 'Balance deducted on download',
	    				            		'name'				 => ( $username != '' ) ? $username : $current_user->email,
	    				            		'body_data'  		 => 'Your balance of $' . $amount . '(USD) has been deducted on downloading the file.<br /> Your remaining balance is $'.$transaction_amount .'(USD).',
	    				            		'show_token'         => true,
	    				            		'service_name'		 => $service_detail[0]->title,
	    				            		'message_title'		 => 'of',
	    				            		'message'		 	 => $message[0]->message,
	    			                        'verification_url'   => $url,
	    			                        'verification_label' => 'Click here to login',
	    			                        'footer_text'        => $this->get_email_footer_section()
	    				            	);
	    				            $balance_email_view = $this->load->view( 'balance-email-view', $template_data, true );
						            $emailRes = $this->_send_email( $balance_email_view, $current_user->email , $template_data['page_title'] ); 
						            $data['email'] = ($emailRes)?'ok':'404';

	    				    		$data['status'] = 201;
	    				    		$data['code'] = 'PAY001';
	    				    		$data['data'] = array(
	    				    							'balance' => $transaction_amount,
	    				    							'attachment_id' => $attachment_id
	    				    							);
	    			    		} else{
	    			    			$data['status'] = 400;
	    			    			$data['code'] = 'PAY002';
	    			    		}
	    			    	} else {
	    			    		$data['status'] = 400;
	    			    		$data['code'] = 'PAY002';
	    			    	}
	    			    }else{
	    			    	$data['status'] = 400;
	    			    	$data['code'] = 'PAY003';
	    			    }
    			    }

		    	}else{
		    		$data['status'] = 400;
		    		$data['code'] = 'PAY004';
		    	}
		    }else {
		    	$data['status'] = 400;
		    	$data['code'] = 'PAY007';
		    }	    	

		} else {
	        $data['status'] = 400;
	        $data['code'] = 'USR008';
	    } 
	   $this->response( $data );
	} 

	public function download_get( $user_id = false, $attachment_id = false ) {
	   if( !is_null($user_id) && is_numeric($user_id) && $user_id > 0 ) {
	   		$get_user = $this->user_model->get( array( 'id' => $user_id ));		   	
		   	if( count( $get_user ) > 0 ) {
		   	    $user  =  $get_user[0];
		   	    if( !is_null($attachment_id) && is_numeric($attachment_id) && $attachment_id > 0 ) {
		   	     	/*attachment*/
		   	     	$attach = $this->attachment_model->get( array('id' => $attachment_id ) );
		   	     	if( count( $attach ) > 0 ){
		   	     		$config_upload = $this->load->config('upload', TRUE );
		   	     		if( 1 ==  $attach[0]->is_paid || '1' ==  $attach[0]->is_paid ){
		   		     		$this->load->helper('download');		     		
		   		     		$name = $config_upload['upload_path'] . $attach[0]->filename;

		   		     		
		   		     		if( file_exists( $name ) ) {		   			     		
		   			     	 force_download( $name, NULL );		   		     		
		   		     		} else {		   		     			
		   		     			// file not exists
		   		     		}
		   	     		} else {
		   	     			// not paid			   	     			 
		   	     		}	   	     		

		   	     	} else {
		   	     		// no attachments		   	     		
		   	     	}
		   	    } else {
		   	    	// invalid attachent id		   	    	
		   	    }
		   	} else {
		   		// ivalid user id
		   	}
	   } else {
	   		// invalud user id
	   }	
	   
	   /*if( $_GET['redirectUri'] ) {
		redirect(  $_GET['redirectUri'] );		   		     				
	   } */

	}

	public function paypal_payment_initial_register_post() {
		$data = array(
			'status' => 200,
			'code' => '' 
			);
		$_POST = json_decode( trim( file_get_contents('php://input') ), true );
		$posted_data = $this->input->post();
		$posted_data = $posted_data['data'];
		$payment_response = $this->paypal_lib->completePayment(  $posted_data );
		if( 200 ==  (int) $payment_response['code'] ) {
			
			$paymentId = $posted_data['paymentId'];
			$payerId = $posted_data['PayerID'];

			$payment_response = $payment_response['result'];
		 	
		 	$payment_response = json_decode( $payment_response, true );		
			
			$allTransaction = array_pop( $payment_response['transactions'] );
			
			if( isset( $allTransaction['custom']) ) {
				$transaction = json_decode( $allTransaction['custom'], true );

 				$get_user = $this->user_model->get( array(  'email' => $transaction['email'] ) );
 				if( count( $get_user ) > 0 ){
 					$user = $get_user[0]; 

 					$update_token = $this->user_model->update(
 					                                array(
 					                                    'token'        => $posted_data['token'],
 					                                    'payment_mode' => 'paypal'
 					                                    ),
 					                                array( 'email' => $transaction['email'] )
 					                            );
 					
 					if( "subscribe" == $posted_data['paid_for'] ){
 						$this->__paypal_user_detail_update_subscribe( $data, $payerId, $paymentId, $user, $allTransaction );
 					} else if ( "balance_load" == $posted_data['paid_for'] ) {
 						$this->__paypal_user_balance_update_load( $data, $payerId, $paymentId, $user, $allTransaction );
 					}

 				} else {
			    	$data['status'] = 400;
			    	$data['code'] = 'USR008';
			    }

				
			}else {
				$data['status'] = 400;
				$data['code'] = '';
			}
			
		}  else {
			$data['status'] = $payment_response['code'];
			$data['code'] = 'USR056';
			$data['data'] = $payment_response['message'];
		}
			
		$this->response( $data );		
	}

	public function paypal_payment_fail_get() {

	}

	private function __paypal_user_detail_update_subscribe( &$data, $payerId, $paymentId, $user, $transaction ){
		$transaction = json_decode( $transaction['custom'], true );

		$updateArr = array(
		                     'payment_mode'    => 'paypal',
		                     'customer_id'     => $payerId,
		                     'subscription_id' => $paymentId,
		                     'is_subscribe'    => 1,
		                     'is_active'       => 1,
		                     'is_verified'     => 0,
		                     'plan_start_date' => NULL,
		                     'plan_end_date'   => NULL
		                 );				 
		$res = $this->user_model->update( $updateArr, array( 'email' => $transaction['email'] ) );

		if( $res ){
		    $data['status'] = 200;
		    $data['code'] = 'USR028';

	     	$access_token = $this->generate_token( $user->email, $user->id, $user->role );
			
			$url = $this->get_url_http( 'email-verify/' . $user->verification_token );
			$templateData = array(
			                     'page_title'         => 'Registration',
			                     'name'               => $user->email,
			                     'body_data'          => "Welcome, your are connected with us. Here's your login credentials",
			                     'email'              => $user->email,
			                     'password'           => '<i>What you have chosen</i>',
			                     'show_token'         => ( strcmp( 'customer',$user->role) == 0 )? true: false,
			                     'verification_url'   => $url,
			                     'verification_label' => 'Verify Token',
			                     'footer_text'        => $this->get_email_footer_section()
			              );

			$register_template = $this->load->view('users/register_view',$templateData , true );
			$emailRes = $this->_send_email( $register_template, $user->email , 'Registration' );
		    $data['data'] = array(
		             'token'        => $access_token,
		             'email'        => $user->email,
		             'user_id'      => $user->id,
		             'role'         => $user->role,
		             'is_subscribe' => $updateArr['is_subscribe'],
		             'is_active'    => $updateArr['is_active'],
		             'is_verified'  => $updateArr['is_verified'],
		             'meta'         => $this->get_meta_by_id( $user->id ),
		             'balance'      => $this->get_balance( $user->id )                                   
	         	);
		} else {
		     $data['status'] = 400;
		     $data['code'] = 'USR029';
		}
	}

	private function __paypal_user_balance_update_load( &$data, $payerId, $paymentId, $user, $allTransaction ) {
		$load_amount = (float) $allTransaction['amount']['total'];		
		$transaction = json_decode( $allTransaction['custom'], true );
		$this->db->group_by('id');
		$get_transaction = $this->payment_model->get( array( 'user_id' => $user->id ),array( 'id' => 'DESC' ) );
		if( count( $get_transaction ) > 0 ){
		    $trans = $get_transaction[0];
		    $transaction_amount = $trans->balance + $load_amount; 
		} else{
		    // $transaction_amount = 0;
		    $transaction_amount = $load_amount;
		}
	    
	    // saving loaded in db
	    $insertArr = array(
	        'user_id' => $user->id,
	        'transaction_type'=>'load',
	        'balance' => $transaction_amount,
	        'payment_method' =>  'paypal',
	        'transaction_id' => $paymentId,
	        'transaction_amount' => $load_amount,
	        'transaction_status' =>'success',
	        'remarks' => date(DATE_ISO8601, strtotime("now"))
	        );
	    $insertArr['created_at'] = $insertArr['updated_at'] = date('Y-m-d H:i:s');
	    $result = $this->payment_model->insert( $insertArr );

	    if( $result ) {
	        $data['status'] = 200;
	        $data['code'] = 'PAY005';
	        $data['data'] = $insertArr;
	    } else {
	        $data['status'] = 400;
	        $data['code'] = 'PAY006';
	    }
	}


	protected function _stripe_charge( $data = NULL ) {
		try {
			if( is_null( $data ) ) {
				return array( 
		    				'status'=> FALSE,
		    				'result' => 'empty data'
		    		);
			}
		    $charge = Charge::create( $data );
		    return  array( 
		    				'status'=> TRUE,
		    				'result' => $charge
		    		);
		} catch (Exception $e) {
			
			return array( 
		    				'status'=> FALSE,
		    				'result' => $e->getMessage()
		    		);
		}
	}

	protected function _stripe_create_new_card( $customer_id = NULL, $token_id = NULL  ) {
		try {
			if( is_null( $customer_id ) && is_null( $token_id ) ) {
				return FALSE;
			}
			$customer = Customer::retrieve( $customer_id );
			$rtn = $customer->sources->create( array("source" => $token_id ) );			
			
			return  array( 
		    				'status'=> TRUE,
		    				'result' => $rtn
		    		);
			

			
		} catch( Exception $e ){

			return array( 
		    				'status'=> FALSE,
		    				'error' => $e->getMessage()
		    		);
		}
	}

   protected function _stripe_new_create_customer( $data = NULL ) {
        try{
            if( is_null ( $data ) ) {
                return  array( 'status' => false, 'error' => 'empty data' );
            }
           $response = Customer::create( $data );
           return  array('status' => true, 'result' => $response );
        } catch( Exception $e ) {

            return  array( 'status' => false, 'error' => $e->getMessage() );
        }
    }

	/* stripe webhooks*/

	public function stripe_webhook_invoice_payment_succeeded_post() {
		// invoice.payment_succeeded
		$events = json_decode( trim( file_get_contents('php://input') ), true);
		if( isset( $events['data'] ) ) {
			$data = $events['data'];
			
			$customer = $data['object']['customer'];
			 /* in user table */
			$get_user = $this->user_model->get( array( 'customer_id' =>$customer )); 
			 if( count( $get_user ) > 0 ){
			 	$user = $get_user[0];
				$updateArr =  array( 									
									'plan_start_date' => $data['object']['period_start'],
									'plan_end_date' => $data['object']['period_end']
									);
				$updateRes = $this->user_model->update( $updateArr , array( 'id' => $user->id ) );					

			 } 
		}
	}

	public function stripe_webhook_invoice_payment_failed_post() {
		// invoice.payment_failed
		$events = json_decode( trim( file_get_contents('php://input') ), true);

		if( isset( $events['data'] ) ) {
			$data = $events['data'];
			
			$customer = $data['object']['customer'];
			 /* in user table */
			$get_user = $this->user_model->get( array( 'customer_id' =>$customer )); 
			 if( count( $get_user ) > 0 ){
			 	$user = $get_user[0];
				$updateArr =  array(
									'is_subscribe' => 0,
									'plan_start_date' => '',
									'plan_end_date' => ''
									);
				$updateRes = $this->user_model->update( $updateArr , array( 'id' => $user->id ) );					

			 } 
		}

	}

	public function stripe_webhook_customer_deleted_post() {
		// customer.subscription.deleted
		$events = json_decode( trim( file_get_contents('php://input') ), true);
		
		if( isset( $events['data'] ) ) {
			$data = $events['data'];
			
			$customer = $data['object']['customer'];
			
			 /* in user table */
			$get_user = $this->user_model->get( array( 'customer_id' => $customer )); 

			 if( count( $get_user ) > 0 ){
			 	$user = $get_user[0];
				$updateArr =  array( 
									'is_subscribe' => 0,
									'customer_id'  => '',
									'subscription_id'  => '',
									'plan_start_date' => '',
									'plan_end_date' => ''
									);
				$updateRes = $this->user_model->update( $updateArr , array( 'id' => $user->id ) );				

			 } 

		}

	}

	/* Paypal webhooks*/

	public function paypal_webhooks_payment_captures_completed_post(){
		$raw_post_data = file_get_contents('php://input');
		parse_str( $raw_post_data, $ipn_arr );

		if( is_array( $ipn_arr ) && count( $ipn_arr ) > 0 ) {
			$ipn_arr['payment_status'] = strtolower( $ipn_arr['payment_status']  );
			if( "completed" == $ipn_arr['payment_status'] ) {
				 /* in user table */
				
				$customer_email = $ipn_arr['option_selection2'];

				$payment_type = $ipn_arr['option_selection3'];

				if( $payment_type == "subscribe" ){

					$get_user = $this->user_model->get( array( 'email' =>$customer_email )); 
					if( count( $get_user ) > 0 ){
					 	$user = $get_user[0];
						$updateArr =  array( 
											'is_subscribe' => 1,
											'is_active' => 1,
											'is_verified' => 1,
											'payment_mode' => 'paypal',
											'customer_id' => $ipn_arr['payer_id'],
											'subscription_id' => $ipn_arr['txn_id'],
											/*'plan_start_date' => $data['object']['period_start'],
											'plan_end_date' => $data['object']['period_end']*/
											);					
						$updateRes = $this->user_model->update( $updateArr , array( 'id' => $user->id ) ); 
					}

				} else if( "load-balance" ==  $payment_type ) {
					$get_user = $this->user_model->get( array( 'email' =>$customer_email )); 
					if( count( $get_user ) > 0 ){
					 	$user = $get_user[0];

					 	/*cheking if current user have last transaction or not*/
					 	$this->db->group_by('id');
					 	$get_transaction = $this->payment_model->get( array( 'user_id' => $user->id ),array( 'id' => 'DESC' ) );
					 	
					 	if( count( $get_transaction ) > 0 ){
					 		$transaction = $get_transaction[0];
					 		$transaction_amount = $transaction->balance + $ipn_arr['option_selection4'];	
					 	} else{
					 		
					 		$transaction_amount = $ipn_arr['option_selection4'];
					 	}
					 	$insertArr = array(
					 		'user_id' => $user->id,
					 		'transaction_type'=>'load',
					 		'balance' => $transaction_amount,
					 		'payment_method' =>  'paypal',
					 		'transaction_id' => $ipn_arr['txn_id'],
					 		'transaction_amount' => $ipn_arr['option_selection4'],
					 		'transaction_status' => $ipn_arr['payment_status'],
					 		'remarks' => date(DATE_ISO8601, strtotime("now"))
					 		);
					 	$insertArr['created_at'] = $insertArr['updated_at'] = date('Y-m-d H:i:s');
					 	$result = $this->payment_model->insert( $insertArr );
					}
				}

			}
		}	

		/*file_put_contents( APPPATH.'../logs/completed.txt', print_r ( $ipn_arr, true ) );	*/
	}

	public function paypal_webhooks_payment_captures_denied_post(){
		$raw_post_data = file_get_contents('php://input') ;
		parse_str( $raw_post_data, $raw_post_array );
		/*file_put_contents( APPPATH.'../logs/completed.txt', print_r ( $raw_post_array, true ) );*/
	}
	
}