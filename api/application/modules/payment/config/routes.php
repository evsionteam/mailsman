<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['balance/check']['GET'] = 'payment/payment/balance_check';

$route['balance/load']['POST'] = 'payment/payment/balance_load';

$route['balance/deduct']['POST'] = 'payment/payment/balance_deduct';

$route['download/(:any)/(:any)']['GET'] = 'payment/payment/download/$1/$2';

$route['stripe/invoice/success']['POST'] = 'payment/payment/stripe_webhook_invoice_payment_succeeded';

$route['stripe/invoice/failed']['POST'] = 'payment/payment/stripe_webhook_invoice_payment_failed';

$route['stripe/customer/deleted']['POST'] = 'payment/payment/stripe_webhook_customer_deleted';

$route['paypal/captured/success']['POST'] = 'payment/payment/paypal_webhooks_payment_captures_completed';

$route['paypal/captured/failed']['POST'] = 'payment/payment/paypal_webhooks_payment_captures_denied';

$route['paypal/success']['POST'] = 'payment/payment/paypal_payment_initial_register';

$route['paypal/fail']['POST'] = 'payment/payment/paypal_payment_fail';


/*$route['stripe/customer/deleted']['GET'] = 'payment/stripe_webhook_customer_deleted';*/ 