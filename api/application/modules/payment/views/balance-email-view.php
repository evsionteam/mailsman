<?php $this->load->view('users/header_email');?>

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">                       
			<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
				<tbody><tr>
					<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 23px; color: #3f4345; line-height: 30px; font-weight: 100;" t-style="not6Headline" mc:edit="40" object="text-editable">
						<!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>Hello <?php echo $name; ?> </singleline><!--[if !mso]><!--></span><!--<![endif]-->
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">
			<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
				<tbody><tr>
					<td width="100%" height="30"></td>
				</tr></tbody>
			</table>
		</td>
	</tr>
</tbody></table>


<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">
			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" c-style="not4GreenBG" object="drag-module-small" style="">
				<tbody><tr>
					<td width="600" valign="middle" align="center">

						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
							<tbody><tr>
								<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px; " t-style="not4BodyText" class="fullCenter" mc:edit="26" object="text-editable">
									<!--[if !mso]><!--><span style="color:#3f4345;font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><multiline><?php echo $body_data;?><br><br>

									<?php if( isset( $service_name ) ): ?>
										<div style="margin:15px; background-color:#ddd;padding: 15px; " ><?php echo $service_name;?> </div>
									<?php endif;?>	


									<?php if( isset( $message_title ) && isset( $message ) ): ?>
										<div style="margin:15px; padding: 15px;" ><?php echo $message_title;?></div>
										<div style="margin:15px; background-color:#ddd;padding: 15px; " ><?php echo $message;?></div>
									<?php endif;?>	
									</multiline><!--[if !mso]><!--></span><!--<![endif]-->
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<?php if( $show_token ) : ?>
<!-- --------------- Button Center --------------- -->
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" >
	<tbody><tr>
	  	<td width="600" valign="middle" align="center">	   
	    	<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
		      	<tbody><tr>
			        <td>
			          	<table border="0" cellpadding="0" cellspacing="0" align="center"> 
				            <tbody><tr> 
				              	<td align="center" height="45" c-style="not6ButBG" bgcolor="#4edeb5" style="border-radius: 5px; padding-left: 30px; padding-right: 30px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; color: rgb(255, 255, 255); background-color: #5d9bfb;" t-style="not6ButText" mc:edit="42">
				              	<multiline><!--[if !mso]><!--><span style="font-family: 'proxima_nova_rgbold', Helvetica; font-weight: normal;"><!--<![endif]-->
				              			<a href="<?php echo $verification_url;?>" style="color: #ffffff; font-size:15px; text-decoration: none; line-height:34px; width:100%;" t-style="not6ButText" object="link-editable"><?php echo $verification_label;?></a>
				              		<!--[if !mso]><!--></span><!--<![endif]--></multiline>
				              	</td> 
			              	</tr></tbody>
			          	</table> 
				      </td>
			  	</tr></tbody>
			</table>
		</td>
	</tr></tbody>
</table><!-- --------------- End Button Center --------------- -->
<?php endif;?>

<?php $this->load->view('users/footer_email');?>