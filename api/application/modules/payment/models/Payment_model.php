<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Payment_model extends Base_model {
	
	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . 'credit';
	}
}