<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['user/login'] = 'users/users/login';

$route['user']['GET'] = 'users/users/user';

$route['user/(:num)']['GET'] = 'users/users/user/$1';

/* pagination => user/{noID=0}/{role}/{page} */
$route['user/(:num)/(:any)/(:any)']['GET'] = 'users/users/user/$1/$2/$3';

/*search => user/{noID=0}/{role}//{page}/{search}*/
$route['user/(:num)/(:any)/(:any)/(:any)']['GET'] = 'users/users/user/$1/$2/$3/$4';

$route['user']['POST'] = 'users/users/user';

$route['user/(:num)']['PUT'] = 'users/users/user/$1';

$route['user/(:num)']['DELETE'] = 'users/users/user/$1';

$route['user/forgot'] = 'users/users/forgot';

$route['user/recover'] = 'users/users/recover';

$route['user/logout'] = 'users/users/logout';

$route['subscription']['POST'] = 'users/users/subscribe';

$route['user/user_verification']['PUT'] = 'users/users/user_verification';

$route['user/user_resent_verification']['GET'] = 'users/users/user_resent_verification';
/*
$route['user/mm_address']['POST'] = 'users/users/mm_address';

$route['user/mm_address/(:num)']['PUT'] = 'users/users/mm_address/$1';

$route['user/mm_address/(:num)']['DELETE'] = 'users/users/mm_address/$1';*/
