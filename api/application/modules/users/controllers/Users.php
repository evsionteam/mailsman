<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Users class.
 *
 * @extends MY_Controller
 */

use \Stripe\Stripe;
use \Stripe\Customer;
use \Stripe\Charge;
use \Stripe\Subscription;

class Users extends MY_Controller {

    /**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	*/


	public function __construct() {
		parent::__construct();
        /**
         * upload_base_path and upload_base_url are fetch form MY_controller
         */

        $this->load->model('user_model');

        $this->load->model('options/option_model','option_model');       

		$this->load->helper( 'user' );

        $this->load->helper('error_code');
        $this->load->library('paypal_lib');

        $this->stripe_config = $this->load->config('stripe', TRUE );
        Stripe::setAPiKey( $this->stripe_config['stripe_key_secret'] );
	}

	public function index() { }

    /*Register a new user to site*/
    public function user_post() {

        $data = array();

        $_POST = json_decode(trim(file_get_contents("php://input")), true);

        $postedData               = array();
        $postedData['email']      = $this->input->post('email');
        $postedData['password']   = $this->input->post('password');
        $postedData['created_at'] = date('Y-m-d H:i:s');

        $this->load->library('form_validation');

        $rules = $this->_form_rules();

        $this->form_validation->set_rules($rules);
        if ( $this->form_validation->run() == FALSE ) {

            $err = array();
            $errors = $this->form_validation->error_array();

            if( isset($errors['email']) && !empty($errors['email']) ){
                $err[] = $errors['email'];
            }

            if( isset($errors['password']) && !empty($errors['password']) ){
                $err[] = $errors['password'];
            }

            if( isset($errors['reTypePassword']) && !empty($errors['reTypePassword']) ){
                $err[] = $errors['reTypePassword'];
            }
            $data['status'] = 400;
            $data['data']  = $err;

        } else {            

            /* verify if email address exists or not*/
            $where = array(
                            '`usr`.`email`'       => $postedData['email']
                        );

            $get_user = $this->user_model->get_user( $where );
            
            $userDetail = $get_user->row();  
            if((count($userDetail) > 0 )  && ( $userDetail->is_active == 1 ) && ( $userDetail->is_subscribe == 1 ) ){
                $data = array(
                    'status'  => 400,
                    'code'   => 'USR015'
                );    
            } else if( (count($userDetail) > 0 )  && ( $userDetail->is_active == 0 ) && ( $userDetail->is_subscribe == 0 )  ) {
               /*
               && ( strcmp('administrator', $userDetail->role ) != 0 ) && ( strcmp('staff', $userDetail->role ) != 0 )
                */
                $token = $this->generate_token( $userDetail->email, $userDetail->id, $userDetail->role );
                $user_detail = array(
                                    'is_exists'    => true,
                                    'id'           => $userDetail->id,
                                    'token'        => $token,
                                    'email'        => $userDetail->email,
                                    'role'         => $userDetail->role,
                                    );

                $data = array(
                    'status'  => 201,
                    'code'   => 'USR037',                    
                    'data' => $user_detail
                );                

                if( isset( $postedData['role'] ) && $postedData['role'] == 'staff' ){
                    $data['code'] = 'USR043';
                }
            }  else {
                /*new user*/
                $postedData['role'] = $this->input->post( 'user_role' ) ? $this->input->post( 'user_role' ) : 'customer';
                $postedData['is_subscribe'] = 0;

                $postedData['is_active'] = 0;
                $verification_token = generate_reset_token(  $postedData['role'] . ' | '  . $postedData['email']  );

                $postedData['verification_token'] = $verification_token;
                $postedData['is_verified'] = 0;

                if( $postedData['role'] == 'staff' ){
                    $postedData['is_active']          = 1;
                    $postedData['verification_token'] = NULL;
                    $postedData['is_verified']        = 1;
                }

                if( $user_id = $this->user_model->insert( $postedData ) ){
                    $query = $this->user_model->get_user( array('`usr`.`id`'=> $user_id ) );
                    $row = $query->row();
                    $token = $this->generate_token( $row->email, $row->id, $row->role );

                    if( $postedData['role'] == 'staff' ) {
                        /* Sending token to email */

                        $url = $this->get_url_http();                       
                        $templateData = array(
                                            'page_title'         => 'Registration',
                                            'name'               => $row->email,
                                            'body_data'          => "Welcome, your are connected with us. Here's your login credentials",
                                            'email'              => $row->email,                                          
                                            'password'           => $postedData['password'],
                                            'show_token'         => true,
                                            'verification_url'   => $url,
                                            'verification_label' => 'Click to Login',
                                            'footer_text'        => $this->get_email_footer_section()
                                        );

                        $register_template = $this->load->view('register_view',$templateData , true );
                       /*  file_put_contents( APPPATH.'../logs/forget.html', $forgot_password_template );*/
                        $emailRes = $this->_send_email( $register_template, $row->email , 'Registration' );
                    }
                    $user_detail = array(
			                            'is_exists'    => false,
			                            'id'           => $row->id,
			                            'token'        => $token,
			                            'email'        => $row->email,
			                            'role'         => $row->role,
			                            );
                    if( 'customer' == $postedData['role'] ) {
                        $user_detail['is_active']    = $row->is_active;
                        $user_detail['is_subscribe'] = $row->is_subscribe;
                        $user_detail['is_verified']  = $row->is_verified;
                        $user_detail['meta']         = $this->get_meta_by_id( $row->id );
                        $user_detail['balance']      = $this->get_balance( $row->id );
	                	$user_detail['is_new_user']  = TRUE;

                    }

                    $data = array(
                        'status'  => 201,
                        'code'   => ( $postedData['role'] == 'staff' ) ? 'USR055':'USR037',
                        'data' =>  $user_detail
                    );

                    if( $postedData['role'] == 'staff' ){
                        $data['code'] = 'USR043';
                    }
                }else{
                    $data['status'] = 503;
                    $data['code'] = "USR003";
                }
            }
        }
        $this->response($data);
	}

	public function user_put( $user_id = false ){

        $this->load->library('base64_decode');

        $data = $updateArr = array();
        $current_user = $this->current_user;

        $current_user_role = $current_user->role;

        /* Only allow admin staff to update other users profile. Normal users can only update their profile*/
        if( ( is_numeric( $user_id ) && $user_id !== false ) && ( $current_user->userId == $user_id ) || ( 'administrator' == $current_user_role || 'staff' == $current_user_role ) ){

            $query = $this->user_model->get_user( array( '`usr`.`id`'=> $user_id ) );

            if( $query->num_rows() > 0 ){

                $_POST = json_decode(trim(file_get_contents('php://input')), true);
                
                /*Saving / Updating usermeta*/
                $meta = $this->input->post( 'meta' );
                /* metas*/

                if( isset( $meta ) ){

                    if( isset( $meta[ 'avatar' ] ) ){
                        /*delete old image*/
                        $avatar = get_meta_data( $user_id, 'avatar', true );
                        $this->delete_image( $this->upload_base_path . $avatar );

                        $file_name = $this->upload_base_path . $user_id . '-'. time();
                        $new_avatar = $meta[ 'avatar' ] = $this->base64_decode->base64_decoder( $meta[ 'avatar' ], $file_name  ) ;
                    }

                    $update_meta_res = $this->user_model->save_metas( $meta, $user_id );
                }

                if( isset( $update_meta_res ) && $update_meta_res ){
                    if( isset( $new_avatar ) ){
                        $meta[ 'avatar' ] =  $this->upload_base_url . $new_avatar;
                    }
                    $data[ 'status' ] = 200;
                    $data[ 'code' ]   = 'USR022';
                    $data[ 'data' ]   = array( 'user_id' => $user_id, 'meta' => $meta );
                }/*else{
                    $data['status'] = 503;
                    $data['code'] = 'USR011';
                }               
               */

                $this->_updatePasswordCallback( $query, $data );

                $this->_isActiveCallback( $query, $data, $user_id );

            }else{
                $data['status'] = 404;
                $data['code'] = 'USR008';
            }
        }else{
            $data['status'] = 403;
            $data['code'] = 'USR010';
        }

		$this->response($data);
	}

   	public function user_delete( $user_id = false) {

        $data = array();
        $current_user = $this->current_user;
        $current_user_role = $current_user->role;

        if( (is_numeric( $user_id ) && $user_id !== false) && ( checkedifEqual( 'administrator', $current_user_role) || checkedifEqual( 'staff' , $current_user_role ) ) ) {

                $query = $this->user_model->get_user( array('`usr`.`id`'=> $user_id ) );
                if( $query->num_rows() > 0 ){
                    $mm_address_id = $query->row('mm_address');

                    /*Delete both user and their user meta*/

                    //$this->user_model->delete_metas($user_id);

                    $avatar = get_meta_data( $user_id, 'avatar', true );
                    $deleteRes = $this->user_model->delete( array('id' => $user_id ) );

                    if( $deleteRes ){
                        if( $avatar != '' ) {
                            $this->delete_image( $this->upload_base_path . $avatar );
                        }

                        /* remove assigned mm address from deleted user */
                        if( $mm_address_id ) {

                        $this->mm_address->update( array( 'has_assigned'=>'' ), array( 'id' => $mm_address_id ) );
                        }

                        $data['status'] = 200;
                        $data['code']   = 'USR023';
                    }else{
                        $data['status'] = 503;
                        $data['code'] = 'USR007';
                    }
                }else{
                    $data['status'] = 404;
                    $data['code'] = 'USR008';
                    $data['data'] = 'User not found.';
                }
   		}else{
   			$data['status'] = 403;
   			$data['code'] = 'USR010';
   		}
   		$this->response( $data );
    }

    public function user_get( $user_id = false, $role = false, $page = false, $search = false ){
        $this->load->config('pagination');

        $current_user = $this->current_user;
        $current_user_role = $current_user->role;

        $data  = array();
        $where = null;

        //***** customer should not get other users *****//

        if( $current_user_role == 'customer' ){
            $user_id = $current_user->userId;
        }

        if( $user_id !== false  && ( is_numeric( $user_id ) && $user_id != 0 ) ) {
            $where = array( 'usr.id' => $user_id );
        }

        if( false !== $role ){
            $where['role'] = $role;
        }

        $order_by = array(
                        'usr.id' => 'DESC'
                    );

        $limit = array();
        /*$where['is_verified'] = 1;*/

        if( 'unverified' == $page ){
            $where[ '`usr`.`mm_address`' ] = NULL;
        }elseif('no_page' == $page){
        }elseif( false !== $page ) {
            $page = ( $page <= 0 ) ? 1: $page;
            $limit['per_page'] = $this->config->item('per_page');
            $limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
        }

        $like = $where_in= NULL;

        if( false !== $search ) {
            $like = array();

            $like['`mma`.`mm_address`'] = $search;
            $like['`usr`.`email`'] = $search;
            /*$mm_address_all  = $this->mm_model->get( null, null, array('mm_address' => $search) );
            $this->db->reset_query();
            if( ($mm_address_all) > 0){
                foreach( $mm_address_all as $mail ){
                    $where_in[] = $mail->assigned_to_id;
                }
            }*/
        }

        $query = $this->user_model->get_user( $where, $order_by, $limit, $like );
        
        $count = $this->user_model->get_user_count( $where, $order_by, $like );

        if( $query->num_rows() > 0 ){

            $data[ 'status' ] = '200';
            $data[ 'code' ]   = 'USR033';

            if( $user_id ){

                //***** Will have only one row always *****//

                $meta = $this->get_meta_by_id( $user_id );
                $row = $query->row();
              /*  $mm_id = $row->mm_address;
                if( !is_null ( $mm_id ) ){
                    $mm_detail = $this->mm_model->get( array( 'mm.id'=>$mm_id ) );
                    $row->mm_address = array( 'id' =>  $mm_id, 'mm_address' => $mm_detail[0]->mm_address );
                }   */

                unset( $row->password );
                
                $row->meta = $meta;
                $row->balance  = $this->get_balance( $row->id );

                $data[ 'data' ] =  $row;

            } else {

                //***** Will have more than one rows *****//

                $temp_data = array();
                foreach( $query->result() as $row ){

                    $meta = $this->get_meta_by_id( $row->id );

                    $row->meta = $meta;

                   /* $mm_id = $row->mm_address;

                    if( !is_null ( $mm_id ) ){
                        $mm_detail = $this->mm_model->get( array( 'mm.id'=>$mm_id ) );
                        $row->mm_address = array( 'id' =>  $mm_id, 'mm_address' => $mm_detail[0]->mm_address );
                    }   */
                    unset( $row->password );

                    $temp_data[] = $row;

                }
                $data['total_rows'] = $count;
                $data[ 'data' ] = $temp_data;
            }

        } else if( is_null( $query )){
            $data['status']   = 200;
            $data['code'] = 'NOT_FOUND';
            $data['data']   = NULL;

        }  else {

            $data['status'] = 404;
            $data['code']   = 'USR008';
        }

        $this->response( $data );
    }

    /*User Login*/
    public function login_post() {

        $data = array();

        $_POST = json_decode(trim(file_get_contents('php://input')), true);
        $user_email = $this->input->post('email');
        $user_password = $this->input->post('password');

        $res = $this->user_model->resolve_user_login( $user_email, $user_password );

        if( $res ){

            $where = array(
                'email'=> $this->input->post('email')
            );

            $query = $this->user_model->get_user( $where );

            $row = $query->row();
            if( $row->is_active == 0 ){
                $data['status'] = 400;
                $data['code'] = 'USR034';
            }  else {
                $token = $this->generate_token( $row->email, $row->id, $row->role );

                $data['status'] = 200;
                $data['code'] = 'USR035';
                $data['data'] =  array(
                                    'token'        => $token,
                                    'email'        => $row->email,
                                    'user_id'      => $row->id,
                                    'role'         => $row->role,
                                    'is_subscribe' => $row->is_subscribe,
                                    'is_active'    => $row->is_active,
                                    'is_verified'    => $row->is_verified,
                                    'meta'         => $this->get_meta_by_id( $row->id ),
                                    'balance'      => $this->get_balance( $row->id ),
                                    'mm_address'   => array( 
                                                               'address_line1' => $row->address_line1,
                                                               'address_line2' => $row->address_line2,
                                                               'city'          => $row->city,
                                                               'state'         => $row->state,
                                                               'country'       => $row->country,
                                                               'zip'           => $row->zip,
                                                               'phone'         => $row->phone,
                                                           )
                                );

            }


        }else {

            $id = $this->user_model->get_user_id_from_email( $user_email );

            if( $id ){
                $data['status'] = 400;
                $data['code']   = 'USR005';
                $data['data']   = array( 'field'  => 'password' );
            } else {

                $data['status'] = 400;
                $data['code']   = 'USR004';
                $data['data']   =  array( 'field'  => 'email' );

            }
        }

        $this->response( $data );

    }

	public function forgot_post(){

		$this->load->helper('user');
        $data = array();

        $_POST = json_decode( trim( file_get_contents( 'php://input' ) ), true );
        $email = $this->input->post( 'email' );

        if( !empty( $email ) ):

            /***** Reset token value and sent through email address *****/
            $query = $this->user_model->get_user( array( 'email'=> $email ) );

            if( $query->num_rows() > 0 ):

                $row = $query->row();
                $user_id =  $row->id;
                $user_email =  $row->email;

                $reset_token = generate_reset_token( $user_id. ' | ' .$user_email  );
                $updateArr = array( 'reset_token' => $reset_token );
                $where =  array( 'email' => $row->email, 'id' => $row->id );
                /* Update reset token */
                $this->user_model->update( $updateArr, $where );
                /* Sending token to email */
                
                $where = array(
                                'option_name' => "title",
                            );
                $this->db->where( $where );
                $option_title = $this->option_model->get( ) ;
                $site_title = '';
                if( count( $option_title ) > 0 ){
                    $site_title = $option_title[0]->option_value;
                }
                $user_name = get_meta_data( $row->id, 'nick_name', true );
          
                $templateData = array(
                                        'page_title'         => 'Password Recovery',
                                        'name'               => ( $user_name != '' ) ? $user_name : $row->email,
                                        'site_logo'          => 'http://rocketway.net/themebuilder/template/templates/notify/images/image_77px_not2.png',
                                        'body_data'          => 'You have requested to reset your password. Here is the reset token to change your password',
                                        'email'              => $row->email,
                                        'reset_token'        => $reset_token,
                                        'footer_text'        => $this->get_email_footer_section()
                                 );                

                $forgot_password_template = $this->load->view('forgot-password_view',$templateData , true );
                /*file_put_contents( APPPATH.'../logs/forget.html', $forgot_password_template );*/

                $emailRes = $this->_send_email( $forgot_password_template, $row->email );

                if( $emailRes ) {
                    $data['status'] = 200;
                    $data['data'] = array(
                            'code'  => 'USR024'
                        );
                }else{
                    $data['status']   = 500;
                    $data['data'] = array(
                            'code'  => 'USR012'
                        );
                }
            else:
                $data['status'] = 404;
                $data['data'] = array(
                        'code'  => 'USR004'
                    );
            endif;

        endif;

		$this->response( $data );
	}

	public function recover_post(){

        $data = array();
        $_POST = json_decode( trim( file_get_contents( 'php://input' ) ), true );

        $email = $this->input->post('email');
        $reset_token = $this->input->post('token');
        $new_password = $this->input->post('password');

		$param = array(
                    'email'=> $email,
                    'reset_token' => $reset_token
        );
        $this->load->library('form_validation');
        $new_rules = array(
                            array(
                                'field' => 'email',
                                'label' => 'Email Address',
                                'rules' => 'trim|required|valid_email|callback_email_address_check',
                                'errors' => array(
                                    'required'    =>  array(
                                                            'code'    => 'USR001',
                                                            'field'   => 'email'
                                                        ),
                                    'valid_email' => array(
                                                            'code'    => 'USR016',
                                                            'field'   => 'email'
                                                        ),
                                    'email_address_check'     => array(
                                                            'code'    => 'USR004',
                                                            'field'   => 'email'
                                                        )
                                )
                            ),
                            array(
                                'field' => 'password',
                                'label' => 'Password',
                                'rules' => 'trim|required|min_length[8]',
                                'errors' => array(
                                    'required' => array(
                                                            'code'    => 'USR017',
                                                            'field'   => 'password'
                                                        ),
                                    'min_length' => array(
                                                            'code'    => 'USR018',
                                                            'field'   => 'password'
                                                        )
                                ),
                            ),
                            array(
                                'field' => 'token',
                                'label' => 'Reset Token',
                                'rules' => 'trim|required|callback_reset_token_check',
                                'errors' => array(
                                    'required' =>  array(
                                                            'code'    => 'USR025',
                                                            'field'   => 'token'
                                                        ),
                                    'reset_token_check' => array(
                                                            'code' => 'USR014',
                                                            'field' => 'token'
                                                        )

                                ),
                            )
                       );
         $rules = $this->_form_rules($new_rules);

          $this->form_validation->set_rules($rules);

        if ( $this->form_validation->run() == FALSE ) {
            $err = array();
            $errors = $this->form_validation->error_array();


            if( isset($errors['email']) && !empty($errors['email']) ){
                $err[] = $errors['email'];
            }

            if( isset($errors['password']) && !empty($errors['password']) ){
                $err[] = $errors['password'];
            }

            if( isset($errors['token']) && !empty($errors['token']) ){
                $err[] = $errors['token'];
            }
            $data['status'] = 400;
            $data['data']  = $err;

        } else {

    		$query = $this->user_model->get_user( $param );
    		if( $query->num_rows() > 0 ){
                $updateArr = array(
                    'password' => $new_password,
                    'reset_token' => null
                );
                $where = array( 'email'=> $email);
    			$updateRes = $this->user_model->update($updateArr,$where);
    			if( $updateRes ){
    				$data['status'] = 200;
                    $data['data'] = array(
                        				'code'=> 'USR021'
                                        );
    			}else{
                    $data['status'] = 500;
                    $data['data'] = array(
                                        'code'  => 'USR013'
                                        );
    			}
    		} else {
                $data['status'] = 400;
                $data['data'] = array(
                                'code'  => 'STH001'
                                );
    		}

        }

		$this->response( $data );
	}

	public function logout() {
		/**
		 * @todo remove 'authToken' Key from header
		 */

		$this->response(
							array(
                                    'status'  => 200,
                                    'data'   => array(
                                                    'code'    => ''
                                                )
								)
						);
	}

    protected function  _updatePasswordCallback( $query, &$data ) {
        
        $current_user = $this->current_user;


        $user_row = $query->row();
        if( !empty( $this->input->post( 'email' ) )  && strcmp('administrator', $current_user->role) == 0 ) {
            $email = $this->input->post( 'email' );            
            $password = $this->input->post( 'password' );
            if( $email && $password ) {                
                
                $data[ 'status' ] = 200;
                $data[ 'code' ]   = 'USR040';
                   unset( $user_row->password );
                   unset( $user_row->address_line1 ); 
                   unset( $user_row->address_line2 ); 
                   unset( $user_row->city ); 
                   unset( $user_row->country ); 
                   unset( $user_row->zip ); 
                   unset( $user_row->phone ); 
                   unset( $user_row->is_subscribe ); 
                   unset( $user_row->mm_id ); 
                $data['data']     = $user_row;
                $this->user_model->update( array( 'password' => $password ), array( 'email' => $email ) );
            }

        } else {
            $new_password = $this->input->post( 'new_password' );
            $old_password = $this->input->post( 'old_password' );

            if( isset( $new_password ) && isset( $old_password ) ){

                if( password_verify( $old_password, $user_row->password ) ){
                    $this->user_model->update( array( 'password' => $new_password ), array( 'id' => $this->current_user->userId ) );
                    $data[ 'status' ] = 200;
                    $data[ 'code' ]   = 'USR040';                    

                }else{
                    $data[ 'status' ] = 403;
                    $data[ 'code' ] = 'USR041';
                }
            }
            
        }

    }

    protected function _isActiveCallback( $query, &$data, $user_id ) {
        $is_active = $this->input->post('is_active');
        $user_row = $query->row();        

        if( isset( $is_active ) ){
            $update = $this->user_model->update( array( 'is_active' => $is_active ), array( 'id' => $user_id  ) );
            if( $update ){
                /*sending an email*/
                $url = $this->get_url_http();               
                $site_email = $this->get_site_info( 'email' );
                $body_data = 'Your account has been disabled. Please contact ' . $site_email. ' for further assistance';
                
                if( $is_active == 1 ) {
                    $body_data = 'Your Account has been activated. You can click on following link to login.';
                }

                $templateData = array(
                                    'page_title'         => ( $is_active == 1 ) ? 'Account Activated' : 'Account Deactivated',
                                    'name'               => get_meta_data( $user_row->id, 'nick_name', true ),
                                    'email'              => $user_row->email,
                                    'status'             => $is_active,
                                    'login_url'          => ($is_active ==1) ? $url : '',
                                    'login_label'          => ($is_active ==1) ? 'Click Here' : '',
                                    'body_data'          => $body_data,
                                    'footer_text'        => $this->get_email_footer_section()
                                );

                $status_template = $this->load->view('status_view', $templateData , true );
                /*file_put_contents( APPPATH.'../logs/status.html', $status_template );*/
                $emailRes = $this->_send_email( $status_template, $user_row->email , $templateData['page_title'] );

                $data[ 'status' ] = 200;
                $data[ 'code' ]   = ($is_active == 1 )?'USR036':'USR039';
            } else {
                $data['status'] = 503;
                $data['code'] = 'USR011';
            }
        }
    }

	protected function _mappingArray( $arr ){

		$excludes =  array( 'email', 'password','role', 'is_active', 'reTypePassword' );
		foreach( $excludes as $exclude ){
			unset( $arr[ $exclude] );
		}
		return $arr;
	}

    protected function _form_rules( $new_rules = NULL ){
        $rules = array(
            array(
                'field' => 'email',
                'label' => 'Email Address',
                'rules' => 'trim|required|valid_email|callback_check_email',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'USR001',
                                            'field'   => 'email'
                                        ),
                    'is_unique' => array(
                                           'code'    => 'USR015',
                                           'field'   => 'email'
                                        ),
                    'valid_email' => array(
                                            'code'    => 'USR016',
                                            'field'   => 'email'
                                        ),
                    'check_email' => array(
                                            'code'   => 'USR015',
                                            'field'  => 'email',
                                        )
                )
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]',
                'errors' => array(
                    'required' => array(
                                            'code'    => 'USR017',
                                            'field'   => 'password'
                                        ),
                    'min_length' => array(
                                            'code'    => 'USR018',
                                            'field'   => 'password'
                                        )
                ),
            ),
            array(
                'field' => 'reTypePassword',
                'label' => 'Retype password',
                'rules' => 'trim|required|matches[password]',
                'errors' => array(
                    'required' =>  array(
                                            'code'    => 'USR019',
                                            'field'   => 'reTypePassword'
                                        ),
                    'matches' =>  array(
                                            'code'    => 'USR002',
                                            'field'   => 'reTypePassword'
                                        ),
                ),
            ),
        );

        if( !is_null( $new_rules ) && is_array( $new_rules ) ){
            /*override default settings*/
            $rules = $new_rules;

        }
        return $rules;
    }

    public function check_email( $email ) {
        $param = array( 
                        'email'        => $email,
                        'is_active'    => 1
                        /*'is_verified'  => 1,
                        'is_subscribe' => 1*/
                        );

        /*$this->db->where( "( `role` = 'administrator' || `role` ='staff' || `role` ='customer' )" );*/
        
        $query = $this->user_model->get_user( $param );
        if( $query->num_rows() > 0 ){             
            return FALSE;
        }

        return TRUE;
    }

    public function email_address_check( $email ){

        $param = array( 'email'=>$email );
        $query = $this->user_model->get_user( $param );

        if( $query->num_rows() > 0 ){ return true; }

        return false;
    }

    public function reset_token_check( $reset_token ){

        $param = array('reset_token'=>$reset_token);
        $query = $this->user_model->get_user( $param );

        if( $query->num_rows() > 0 ){ return true; }

        return false;
    }

    public function subscribe_post() {
        $data = NULL;
        $current_user  = $this->current_user;
        $_POST = json_decode ( trim ( file_get_contents('php://input') ), true );

        /*token,payment_mode*/
        $token = $this->input->post( 'token' );
        $res = NULL;

        $get_user = $this->user_model->get( array( 'id' => $current_user->userId ) );      
        if( count( $get_user ) >= 1 ) {
            $user = $get_user[0];
            /*Add token in Database*/            

            $update_token = $this->user_model->update(
                                            array(
                                                'token'        => $token,
                                                'payment_mode' => $this->input->post('payment_mode')
                                                ),
                                            array( 'id' => $current_user->userId )
                                        );

            if( $update_token ){
                if( strcmp("stripe",$this->input->post('payment_mode') ) == 0 ) {
                    /**
                     *  stripe payment
                     */
                    $create = array(
                                      "description" => "Subscriber=>". $user->email ."@". date('Y-m-d H:i:s'),
                                      "source" => $token /*obtained with Stripe.js*/
                                    );
                    $response = $this->_stripe_create_customer( $create );

                    if( $response['status'] ) {
                        $response = $response['result'];
                        /* connect customer with plan( stripe )*/
                        $subscribe = $this->_stripe_subscribe_customer_to_plan( array(
                                                                                        "customer" => $response->id,
                                                                                        "plan" => "plan-a"
                                                                                    )
                                                                                );
                        if( $subscribe['status'] ) {
                            $subscribe = $subscribe['result'];
                            $updateArr = array(
                                                'customer_id'     => $response->id,
                                                'subscription_id' => $subscribe->id,
                                                'is_subscribe'    => 1,
                                                'is_active'       => 1,
                                                'is_verified'     => 0,
                                                'plan_start_date' => $subscribe->current_period_end,
                                                'plan_end_date'   => $subscribe->current_period_start
                                            );
                           $res = $this->user_model->update( $updateArr, array( 'id' => $current_user->userId ) );
                           if( $res ){
                                $data['status'] = 200;
                                $data['code'] = 'USR028';
                                $access_token = $this->generate_token( $user->email, $user->id, $user->role );

                                if( $this->input->post('send_email') ){
                                    /* Sending token to email */
                                    $url = $this->get_url_http( 'email-verify/' . $user->verification_token );
                                    $templateData = array(
                                                            'page_title'         => 'Registration',
                                                            /*'name'               => get_meta_data( $user->id, 'nick_name', true ),*/
                                                            'name'               => $user->email,
                                                            'body_data'          => "Welcome, your are connected with us. Here's your login credentials",
                                                            'email'              => $user->email,
                                                            'password'           => '<i>What you have chosen</i>',
                                                            'show_token'         => ( strcmp( 'customer',$user->role) == 0 )? true: false,
                                                            'verification_url'   => $url,
                                                            'verification_label' => 'Verify Token',
                                                            'footer_text'        => $this->get_email_footer_section()
                                                     );

                                    $register_template = $this->load->view('register_view',$templateData , true );
                                    /*file_put_contents( APPPATH.'../logs/register.html', $register_template );*/

                                    $emailRes = $this->_send_email( $register_template, $user->email , 'Registration' );
                                }

                                $data['data'] = array(
                                        'token'        => $access_token,
                                        'email'        => $user->email,
                                        'user_id'      => $user->id,
                                        'role'         => $user->role,
                                        'is_subscribe' => $updateArr['is_subscribe'],
                                        'is_active'    => $updateArr['is_active'],
                                        'is_verified'  => $updateArr['is_verified'],
                                        'meta'         => $this->get_meta_by_id( $user->id ),
                                        'balance'      => $this->get_balance( $user->id )                                   
                                    );

                           } else {
                                $data['status'] = 400;
                                $data['code'] = 'USR029';
                           }
                           

                        } else {
                            $data['status'] = 400;
                            $data['code'] = 'USR032';
                        }

                    } else {
                        $data['status'] = 400;
                        $data['code'] = 'USR031';
                        $data['data'] = $response['error'];
                    }

                } else if( strcmp("paypal", $this->input->post('payment_mode') ) == 0 ) {
                    /**
                     * Paypal payment
                    */

                    $amount = $this->get_site_info('subscription_cost');
                    $paypal_args = array(
                                'custom'      => json_encode( array('email' => $user->email ) ),
                                'currency'    => 'USD',
                                'amount'      => $amount,
                                'description' => 'Subscription'
                            );
                   $payment = $this->paypal_lib->setPayment( $paypal_args );

                   $data['status'] = $payment['code'];
                   $data['code'] =  ( $payment['code'] == 200 ) ?'USR053' : 'USR054';
                   $data['data'] = $payment['message'];
                }
                
            } else {
                $data['status'] = 400;
                $data['code'] = 'USR030';
            }
        } else {
            $data['status'] = 400;
            $data['code'] = 'USR008';
        }
        $this->response( $data );
    }

    protected function _stripe_create_customer( $data = NULL ) {
        try{
            if( is_null ( $data ) ) {
                return  array( 'status' => false, 'error' => 'empty data' );
            }
           $response = Customer::create( $data );
           return  array('status' => true, 'result' => $response );
        } catch( Exception $e ) {

            return  array( 'status' => false, 'error' => $e->getMessage() );
        }
    }

    protected function _stripe_subscribe_customer_to_plan( $data = NULL ){
        try{
            if( is_null ( $data ) ) {
                return  array( 'status' => false, 'error' =>'empty data'  );
            }

            $response = Subscription::create( $data );
            return  array('status' => true, 'result' => $response );
        } catch( Exception $e ) {
            return  array( 'status' => false, 'error' => $e->getMessage() );
        }
    }

    public function user_verification_put(){
        $data = NULL;
        $_POST = json_decode( trim( file_get_contents( 'php://input') ), true );
        /* email verification code*/
        $where = array();


        $code = $this->input->post('verification_code');
        if( !is_null( $code ) ){

            $where['verification_token'] = $code;
            $query = $this->user_model->get_user( $where );          

            if( $query->num_rows() > 0 ) {
                $row = $query->row();

                $updateUser = $this->user_model->update(  array( 'is_verified' => 1 ), $where );
                if( $updateUser ) {
                    $data['status'] = 200;
                    $data['code'] = 'USR044';
                } else {
                    $data['status'] = 400;
                    $data['code'] = 'USR045';
                }

            } else {
                $data['status'] = 400;
                $data['code'] = 'USR048';
            }
        } else {
            $data['status'] = 400;
            $data['code'] = 'USR049';
        }

        $this->response( $data );
    }

    public function user_resent_verification_get() {
        $current_user  = $this->current_user;
        $data = array();
        if( !$current_user ){
            $data['status'] = 400;
            $data['code'] = 'USR050';
        }
        $get_user = $this->user_model->get( array( 'id' => $current_user->userId ) ); 
        if( count( $get_user ) >= 1 ) {
            $user = $get_user[0];
            $verification_token = generate_reset_token(   $current_user->role . ' | '  .  $current_user->email );

            $this->user_model->update( array( 'verification_token'=> $verification_token ), array( 'id' => $current_user->userId ) );

            $url = $this->get_url_http( 'email-verify/' . $verification_token );            
            
            $user_name = get_meta_data( $user->id, 'nick_name', true );
            /* Sending token to email */
            $templateData = array(
                                    'page_title'         => 'Resent Email Verfication',
                                    'name'               => ( $user_name != '' ) ? $user_name : $user->email,
                                    'body_data'          => "Here is your email verification code",
                                    'show_token'         =>  true,
                                    'verification_url'   => $url,
                                    'verification_label' => 'Click to verify',
                                    'footer_text'        => $this->get_email_footer_section()
                             );

            $verification_template = $this->load->view('verification_view',$templateData , true );
            $emailRes = $this->_send_email( $verification_template, $user->email , $templateData['page_title'] );
            if( $emailRes ){
                $data['status'] = 200;
                $data['code'] = 'USR051';
            } else {                
                $data['status'] = 400;
                $data['code'] = 'USR052';
            }

        } else {
            $data['status'] = 400;
            $data['code'] = 'USR050';
        }
        $this->response( $data );
    }

}
/* End of file*/
