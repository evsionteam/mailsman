<?php
/**
 * Generate token key
 **/
function generate_reset_token( $string ) {
    $intermediateSalt = md5(uniqid(rand(), true));
    $salt = substr($intermediateSalt, 0, 6);
    return hash("sha256", $string . $salt);
}

function get_meta_data( $user_id , $key = NULL , $valueOnly = FALSE ) {
	$ci = &get_instance();
	$ci->load->model('user_model');

	return $ci->user_model->_meta_row( $user_id, $key, $valueOnly );
}

function update_meta_data( $user_id, $key, $value ) {
	$ci = &get_instance();
	$ci->load->model('user_model');
	return $ci->user_model->save_metas( array( $key => $value ), $user_id );
}

function remove_meta_data( $user_id, $key ) {
	$ci = &get_instance();
	$ci->load->model('user_model');
	return $ci->user_model->delete_metas( $user_id, $key );
}

function checkedifEqual( $value1, $value2 ) {
	if( (string) $value1 === (string) $value2 ){
		return true;
	} else {
		return false;
	}
}
