<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * User_model class.
 *
 * @extends Base_model
 */
class User_model extends Base_model {

	
	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {

		parent::__construct();
		$this->load->database();
		
		$this->table = $this->prefix . "users";
		$this->table_meta = $this->prefix . "usermeta";
		$this->table_address = $this->prefix . "mm_address";
		$this->join_table = $this->prefix . "mm_address as `mma`";
	}

	/**
	 * create_user function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function insert( $insertArr ) {

		if( isset( $insertArr['password'] ) ) {
			$insertArr['password'] = $this->hash_password(  $insertArr['password'] );
		}

		$this->db->insert( $this->table , $insertArr );
        return $this->db->insert_id();
	}

	public function update( $updateArr , $where ) {

		if( isset($updateArr['password'] ) ) {
			$updateArr['password'] = $this->hash_password(  $updateArr['password'] );
		}
		return  $this->db->update( $this->table, $updateArr ,$where );
	}

	public function delete( $where ) {
		return  $this->db->delete( $this->table, $where );
	}

	public function save_metas( $update_meta, $user_id ){
		foreach ( $update_meta as $meta_key => $value ):
            if( $data = $this->_meta_row( $user_id, $meta_key ) ){
                $update = array('value' => $value);
                $where = array(
                    'key'=> $meta_key,
                    'user_id'=> $user_id
                );
                $this->db->update($this->table_meta,$update,$where);
            }else{
                $insertArr = array(
                    'user_id'=> $user_id,
                    'key'   => $meta_key,
                    'value' => $value,
                    'created_at' => date( 'Y-m-d H:i:s' ),
                    'updated_at' => date( 'Y-m-d H:i:s' )
                );
                $this->db->insert($this->table_meta,$insertArr);
            }
		endforeach;
		return TRUE;
	}

	public function _meta_row( $user_id, $meta_key = NULL, $valueOnly = false ) {
		$where = array(
						 'user_id' => $user_id
					 );

		if( !is_null ( $meta_key ) ) {
			$where['key'] = $meta_key;
		}
		$res = $this->db->get_where(
										$this->table_meta,
										$where
									);

		if( $res->num_rows() > 0  ) {
			if( $res->num_rows() == 1 ) {
				if( $valueOnly ){
					return $res->row('value');
				} else {
					return $res->result();
				}
			} else {
				return $res->result();
			}

		} else{
			return FALSE;
		}
	}

	public function delete_metas( $user_id, $key = NULL ){
        $this->db->where('user_id', $user_id);
        if(!is_null($key)){
            $this->db->where('key', $key);
        }
        return $this->db->delete($this->table_meta);
	}


	/**
	 * resolve_user_login function.
	 *
	 * @access public
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function resolve_user_login($email, $password) {

		$this->db->select('password');
		$this->db->from( $this->table );
		$this->db->where('email', $email);
		$hash = $this->db->get();

		$hash_password = $hash->row('password');

		return $this->verify_password_hash($password, $hash_password);
		// return $this->verify_password_hash('ashok', $this->hash_password('ashok'));

	}

	/**
	 * get_user_id_from_email function.
	 *
	 * @access public
	 * @param mixed $email
	 * @return int the user id
	 */
	public function get_user_id_from_email($email) {

		$this->db->select('id');
		$this->db->from( $this->table );
		$this->db->where('email', $email);
		return $this->db->get()->row('id');

	}

	/**
	 * get_user function.
	 *
	 * @access public
	 * @param  [array] $where    [description]
	 * @param  [array] $order_by [description]
	 * @return object the user object
	 */
	public function get_user( $where = NULL, $order_by = NULL, $limit = NULL, $like = NULL ) {

		$this->db->select( ' `usr`.`id`, `usr`.`password`, `usr`.`email`, `usr`.`role`, `usr`.`is_active`, `usr`.`is_verified`, `usr`.`is_subscribe`,`mma`.`id` as `mm_id`, `mma`.`address_line1`, `mma`.`address_line2`, `mma`.`city`, `mma`.`state`, `mma`.`country`, `mma`.`zip`, `mma`.`phone` ' );

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){

			foreach( $like as $key => $val ){
				$this->db->or_like( $key, $val ,'both');
			}

		}

		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}

		$this->db->join($this->join_table,' `usr`.`mm_address` = `mma`.`id` ','left');

		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}


		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}

		$this->db->from($this->table . ' as `usr` ');
		$query = $this->db->get();
		return $query;
	}

	public function get_user_count( $where = NULL, $order_by = NULL, $like = NULL ) {

		$this->db->select( 'count(*) as total_rows ' );
		$this->db->from( $this->table . ' as `usr` ' );
		$this->db->join($this->join_table,' `usr`.`mm_address` = `mma`.`id` ','left');

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			// $this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
			foreach( $like as $key => $val ){
				$this->db->or_like( $key, $val ,'both');
			}
		}

		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}

		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}


		$query = $this->db->get( );

		if( $query->num_rows() > 0 ) {
			return $query->row('total_rows');
		}
		return 0;
	}

	/**
	 * hash_password function.
	 *
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {

		return password_hash($password, PASSWORD_BCRYPT);

	}

	/**
	 * verify_password_hash function.
	 *
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		return password_verify($password, $hash);
	}

	public function get_user_by_token( $token ) {
		$tokenRes = $this->db->get_where( $this->table, array( 'token'=> $token ) );

        if ($tokenRes->num_rows() > 0 ) {
            $response = $this->db->get_where( $this->table, array( 'id' => $tokenRes->row()->id ) );
            return $response->row();
        }
        return FALSE;
	}

}
