<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo $page_title;?></title>

  <style type="text/css">
    body{
      background-color: rgb(242, 242, 242);
    }
  </style>

</head>
<body>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module" bgcolor="#303030" c-style="not6BG" style="background-color: rgb(242, 242, 242);">
    <tbody><tr mc:repeatable="">
      <td align="center" id="not6">
        <div mc:hideable="">
          
          <!-- Mobile Wrapper -->
          <table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2">
            <tbody><tr>
              <td width="100%" align="center">
                
                <div class="sortable_inner ui-sortable">
                  <!-- Space -->
                  <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
                    <tbody><tr>
                      <td width="600" height="50"></td>
                    </tr>
                  </tbody></table><!-- End Space -->
                  
                  <!-- Space -->
                  <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
                    <tbody><tr>
                      <td width="600" height="50"></td>
                    </tr>
                  </tbody></table><!-- End Space -->
                  
                  <!-- Start Top -->
                  <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#4edeb5" style="border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: #5d9bfb;" object="drag-module-small" c-style="not6GreenBG">
                    <tbody><tr>
                      <td width="600" valign="middle" align="center" class="logo">
                        
                        <!-- Header Text --> 
                        <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
                          <tbody><tr>
                            <td width="100%" height="30"></td>
                          </tr>
                          <tr>
                            <td width="100%"><span object="image-editable"><img editable="true" src="<?php echo (!isset($site_logo)) ? site_url('uploads/logo/logo.png') : $site_logo;?>" alt="" border="0" mc:edit="39"></span></td>
                          </tr>
                          <tr>
                            <td width="100%" height="30"></td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>
                  </tbody></table>
                  
                </div>
                
                <!-- Mobile Wrapper -->
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small" style="-webkit-border-bottom-left-radius: 5px; -moz-border-bottom-left-radius: 5px; border-bottom-left-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-bottom-right-radius: 5px; border-bottom-right-radius: 5px;">
                  <tbody><tr>
                    <td width="600" align="center" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);" bgcolor="#ffffff" c-style="not6Body">
                      
                      <div class="sortable_inner ui-sortable">
                        
                        <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
                          <tbody><tr>
                            <td width="600" valign="middle" align="center">
                             
                              <table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
                                <tbody><tr>
                                  <td width="100%" height="30"></td>
                                </tr>
                              </tbody></table>
                            </td>
                          </tr>
                        </tbody></table>