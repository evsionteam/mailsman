<?php $this->load->view('header_email');?>
<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">                       
			<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
				<tbody><tr>
					<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 23px; color: #3f4345; line-height: 30px; font-weight: 100;" t-style="not6Headline" mc:edit="40" object="text-editable">
						<!--[if !mso]><!--><span style="font-family: 'proxima_novathin', Helvetica; font-weight: normal;"><!--<![endif]--><singleline>Hello <?php echo $name; ?> </singleline><!--[if !mso]><!--></span><!--<![endif]-->
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">
			<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
				<tbody><tr>
					<td width="100%" height="30"></td>
				</tr></tbody>
			</table>
		</td>
	</tr>
</tbody></table>

<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">
			<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile" c-style="not4GreenBG" object="drag-module-small" style="">
				<tbody><tr>
					<td width="600" valign="middle" align="center">

						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter">
							<tbody><tr>
								<td valign="middle" width="100%" style="text-align: center; font-family: Helvetica, Arial, sans-serif; font-size: 14px; color: #3f4345; line-height: 24px; " t-style="not4BodyText" class="fullCenter" mc:edit="26" object="text-editable">
									<!--[if !mso]><!--><span style="color:#3f4345;font-family: 'proxima_nova_rgregular', Helvetica; font-weight: normal;"><!--<![endif]--><multiline><?php echo $body_data;?><br><br><span style="color:#333;text-align: center;font-size: 25px;font-weight: bold;display: block;"><?php echo $reset_token;?></span></multiline><!--[if !mso]><!--></span><!--<![endif]-->
								</td>
							</tr>
						</tbody></table>
					</td>
				</tr>
			</tbody></table>
		</td>
	</tr>
</tbody></table>



<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff" c-style="not6Body" object="drag-module-small" style="background-color: rgb(255, 255, 255);">
	<tbody><tr>
		<td width="600" valign="middle" align="center">
			<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
				<tbody><tr>
						<td width="100%" height="40"></td>
					</tr></tbody>
			</table>
		</td>
	</tr></tbody>
</table>
<?php $this->load->view('footer_email');?>