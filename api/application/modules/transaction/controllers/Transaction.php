<?php
defined( 'BASEPATH' ) OR exit('No direct script access allowed');


/**
 * Transaction class
 *
 * @extends MY_Controller
 */
class Transaction extends MY_Controller {

	/**
	 * __construct function
	 *
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();
		$this->load->model('transaction_model');
		$this->load->model('users/user_model','user_model');
	}

	public function transaction_get( $page = false, $search = false  ) {
		$this->load->config('pagination');

		$current_user      = $this->current_user;
        $current_user_role = $current_user->role;
        $current_user_id   = $current_user->userId;

	   	$data = array();
	   	$where = NULL;
	   	if( "administrator" != $current_user_role ) {	   		
		   	$where  = array(
		   					'user_id' => $current_user_id
				   		);

	   	} else {
	   		if( $search ) {
	   			$where  = array(
		   					'user_id' => $search
				   		);
	   		}
	   	}

   		$order_by = array(
						'id' => 'DESC'
					);
		$limit = array();
		if(  false !== $page ) {			
			$page = ( $page < 0 ) ? 1: $page;
    		$limit['per_page'] = $this->config->item('per_page');
    		$limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );    		
		}



		
	   	$response = $this->transaction_model->get( $where, $order_by, $limit );
	   	$count = $this->transaction_model->get_count( $where, $order_by );
	   			
	   	if( $response ){
	   		foreach ( $response as $key => $val ) {
	   			$text = '';
	   			$person = 'You';
	   			$person1 = 'Your';
	   			if ( 'administrator' === $current_user_role ){
	   				$act_user = $this->user_model->get_user( array( '`usr`.`id`'  => $val->user_id ) )->row();
	   				if( count( $act_user ) > 0 ){
	   					$person =  $act_user->email.' has';
	   					$person1 =  $act_user->email.'\'s';
	   				}
	   			}
	   			if ( 'load' == $val->transaction_type ) {
	   				$text = sprintf( '%s %s amount of %s%s%s via %s', $person, 'loaded', '$', $val->transaction_amount, '(USD)', $val->payment_method );
	   			} else if( 'deduct' == $val->transaction_type ) {
	   				$text = sprintf( '%s balance has been %s by %s%s%s', $person1, 'deducted', '$', $val->transaction_amount, '(USD)' );
	   				$response[$key]->message = $val->remarks;
	   			}

	   			$response[$key]->text = $text;
	   		}

	   		$data['status']     = 200;
	   		$data['code']       = "TRAN001";
	   		$data['total_rows'] = $count;
	   		$data['data']       = $response;
	   	} elseif ( is_null( $response ) ) {
	   		$data['status']     = 200;
	   		$data['code']       = "NOT_FOUND";
	   		$data['data']       = [];
	   		$data['total_rows'] = 0;
	   	} else {
	   		$data['status']     = 400;
	   		$data['code']       = "TRAN002";
	   		$data['data']       = [];
	   		$data['total_rows'] = 0;
	   	}	   		
		$this->response( $data );

	}
	   	  
	   		   	

}