<?php
defined( 'BASEPATH' ) OR exit('No direct script access allowed');


/**
 * Transaction class
 *
 * @extends MY_Controller
 */
class Transaction_model extends Base_model {
	

	/**
	 * __construct function
	 *
	 * @access public
	 * @return void
	 */
	public function __construct(){
		parent::__construct();	
	   	$this->table = $this->prefix . "credit";
	}
}