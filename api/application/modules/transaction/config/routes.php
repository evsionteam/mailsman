<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

$route['transactions']['GET'] = 'transaction/transaction/transaction';
$route['transactions/(:num)']['GET'] = 'transaction/transaction/transaction/$1';
$route['transactions/(:num)/(:num)']['GET'] = 'transaction/transaction/transaction/$1/$2';