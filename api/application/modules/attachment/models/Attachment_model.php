<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Attachment_model class.
 * 
 * @extends Base_Model
 */
class Attachment_model extends Base_model {
	
	public function __construct() {
		parent::__construct();
		$this->table = $this->prefix . "attachments";
	}
}

/* End of file*/
