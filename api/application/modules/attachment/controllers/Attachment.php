<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Attachments class.
 * 
 * @extends CI_Controller
 */
class Attachment extends MY_Controller {

	protected $dest;

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
		$config_upload = $this->load->config('upload', TRUE );		
		$this->dest = $config_upload['upload_path'];
		$this->load->model('attachment_model');
	}

	public function index(){}

	public function attachment_post(){
		
		
		$data = array();
		$current_user  = $this->current_user;		

		// $_POST = json_decode( trim( file_get_contents('php://input') ), true );
		
			/*$this->load->library('form_validation');

		$rules = $this->_form_rules();

		$this->form_validation->set_rules($rules);

	if ( $this->form_validation->run() == FALSE ) {
		    $errors = $this->form_validation->error_array();
		    $err = array();

		    foreach( $errors as $error => $err_val  ):
		        $err[] = $err_val;
		    endforeach;

		    $data['status'] = '400';
		    $data['code'] = 'ATT002';
		    $data['data']  = $err;

		} else {*/
			
			$insertArr  = array();
			$uploaded_data = $this->uploader();
			if ( is_array( $uploaded_data) && count( $uploaded_data ) > 0 ) {

				// $insertArr['user_id']       = $input_data['user_id'];
				// $insertArr['is_paid']       = $input_data['is_paid']; // 1 paid | 0 not paid
				$insertArr['is_paid']       = 0; // 1 paid | 0 not paid
				$insertArr['charge']        = $this->input->post('charge'); 
				$insertArr['is_archive']    = 0;  // 1 archive  | 0  not archived

				// decode to image and save them 
				// $filename = $this->base64_decode->base64_decoder( $input_data['file'], $this->dest.$attachment_id );
				
				$insertArr['filename'] = $uploaded_data['file_name'];
				
				if( $response = $this->attachment_model->insert( $insertArr ) ){

					$data['status'] = 200;
					$data['code'] = 'ATT001';
					$data['data'] = array ( 'id' => $this->db->insert_id() );

				} else {
					$data['status'] = 400;
					$data['code'] = 'ATT012';					
				}
			} else {

				$data['status'] = 400;
				$data['code'] = 'ATT011';
				$data['data'] = array('error' => $uploaded_data );
			}	
		/*} */

		$this->response( $data );
	}

	public function attachment_get( $attachment_id = false, $page = false, $search = false ) {
		
		$this->load->config('pagination');
		// check if user have admin role or not
		$current_user = $this->current_user;
	   	$data = array();
	   	
	   	$where = array( 'is_archive' => 0, 'is_paid' => 0 );
	   	
	   	if( $attachment_id !== false  && ( is_numeric( $attachment_id ) && $attachment_id != 0 ) ) {
	   		$where['id'] = $attachment_id ;
	   	}

	   	$order_by = array(
	   					'id' => 'DESC'
	   				);

		$limit = array();
		if(  false !== $page ) {
			$page = ( $page < 0 ) ? 1: $page;
    		$limit['per_page'] = $this->config->item('per_page');
    		$limit['offset'] = ( $page - 1 ) * ( $limit['per_page'] );
		}

		$like = NULL;

		if( false !== $search ) {
			$like = array(
					'filename' => $search
				);
		}

	   	$response = $this->attachment_model->get( $where, $order_by, $limit, $like );
	   	$count = $this->attachment_model->get_count( $where, $order_by, $like );

	   	if( $response ){
	   		
	   		$data['status']   = 200;
	   		$data['code'] = 'ATT006';
	   		$data['total_rows'] = $count;
	   		$data['data']   = $response;
	   		
	   	} else if( is_null( $response )){
	   		$data['status']   = 200;
	   		$data['code'] = 'NOT_FOUND';
	   		$data['data']   = NULL;

	   	}  else {
	   		$data['status']   = 400;
	   		$data['code'] = 'ATT007';
	   		
	   	}		

		$this->response( $data );
	}

	public function attachment_delete(){
		// delete
	   	$data = array();

		$attachment_id = $this->uri->segment( $this->uri->total_segments(), false  );

		if ( is_numeric( $attachment_id ) && $attachment_id != false) {
			//  is_archive  is set to 1 to flagged as deleted
			$deleteRes = $this->attachment_model->update(  array( 'is_archive'=> '1' ), array('id' => $attachment_id ) ); 

			if( $deleteRes ){
				$data['status']   = 200;
				$data['code'] = 'ATT003';
			} else {
				$data['status'] = 400;
				$data['code'] = 'ATT004';
			}
			
		} else {
			$data['status'] = 400;
			$data['code'] = 'ATT005';
		}
		$this->response( $data );
	}

	private function _form_rules() {
	    $rules = array(
	        /*array(
	            'field' => 'user_id',
	            'label' => 'User ID',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'ATT008',
	                                        'field'   => 'user_id'
	                                    )
	            )
	        ),array(
	            'field' => 'is_paid',
	            'label' => 'Is Paid?',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'ATT009',
	                                        'field'   => 'is_paid'
	                                    )
	            )
	        ),array(
	            'field' => 'file',
	            'label' => 'File',
	            'rules' => 'trim|required',
	            'errors' => array(
	                'required' =>  array(
	                                        'code'    => 'ATT010',
	                                        'field'   => 'file'
	                                    )
	            )
	        )*/
	        
	    );
	    
	    return $rules;
	}

}
/* End of file*/