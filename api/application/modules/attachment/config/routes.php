<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//$route['default_controller'] = 'welcome';

// $route['attachment']['POST'] = "attachment/attachment/attachment";
$route['attachment']['POST'] = "attachment/attachment/attachment";

$route['attachment']['GET'] = "attachment/attachment/attachment";

$route['attachment/(:num)']['GET'] = "attachment/attachment/attachment/$1";

/* attachment/{noID=0}/{page}/ =>pagination */
$route['attachment/(:num)/(:any)']['GET'] = "attachment/attachment/attachment/$1/$2";

/* attachment/{noID=0}/{page}/{search}  =>search */
$route['attachment/(:num)/(:any)/(:any)']['GET'] = "attachment/attachment/attachment/$1/$2/$3";


$route['attachment/(:num)']['DELETE'] = "attachment/attachment/attachment/$1";