<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Code extends REST_Controller {
	protected $code;
	public function __construct() {
	 	parent::__construct();
	 	$this->code = $this->load->config('code', TRUE );
	}
	/**
	 * @api {get} /code/ Code status list
	 * @apiName codeList
	 * @apiGroup  Code
	 *
	 * @apiSuccessExample {json} Response Example:
	 * HTTP/1.1 200 OK
	*                  {
	*						"USR001": "The email address already exists.",
	*						"USR002": "The passwords did not match.",
	*						"USR003": "There was a problem creating new account.",
	*						"USR004": "Email address is not registered.",
	*						"USR005": "Wrong password for the email.",
	*						"USR006": "No admin privileges.",
	*						"USR007": "Problem deleting user.",
	*						"USR008": "User not found.",
	*						"USR009": "There was a problem while listing users.",
	*						"USR010": "You don't have permission.",
	*						"USR011": "There was a problem while updating.",
	*						"USR012": "Unable to send the reset token.",
	*						"USR013": "Unable to update the password.",
	*						"USR014": "Either Email address is not registered or the reset token did not match.",
	*						"SRV001": "There was a problem creating service.",
	*						"SRV002": "You don't have permission.",
	* 						"SRV003": "Service not found.",
	*						"SRV004": "There was a problem while updating.",
	*						"SRV005": "There was a problem deleting service.",
	*						"SRV006": "There was a problem while listing services."
	*					}	
	 */

	public function index_get() {		
		$code_data = array();
		foreach( $this->code as $key => $value ){
			$code_data[] = array($key => $value);
		}
		echo json_encode( $code_data  );
	}
}
