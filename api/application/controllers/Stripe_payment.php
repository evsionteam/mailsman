<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stripe_payment extends CI_Controller {
	
	private $stripe_config;
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');
		$this->stripe_config = $this->load->config('stripe', TRUE );
		$this->load->library('stripe_lib');
	}
	
	public function index(){		
		?>
		<form action="<?php echo site_url('stripe_payment/checkout');?>" method="POST">
		  <script
		    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
		    data-key="<?php echo $this->stripe_config['stripe_key_public'];?>"
		    data-amount="999"
		    data-name="Demo Site"
		    data-description="Widget"
		    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
		    data-locale="auto">
		  </script>
		</form>
		<?php	
	}		

	public function checkout() {
		// echo $this->stripe_config['stripe_key_secret'];
			/*{
				stripeToken: "tok_19Uq25APR8WeGXmCEgmaKtSd",
				stripeTokenType: "card",
				stripeEmail: "cloudashokm@gmail.com"
			}*/
			$this->stripe_lib->charge('10', $_POST );
		
	}

}
