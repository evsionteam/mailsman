<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 /**
  * *****************************
  *              User
  * ******************************
  */
/**
        * @api {post} /user/ Register
        * @apiName RegisterUser
        * @apiGroup User
        * 
        * @apiParam {String} email User's email.
        * @apiParam {Password} password User's password.
        * @apiParam {Password} reTypePassword Retype the password.
        *
        * @apiSuccess {Integer} userID Inserted User's id.
        *
        * @apiSuccessExample {json} Response(example):
        *       HTTP/1.1 201 Created
        *       {
        *           "status": 201,
        *           "data": {
        *              "userID": 1,
        *              "message": "Congratulation! Your account has been created."
        *           }
        *       }
        *
        * @apiError USR001 The Email address already exists.
        * @apiError USR002 The passwords did not match.
        * @apiError USR003 There was a problem creating new account.
        *
        * @apiErrorExample {json} Response(example):
        *       HTTP/1.1 400 Bad Request
        *       {
        *           "status": 400,
        *           "code": "USR001",
        *           "data": "Email address already exists."
        *
        *       }
    */
   
    /**
         * @api {post} /user/login/ Login
         * @apiName LoginUser
         * @apiGroup User
         *
         *
         * @apiParam {String} email User's email.
         * @apiParam {Password} password User's password.
         *
         * @apiSuccess {String} token Generated token.
         * @apiSuccess {String} email User's email.
         *
         * @apiSuccessExample {json} Response(example):
         *       HTTP/1.1 200 OK
         *       {
         *           "status": 200,
         *           "data": {
         *              "token": "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9bq4FhY",
         *              "email": "youremail@gmail.com"
         *           }
         *       }
         *
         * @apiError USR004 Email address is not registered.
         * @apiError USR005 Wrong password for the email.
         *
         * @apiErrorExample {json} Response(example):
         *       HTTP/1.1 400 Bad Request
         *       {
         *           "status": 400,
         *           "code": "USR004",
         *           "data": "Email address is not registered."
         *       }
         *
     */
    
    /**
        * @api {PUT} /user/:id Update
        * @apiName UpdateUser
        * @apiGroup User
        *
        * @apiHeader {String} x-access-token Users unique access-key.
        *
        * @apiParam {Integer} id Users unique ID.
        *
        * @apiSuccess {String} data Response message.
        *
        * @apiSuccessExample {json} Response(example):
        *       HTTP/1.1 200 OK
        *       {
        *           "status": 200,
        *           "data": {
        *           			"address": "address",
        *           			"contact": "12332323"
        *                   }
        *       }
        *
        * @apiError USR008 User not found.
        * @apiError USR010 You don\'t have permission.
        * @apiError USR011 There was a problem while updating.
        *
        @apiErrorExample {json} Response(example):
        *       HTTP/1.1 404 Not Found
        *       {
        *           "status": 404,
        *           "code": "USR008",
        *           "data": "User not found."
        *       }
        *
    */    
   
   /**
        * @api {DELETE} /user/:id Delete
        * @apiName DeleteUser
        * @apiGroup User
        *
        * @apiHeader {String} x-access-token Users unique access-key.
        *
        * @apiParam {Integer} id Users unique ID.
        *
        * @apiSuccess {String} data Response message.
        *
        * @apiSuccessExample {json} Response(example):
        *       HTTP/1.1 200 OK
        *       {
        *           "status": 200,
        *           "data": "User deleted successfully."
        *       }
        *
        * @apiError USR007 There was a problem deleting.
        * @apiError USR008 User not found.
        * @apiError USR010 You don\'t have permission.
        *
        @apiErrorExample {json} Response(example):
        *       HTTP/1.1 403 Forbidden
        *       {
        *           "status": 403,
        *           "code": "USR010",
        *           "data": "You don\'t have permission."
        *       }
    */
   
   /**
        * @api {get} /user/ User List
        * @apiName ListUser
        * @apiGroup User
        * 
        * @apiHeader {String} x-access-token Users unique access-key.
        *
        * @apiSuccess {String} data Response message.
        *
        * @apiSuccessExample {json} Response(example):
        *       HTTP/1.1 200 OK
        *       {
        *           "status": "200",
        *          "data":
        *             {
        *                        "id": "4",
        *                        "email": "youremail@email.com",
        *                        "password": "$2y$10$az46soN6cVJvrHCLZe8HXOkjs.1CaKDpb9wRwz6gIcMKRtShFqzne",
        *                        "reset_token": "c452c7ed5a01f8ac2d906c9f34f2286bed0ce2fc8318ef37ea7174971fd9e369",
        *                        "role": "1",
        *                        "is_active": "1",
        *                        "created_at": "2016-12-08 10:35:26",
        *                        "updated_at": "2016-12-26 09:51:56"
        *                },
        *       }
        *
        * @apiError USR008 User not found.
        * @apiError USR009 There was a problem while listing users.
        * @apiError USR010 You don\'t have permission.
        *
        * @apiErrorExample {json} Response(example):
        *       HTTP/1.1 404 Not Found
        *       {
        *           "status": 404,
        *           "code": "USR008",
        *           "data": "User not found."
        *       }
    */
   
    /**
     * @api {post} /user/forgot Forgot Password
     * @apiName ForgotPassword
     * @apiGroup User
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {String} email Users email.
     *
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *   HTTP/1.1 200 OK
     *   {
            "status": "200",
            "data": "Reset Token has been sent to your email account."
     *   }
     *
     *
     * @apiError USR004 Email address is not registered.
     * @apiError USR012 Unable to send the reset token.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 404,
     *           "code": "USR004",
     *           "data": "Email address is not registered."
     *       }
     */
    
     /**
     * @api {post} /user/recover Recover Password
     * @apiName RecoverPassword
     * @apiGroup User
     *
     *
     * @apiParam {String} email Users email.
     * @apiParam {String} reset_token Reset Token.
     * @apiParam {Password} password New Password.
     *
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *   HTTP/1.1 200 OK
     *   {
            "status": "200",
            "data": "Password updated successfully."
     *   }
     *
     *
     * @apiError USR013 Unable to update the password.
     * @apiError USR014 Either Email address is not registered or the reset token did not match.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 400 Bad Request
     *       {
     *           "status": 400,
     *           "code": "USR014",
     *           "data": "Either Email address is not registered or the reset token did not match."
     *       }
     */
    
    /**
            * @api {post} /user/subscribe/ subscribe
            * @apiName subscribeUser
            * @apiGroup User
            * 
            * @apiParam {String} token token.
            * @apiParam {String=stripe} payment_mode Paymment Mode
            *
            *
            * @apiSuccessExample {json} Response(example):
            *       HTTP/1.1 201 Created
            *       {
            *           "status": 201,
            *           "code": ""
            *       }
            * 
        */
       
     /**
         * @api {PUT} /user_verification Update
         * @apiName verifyUser
         * @apiGroup User
         *
         * @apiHeader {String} x-access-token Users unique access-key.
         *
         *@apiParam Email email Email Address
         *@apiParam String verification_token Verification Token
         *
         * @apiSuccess {String} data Response message.
         *
         * @apiSuccessExample {json} Response(example):
         *       HTTP/1.1 200 OK
         *       {
         *           "status": 200,
         *           "code" :  "USR044"
         *       }
         *
         * @apiError USR047 Opps..looks like your email address is not verified        
         *
         @apiErrorExample {json} Response(example):
         *       HTTP/1.1 404 Not Found
         *       {
         *           "status": 404,
         *           "code": "USR008"
         *       }
         *
     */     
    

 /**
  * *****************************
  *              Services
  * ******************************
  */
 
   /**
     * @api {post} /services/ Insert
     * @apiName InsertService
     * @apiGroup Services
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {String} title Service title.
     * @apiParam {String} description Service description.
     * @apiParam {Integer} cost Service cost.
     * @apiParam {Integer} has_note Whether the service provide option to enter additional note or not.
     * @apiParam {Integer} [position] Position/order. By Default it will add +1 to total rows.
     *
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 201 Created
     *       {
     *           "status": 201,
     *           "data": "Service inserted successfully."
     *       }
     *
     * @apiError SRV001 There was a problem creating service.
     * @apiError SRV002 You don't have permission.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 503 Service Unavailable
     *       {
     *           "status": 503,
     *           "code": "SRV001",
     *           "data": "There was a problem creating service."
     *
     *       }
     */
    
      /**
     * @api {PUT} /services/:id Update
     * @apiName UpdateService
     * @apiGroup Services
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {String} title Service title.
     * @apiParam {String} description Service description.
     * @apiParam {Integer} cost Service cost.
     * @apiParam {Integer} has_note Whether the service provide option to enter additional note or not.
     * @apiParam {Integer} [position] Position/order. By Default it will add +1 to total rows
     *
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
     *           "status": 200,
     *           "data": "Service updated successfully."
     *       }
     *
     * @apiError SRV002 You don't have permission.
     * @apiError SRV003 Service not found.
     * @apiError SRV004 There was a problem while updating.
     *
      @apiErrorExample {json} Response(example):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 404,
     *           "code": "SRV003",
     *           "data": "Service not found."
     *       }
     *
     */
    
      /**
     * @api {DELETE} /services/:id Delete
     * @apiName DeleteService
     * @apiGroup Services
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {Integer} id Service ID.
     *
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
     *           "status": 200,
     *           "data": "Service deleted successfully."
     *       }
     *
     * @apiError SRV002 You don't have permission.
     * @apiError SRV003 Service not found.
     * @apiError SRV005 There was a problem deleting service.
     *
      @apiErrorExample {json} Response(example):
     *       HTTP/1.1 403 Forbidden
     *       {
     *           "status": 403,
     *           "code": "SRV002",
     *           "data": "You don't have permission."
     *       }
     */
    
     /**
     * @api {get} /services/{id}/{paged} Service List
     * @apiName ListServices
     * @apiGroup Services
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {Integer} [id=0] specific ID. If route for listing then value will be 0
     * @apiParam {Integer{>=0}} [paged=false] Pagination
     * 
     * 
     * @apiSuccess {String} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
                "status": "200",
                "data": [
                        {
                            "service_id": "10",
                            "title": "service 2",
                            "description": "Description",
                            "cost": "123",
                            "has_note": "1"
                        },
                        {
                            "service_id": "11",
                            "title": "scan",
                            "description": "scan only",
                            "cost": "123",
                            "has_note": null
                        }
     *          ]
     *       }
     *
     * @apiError SRV003 Service not found.
     * @apiError SRV006 There was a problem while listing services.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 404,
     *           "code": "SRV003",
     *           "data": "Service not found."
     *       }
     */
    

    /**
  * *****************************
  *              Messaging
  * ******************************
  */
 
  /**
     * @api {post} /message/ Insert
     * @apiName InsertMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     
     * @apiParam {File} file File.
     * @apiParam {Integer} charge charge.
     * @apiParam {Integer} staff_id Staff Id.
     * @apiParam {String} customer_id Customer Id.
     * @apiParam {Integer} attachment_id Attachment Id.
     * @apiParam {Integer} service_id Service Id.
     * @apiParam {Integer} group_id group Id. Initially it must be 0
     * @apiParam {String} message Message
     * @apiParam {String = unread,pending,completed} status Status 
     *
     * @apiParamExample JSON Example
	            {
					"staff_id"  : "67",
                    "customer_id" : "87",
                    "attachment_id":"123",
					"service_id":"123",
					"group_id":"123",
					"message" : "Scan and Scan",
					"is_read" : "unread"
				}

     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 201 Created
     *       {
     *           "status": 201,
     *           "code"  : "MSG001",
     *           "data": {
							"staff_id"  : "67",
							"customer_id" : "87",
							"attachment_id":"123",
							"message" : "Scan and Scan",
							"is_read" : "0"
						}
     *       }
     *
     * @apiError MSG002 Message not sent.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 503 Service Unavailable
     *       {
     *           "status": 503,
     *           "code": "MSG002",
     *           "
     *
     *       }
     */
    

    /**
     * @api {get} /message/{id}/{status}/{page}/{search} Message List
     * @apiName ListMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {Integer} [id=0] specific message ID. If route for listing then value will be 0
     * @apiParam {String=all,unread,pending,completed} [status=unread] status
     * @apiParam {Integer{>=0}} [page=false] Pagination
     * @apiParam {String} [search=false] Search Text
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
			  "status": 200,
			  "data": [
			    {
			      "id": "9",
			      "staff_id"  : "67",
                 "customer_id" : "87",
			      "attachment_id": "17",
			      "message": "scan and scan",
			      "status": "0",
			      "created_at": "2017-01-03 11:08:47",
			      "updated_at": "2017-03-22 10:03:27"
			    },
			    {
			      "id": "10",
			      "staff_id": "67",
			      "customer_id": "87",
			      "attachment_id": "0",
			      "message": "Your book has arrived.",
			      "status": "0",
			      "created_at": "2017-01-03 11:08:54",
			      "updated_at": "2017-03-22 12:34:59"
			    }
			  ]
			} 
     *
     * @apiError USR010 You don't have permission..
     * @apiError MSG004 Unable to list
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "MSG004"
     *       }
     *@apiErrorExample {json} Response(USR010):
     *       HTTP/1.1 403 Not Found
     *       {
     *           "status": 703,
     *           "code": "USR010"
     *       }
     */
    
    /**
     * @api {get} /message/{id} Message List Specific
     * @apiName ListMessageSpec
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
			  "status": 200,
			  "data": 
			    {
			      "id": "9",
			      "staff_id": "67",
			      "customer_id": "87",
			      "attachment_id": "17",
			      "message": "scan and scan",
			      "status": "0",
			      "created_at": "2017-01-03 11:08:47",
			      "updated_at": "2017-03-22 10:03:27"
			    }
			} 
     *
     * @apiError USR010 You don't have permission..
     * @apiError MSG004 Unable to list
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "MSG004"
     *       }
     *@apiErrorExample {json} Response(USR010):
     *       HTTP/1.1 403 Not Found
     *       {
     *           "status": 403,
     *           "code": "USR010"
     *       }
     */
    
    /**
     * @api {delete} /message/{id} Message delete
     * @apiName deleteMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
			  "status": 200,
			  "code": "MSG005"
			   
			} 
     *
     * @apiError MSG006 Message not deleted
     * @apiError MSG007 Message Id not exists
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "MSG004"
     *       }
     */
    
    /**
     * @api {PUT} /message/{id} Message Seen update
     * @apiName readMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
              "status": 200,
              "code": "MSG013"
               
            } 
     *
     * @apiError MSG007 Message Id not exists
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "MSG007"
     *       }
     */
    
    /**
     * @api {GET} /message_count/ Message count
     * @apiName countUnseenMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
              "status": 200,
              "code": "MSG014",
              "total_rows": {
                            "total": "1",
                            "completed": "1",
                            "pending": "1",
                            "unseen": "1",
                            "unread": "1"
                        }
               
            } 
     *
     * @apiError MSG007 Unable to generate
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "MSG015"
     *       }
     */
    

    /**
     * @api {put} /message/{id} Update
     * @apiName updateMessage
     * @apiGroup Message
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
 	 
     * @apiParam {Integer} staff_id Staff Id.
     * @apiParam {String} customer_id Customer Id.
     * @apiParam {Integer} attachment_id Attachment Id.
     * @apiParam {Integer} service_id Service Id.
     * @apiParam {Integer} group_id group Id. Initially it must be 0
     * @apiParam {String} message Message
     * @apiParam {String = unread,pending,completed} status Status 
      *
      * @apiParamExample JSON Example
 	            {
 					
 					"staff_id"  : "67",
 					"customer_id" : "87",
 					"attachment_id":"123",
 					"service_id":"123",
 					"group_id":"123",
 					"message" : "Scan and Scan",
 					"is_read" : "unread"
 				}

     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 201 Created
     *       {
     *           "status": 201,
     *           "code"  : "MSG001",
     *           "data": {
							"staff_id"  : "67",
							"customer_id" : "87",
							"attachment_id":"123",
							"message" : "Scan and Scan",
							"is_read" : "0"
						}
     *       }
     *
     * @apiError MSG002 Message not sent.
     *
     * @apiErrorExample {json} Response(example):
     *       HTTP/1.1 503 Service Unavailable
     *       {
     *           "status": 503,
     *           "code": "MSG002",
     *           "
     *
     *       }
     */
    


        /**
         * @api {get} /message/message_thread/{group_id} Message Thread
         * @apiName threadMessage
         * @apiGroup Message
         *
         * @apiHeader {String} x-access-token Users unique access-key.
         *
         * @apiSuccessExample json success example
                  {
                      "status": 200,
                      "code": "MSG016",
                      "data": [
                        {
                          "id": "290",
                          "group_id": "290",
                          "sender_id": "164",
                          "receiver_id": "168",
                          "attachment_id": "95",
                          "service_id": null,
                          "message": "Hello",
                          "status": "unread",
                          "is_seen": "1",
                          "created_at": "2017-04-24 16:31:26",
                          "updated_at": "2017-04-24 16:31:26"
                        },
                        {
                          "id": "291",
                          "group_id": "290",
                          "sender_id": "164",
                          "receiver_id": "168",
                          "attachment_id": null,
                          "service_id": "26",
                          "message": "This one is special",
                          "status": "pending",
                          "is_seen": "1",
                          "created_at": "2017-04-24 16:31:50",
                          "updated_at": "2017-04-24 16:31:50"
                        },
                        {
                          "id": "292",
                          "group_id": "290",
                          "sender_id": "164",
                          "receiver_id": "168",
                          "attachment_id": "96",
                          "service_id": "26",
                          "message": "This one is special",
                          "status": "completed",
                          "is_seen": "1",
                          "created_at": "2017-04-24 16:32:35",
                          "updated_at": "2017-04-24 16:32:35"
                        }
                      ]
                    }
               
         * @apiError MSG017 Unable to fetch message threading
         * @apiError MSG018 Group ID is missing
         *
         * @apiErrorExample {json} Response(MSG017):
         *       HTTP/1.1 400 Not Found
         *       {
         *           "status": 400,
         *           "code": "MSG017"
         *       }
         *@apiErrorExample {json} Response(MSG018):
         *       HTTP/1.1 400 Not Found
         *       {
         *           "status": 400,
         *           "code": "MSG018"
         *       }
         */
        

/**
  * *****************************
  *              Attachments
  * ******************************
  */
 

 /**
     * @api {post} /attachment/ Insert
     * @apiName InsertAttachment
     * @apiGroup Attachment
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     
     * @apiParam {Integer} is_paid Attachment Id.
     * @apiParam {String} file Base64 encodee files supported pdf, doc,text,image
     *
     * @apiParamExample JSON Example
	            {
					"user_id":"87",
					"is_paid" : "0",
					"file" : "data:image/png;base64,iVBORw...."
				}

     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response:
     *       HTTP/1.1 201 Created
     *       {
     *           "status": 200,
     *           "code"  : "ATT001"       
     *       }
     *
     * @apiError ATT002 Attachment not uploaded.
     *
     * @apiErrorExample {json} Response
     *       HTTP/1.1 400 Service Unavailable
     *       {
     *           "status": 400,
     *           "code": "ATT002"
     *
     *       }
     */
    
 /**
	 * @api {delete} /attachments/{id} Attachment delete
	 * @apiName deleteAttachment
	 * @apiGroup Attachment
	 *
	 * @apiHeader {String} x-access-token Users unique access-key.
	 * 
	 * @apiSuccess {JSON} data Response.
	 *
	 * @apiSuccessExample {json} Response(example):
	 *       HTTP/1.1 200 OK
	 *       {
			  "status": 200,
			  "code": "ATT003"
			   
			} 
	 *
	 * @apiError ATT004 Attachment not deleted
	 * @apiError ATT005 Attachment Id not found 
	 *
	 * @apiErrorExample {json} Response(MSG004):
	 *       HTTP/1.1 404 Not Found
	 *       {
	 *           "status": 400,
	 *           "code": "ATT004"
	 *       }
 */



 /**
     * @api {get} /attachment/{id}/{page}/{search} Attachments List
     * @apiName ListAttachments
     * @apiGroup Attachment
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * @apiParam {Integer} [id=0] specific ID. If route for listing then value will be 0
     * @apiParam {Integer{>=0}} [paged=false] Pagination 
     * @apiParam {String} [search=false] Search Text
     * 
     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *       {
				  "status": 200,
				  "code": "ATT006",
				  "data": [
				    {
				      "id": "20",
				      "user_id": "87",
				      "filename": "87.png",
				      "is_paid": "0",
				      "charge": "0",
				      "is_archive": "0",
				      "created_at": "2017-03-22 07:58:10",
				      "updated_at": "2017-03-22 07:58:10"
				    },
				    {
				      "id": "21",
				      "user_id": "87",
				      "filename": "87.txt",
				      "is_paid": null,
				      "charge": "0",
				      "is_archive": "0",
				      "created_at": "2017-03-22 08:10:02",
				      "updated_at": "2017-03-22 08:10:02"
				    }
				  ]
				}
     *
     * @apiError ATT007 Unable to list Attachment
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "ATT007"
     *       }
     */
    
    /**
     * @api {get} /attachment/{id} attachments List Specific
     * @apiName ListAttachmentSpec
     * @apiGroup Attachment
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     * 
     * @apiSuccess {JSON} data Response.
     *
     * @apiSuccessExample {json} Response(example):
     *       HTTP/1.1 200 OK
     *     {
				  "status": 200,
				  "code": "ATT006",
				  "data": {
				    "id": "20",
				    "user_id": "87",
				    "filename": "87.png",
				    "is_paid": "0",
				    "charge": "0",
				    "is_archive": "0",
				    "created_at": "2017-03-22 07:58:10",
				    "updated_at": "2017-03-22 07:58:10"
				  }
				} 
     *
     * @apiError ATT007 Unable to list Attachment
     *
     * @apiErrorExample {json} Response(MSG004):
     *       HTTP/1.1 404 Not Found
     *       {
     *           "status": 400,
     *           "code": "ATT007"
     *       }
     */
    

  
  /**
  * *****************************
  *              Options
  * ******************************
  */  
 

  /**
      * @api {post} /options/ Insert
      * @apiName InsertOptions
      * @apiGroup Options
      *
      * @apiHeader {String} x-access-token Users unique access-key.
      *
      * @apiParam {email} email Option Name.
      * @apiParam {title} title Option Value
      *
      * @apiParamExample JSON Example
 	            {
                    "email": "email@email.com",
                    "title": "title"
                }

      * @apiSuccess {JSON} data Response message.
      *
      * @apiSuccessExample {json} Response:
      *       HTTP/1.1 201 Created
      *       {
      *           "status": 200,
      *           "code"  : "OPT001"       
      *       }
      *
      * @apiError OPT002 Options not added.
      *
      * @apiErrorExample {json} Response
      *       HTTP/1.1 400 Service Unavailable
      *       {
      *           "status": 400,
      *           "code": "OPT002"
      *
      *       }
      */
 

 /**
     * @api {put} /options/{id} Update
     * @apiName updateOptions
     * @apiGroup Options
     *
     * @apiHeader {String} x-access-token Users unique access-key.
     *
     * @apiParam {integer} id Option Id in URL.
     * 
     * @apiParam {String} email Email Address.
     * @apiParam {String} title Title
     *
     * @apiParamExample JSON Example
 	            {
                    "email": "info@mailsman.com",
                    "title": "Mails Man"
                }

     * @apiSuccess {JSON} data Response message.
     *
     * @apiSuccessExample {json} Response:
     *       HTTP/1.1 201 Created
     *       {
     *           "status": 200,
     *           "code"  : "OPT001"       
     *       }
     *
     * @apiError OPT002 Options not added.
     *
     * @apiErrorExample {json} Response
     *       HTTP/1.1 400 Service Unavailable
     *       {
     *           "status": 400,
     *           "code": "OPT002"
     *
     *       }
     */
       
  /**
 	 * @api {delete} /options/{id} Options delete
 	 * @apiName deleteOptions
 	 * @apiGroup Options
 	 *
 	 * @apiHeader {String} x-access-token Users unique access-key.
 	 *
     * @apiParam {integer} id Option Id in URL.
 	 * 
 	 * @apiSuccess {JSON} data Response.
 	 *
 	 * @apiSuccessExample {json} Response(example):
 	 *       HTTP/1.1 200 OK
 	 *       {
 			  "status": 200,
 			  "code": "OPT003"
 			   
 			} 
 	 *
 	 * @apiError OPT004 Option not deleted
 	 * @apiError OPT005 Option Id not found 
 	 *
 	 * @apiErrorExample {json} Response(MSG004):
 	 *       HTTP/1.1 404 Not Found
 	 *       {
 	 *           "status": 400,
 	 *           "code": "OPT004"
 	 *       }
  */



  /**
      * @api {get} /options/{id}/{page}/{search} Options List
      * @apiName ListOptions
      * @apiGroup Options
      *
      * @apiHeader {String} x-access-token Users unique access-key.
      * @apiParam {Integer} [id=0] specific ID. If route for listing then value will be 0
      * @apiParam {Integer{>=0}} [paged=false] Pagination
      * @apiParam {String} [search=false] Search Text
      * 
      * 
      * @apiSuccess {JSON} data Response message.
      *
      * @apiSuccessExample {json} Response(example):
      *       HTTP/1.1 200 OK
      *       {
				  "code": 200,
				  "status": "OPT006",
				  "data": [				    
    				        {
                                "email": "info@mailsman"
                            },
                            {
                                "title": "mailsman"
                            }
				  ]
				}
      *
      * @apiError OPT007 Unable to list Options
      *
      * @apiErrorExample {json} Response(MSG004):
      *       HTTP/1.1 404 Not Found
      *       {
      *           "status": 400,
      *           "code": "OPT007"
      *       }
   */
     
    /**
      * @api {get} /options/{id} Options List Specific
      * @apiName ListOptionsSpec
      * @apiGroup Options
      *
      * @apiHeader {String} x-access-token Users unique access-key.
      * 
      * @apiSuccess {JSON} data Response.
      *
      * @apiSuccessExample {json} Response(example):
      *       HTTP/1.1 200 OK
      *    {
				  "status": 200,
				  "code": "OPT006",
				  "data": {
				    "id": "2",
				    "option_name": "title",
				    "option_value": "mailsman",
				    "created_at": "2017-03-22 08:38:09",
				    "updated_at": "2017-03-22 08:38:09"
				  }
				}
				      *
      * @apiError OPT007 Unable to list Option
      *
      * @apiErrorExample {json} Response(MSG004):
      *       HTTP/1.1 404 Not Found
      *       {
      *           "status": 400,
      *           "code": "OPT007"
      *       }
    */
   
