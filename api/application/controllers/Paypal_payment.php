<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Paypal_payment extends CI_Controller {
	
	private $stripe_config;
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('url');		
		$this->load->library('paypal_lib');
	}
	
	public function index(){		
		$paypal_args = array(
                                'custom' => json_encode(array('email' => 'cloudashokm@gmail.com')),
                                'currency' => 'USD',
                                'amount' => 100,
                                'description' => 'Subscription'
                            );
       $payment = $this->paypal_lib->setPayment( $paypal_args );
		print_r($payment);		
	
	}

	public function paypal_payment_com(){
		$payment = $this->paypal_lib->completePayment(  $this->input->get() );
		
	}
	public function paypal_fail(){
		print_r( $this->input->get() );		
	}

}
