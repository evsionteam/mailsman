<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Base_model class.
 * 
 * @extends CI_Model
 */

class Base_model extends CI_Model {

	public $prefix = "mails_";
	
	public $table = "";
	
	public function __construct() {
		
		parent::__construct();		
	}

	public function get( $where = NULL, $order_by = NULL, $limit = NULL, $like = NULL ) {
		
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}
		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $limit )  && ( is_array( $limit ) && count( $limit ) > 0 ) ){
			$this->db->limit( $limit['per_page'], $limit ['offset']  );
		}
		
		$this->db->select( '*,  UNIX_TIMESTAMP( `created_at`) as `created_on` ,UNIX_TIMESTAMP( `updated_at`) as `updated_on`' );
		
		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	

		$query = $this->db->get( $this->table );


		if( $query->num_rows() > 0 ){			
			return $query->result();
		}
		else {
			return NULL;
		}
	}

	public function get_count( $where = NULL, $order_by = NULL, $like = NULL ) {
		if( !is_null( $where ) ) {
			$this->db->where( $where );
		}

		if( !is_null( $order_by ) ) {
			$this->db->order_by( key( $order_by ),$order_by[  key( $order_by ) ] );
		}

		if( !is_null( $like ) && ( is_array( $like ) && count( $like) > 0 ) ){
			$this->db->like( key( $like ), $like [ key( $like ) ] ,'both');
		}	

		$this->db->select( 'count( * ) as total_rows' );
		
		$query = $this->db->get( $this->table );

		if( $query->num_rows() > 0 ){			
			return $query->row('total_rows');			
		} else {
			return 0;
		}
	}
	
	
	public function insert( $insertArr ) {							
		$insertArr['updated_at'] = $insertArr['created_at'] =  date('Y-m-d H:i:s');	
		return $this->db->insert( $this->table , $insertArr );
	}

	public function update( $updateArr , $where ) {
		$insertArr['updated_at'] = date('Y-m-d H:i:s');	
		return  $this->db->update( $this->table, $updateArr ,$where );
	}

	public function delete( $where ) {
		return  $this->db->delete( $this->table, $where );
	}
	
}

/* End of file*/