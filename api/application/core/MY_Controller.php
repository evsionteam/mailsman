<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Attachments class.
 *
 * @extends CI_Controller
 */
class MY_Controller extends REST_Controller {

	// %s is the REST API key
    const MESSAGE_TOKEN_INVALID = 'Invalid Token key %s';

     // %s is the REST API key
    const MESSAGE_TOKEN_NOT_FOUND = 'API key not found!';
    const MESSAGE_TOKEN_INVALID_CREDENTIAL = 'Invalid credentials';
    const MESSAGE_TOKEN_UNAUTHORIZED = 'This API key does not have access to the requested controller';
    const MESSAGE_AJAX_ONLY = 'Only AJAX requests are allowed';
    const MESSAGE_TOKEN_PERMISSIONS = 'This API key does not have enough permissions';
    const MESSAGE_INVALID_REQUEST = "Invalid request!";
    const MESSAGE_INVALID_DATAFORMAT = "Invalid input formats!";
    const MESSAGE_INVALID_USER = "Invalid API token";

    public $http_status_check = NULL;

    protected $headers;
    public $is_valid_user = false;
    public $current_user = false;
    public $is_angular_request = false;
    public $upload_base_path;
    public $upload_base_url;

	public function __construct() {
		parent::__construct();

		/*for profile image*/
		$this->upload_base_path = BASEPATH.'../uploads/';

		if( !$this->load->is_loaded('url') ) {
			$this->load->helper('url');
		}

	  	$this->upload_base_url = site_url( 'uploads/');
		/*for profile image*/	  	

		foreach ($this->input->request_headers() as $key => $value) {
            $this->headers[trim($key)] = $value;
        }

		$this->validate_token();
	}

    /**
     * Takes mixed data and optionally a status code, then creates the response
     *
     * @access public
     * @param array|NULL $data Data to output to the user
     * @param int|NULL $http_code HTTP status code
     * @param bool $continue TRUE to flush the response to the client and continue
     * running the script; otherwise, exit
     */
    public function response($data = NULL, $http_code = NULL, $continue = FALSE) {
		ob_start();

		// if( isset( $data['status'] ) ) {
		// 	$http_code = $data['status'];
		// }

        // If the HTTP status is not NULL, then cast as an integer
        if ($http_code !== NULL) {
            // So as to be safe later on in the process
            $http_code = (int) $http_code;
        }

        // Set the output as NULL by default
        $output = NULL;

        // If data is NULL and no HTTP status code provided, then display, error and exit
        if ($data === NULL && $http_code === NULL) {
            $http_code = self::HTTP_NOT_FOUND;
        }

        // If data is not NULL and a HTTP status code provided, then continue
        elseif ($data !== NULL) {
            // If the format method exists, call and return the output in that format
            if (method_exists($this->format, 'to_' . $this->response->format)) {
                // Set the format header
                $this->output->set_content_type($this->_supported_formats[$this->response->format], strtolower($this->config->item('charset')));
                $output = $this->format->factory($data)->{'to_' . $this->response->format}();

                // An array must be parsed as a string, so as not to cause an array to string error
                // Json is the most appropriate form for such a datatype
                if ($this->response->format === 'array') {
                    $output = $this->format->factory($output)->{'to_json'}();
                }
            } else {
                // If an array or object, then parse as a json, so as to be a 'string'
                if (is_array($data) || is_object($data)) {
                    $data = $this->format->factory($data)->{'to_json'}();
                }

                // Format is not supported, so output the raw data as a string
                $output = $data;
            }
        }

        // If not greater than zero, then set the HTTP status code as 200 by default
        // Though perhaps 500 should be set instead, for the developer not passing a
        // correct HTTP status code
        $http_code > 0 || $http_code = self::HTTP_OK;

        $this->output->set_status_header($http_code);
        $this->http_status_check = $http_code;

        // JC: Log response code only if rest logging enabled
        if ($this->config->item('rest_enable_logging') === TRUE) {
            $this->_log_response_code($http_code);
        }

        // Output the data
        $this->output->set_output($output);

        if ($continue === FALSE) {
            // Display the data and exit execution
            $this->output->_display();
            exit;
        } else {
			ob_end_flush();
		}

        // Otherwise dump the output automatically
    }

	protected function validate_token(){
	    $this->load->library('JWT');
	    $secret_key = $this->config->item('web_token_secret');
	    $token_key = $this->config->item('REST_TOKEN_KEY');
	    $token_key = $token_key ? $token_key : "x-access-token";

	    // excluding pagee
	    if( (strstr(uri_string(), 'recover' ) != FALSE) ||
	    	(strstr(uri_string(), 'login' ) != FALSE) ||
	    	( strstr(uri_string(), 'user' ) != FALSE && strtolower($this->input->server('REQUEST_METHOD')) == "post" ) ||
	    	( strstr(uri_string(), 'options' ) != FALSE && strtolower($this->input->server('REQUEST_METHOD')) == "get" )  ||
	    	( strstr(uri_string(), 'download' ) != FALSE ) ||
	    	( strstr(uri_string(), 'forgot' ) != FALSE ) ||
	    	( strstr(uri_string(), 'user_verification' ) != FALSE ) ||	    	
	    	( strstr(uri_string(), 'stripe' ) != FALSE ) ||	    	
	    	( strstr(uri_string(), 'paypal' ) != FALSE ) ){

	    	    return;
	    } else {
	    		$this->load->model( 'users/user_model', 'user_model' );
	            if( in_array( $token_key, array_keys($this->headers) ) || in_array( 'X-Access-Token', array_keys($this->headers) )) {
	                
	                if(! isset($this->headers[$token_key])){
		                $token = $this->headers['X-Access-Token'];
	                }else{
		                $token = $this->headers[$token_key];
	                }

	                if ( $token && $response = $this->jwt->decode( $token, $secret_key, $verify = true ) ) {
	                    $this->is_valid_user = true;	                  
                    	$this->current_user = $response;

	                } else {
	                	$data['status'] = REST_Controller::HTTP_UNAUTHORIZED;
	                	$data['code'] = 'USR031';
	                    $data['message'] = SELF::MESSAGE_TOKEN_INVALID_CREDENTIAL;
	                    return $this->response( $data );
	                }

	            }else{
            		$data['status'] = REST_Controller::HTTP_UNAUTHORIZED;
            		$data['code'] = 'USR031';
            	    $data['message'] = SELF::MESSAGE_TOKEN_NOT_FOUND;
	                return $this->response( $data );
	            }

	            $this->is_valid_user = true;
	    }
	}

	protected function generate_token( $user_email = NULL, $user_id = NULL , $role = NULL){
	    $this->load->library('JWT');
	    $this->load->model('user_model');

	    if( !is_null( $user_id ) && !is_null( $user_email ) ) {
	        $secret_key = $this->config->item('web_token_secret');
	        $ttl        = 86400;
	        $token      = $this->jwt->encode( array(
	                                          'email'       => $user_email,
	                                          'userId'      => $user_id,
	                                          'role'        => $role,
	                                          'issuedAt'    => date(DATE_ISO8601, strtotime("now")),
	                                          'ttl'         => $ttl
	                                        ),
	                                    $secret_key);
	        return $token;
	    } else{
	        return NULL;
	    }
	}

	protected function validation_exclude() {
	    return array(
	                'login',
	                'user',
	                'forgot'
	                );
	}

	protected function delete_image( $path ){
		if( @unlink( $path) ){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	protected function get_url_http( $append = NULL ){
			$is_https = false;
			if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {		
				$is_https = true;
			}
			$url = 'http';
			$url .= ( ($is_https === true) ?'s':'') . '://' . $_SERVER['HTTP_HOST'];
			if(  $_SERVER['HTTP_HOST'] == "localhost" ) {
			  	$url .= '/mailsman/public';
			}
		  $url .= '/#/';
		  if( !is_null( $append ) ){
		  	$url .= $append;
		  }
		  return $url;
	}

	protected function get_site_info( $key ){
		$this->load->model( 'options/option_model', 'option_model' );
		$where = array(
                    'option_name' => $key
                );
	    $this->db->where( $where );
	    $option = $this->option_model->get() ;	   
	    if( count( $option ) > 0 ){
	        return $option[0]->option_value;
	    }
	    return NULL;
	}

	protected function get_email_footer_section() {
		$site_title = $this->get_site_info('title');
		return '&copy; ' . date('Y') . $site_title . '. All Rights reserved. ';
	}

	protected function _send_email( $template, $sendTo = NULL , $subject = 'Password Recovery' ) {

		$this->load->model('options/option_model','option_model');
		$this->load->library( 'email' );

		$where = array(
				'option_name' => "title",
			);
		$this->db->where_in( 'option_name', array( "title","email" ) );
	   	$response = $this->option_model->get( ) ;

		$opt = array(
			'title' => 'Mailsman',
			'email' => 'noreply@mailsman.com'
			);
		if( count( $response ) > 0 ){

			foreach( $response as $res ) {
				$opt[$res->option_name] = $res->option_value;
			}

			$this->email->from( $opt['email'], $opt['title'] );
		}   else {
			$this->email->from( $opt['email'], $opt['title'] );
		}
	    $this->email->set_newline("\r\n");
		$this->email->to( $sendTo );
		$this->email->subject( $subject );
		$this->email->message( $template );		
	    return $this->email->send();
	}

	protected function activity_moniter(){		
		$this->load->model('activity/activity_model','activity_model');
		if( 
			(strstr(uri_string(), 'recover' ) != FALSE) ||
		 	(strstr(uri_string(), 'login' ) != FALSE) ||
		 	( strstr(uri_string(), 'user' ) != FALSE && strtolower($this->input->server('REQUEST_METHOD')) == "post" ) ||
		 	( strstr(uri_string(), 'options' ) != FALSE && strtolower($this->input->server('REQUEST_METHOD')) == "get" )  ||
		 	/*( strstr(uri_string(), 'download' ) != FALSE ) ||*/
		 	( strstr(uri_string(), 'forgot' ) != FALSE ) ||
	    	( strstr(uri_string(), 'user_verification' ) != FALSE ) ||
	    	( strstr(uri_string(), 'stripe' ) != FALSE ) ||

	    	( strstr(uri_string(), 'paypal' ) != FALSE ) ||
 			( strstr(uri_string(), 'message' ) != FALSE && strtolower($this->input->server('REQUEST_METHOD')) == "get" )
	    ){

	        return;
	    }

		$uri_segment= $this->uri->rsegment_array();
		$method = $this->input->method();


		if( !$this->current_user ){ return; }
		$user_id = $this->current_user->userId;

		$insertArray =  array(
				'user_id'    => $user_id,
				'modules'    => $uri_segment[1],
				'method'     => $method,
				'status'     => ($this->http_status_check)?$this->http_status_check:404
		);

		/*file_put_contents( APPPATH . '../logs/activity.txt',print_r( $uri_segment, true ) );
		file_put_contents( APPPATH . '../logs/post.txt',print_r( $_POST, true ) );*/

		if(  strcmp( 'staff', $this->current_user->role ) == 0 ){
			switch ( $method ) {
				case 'post':
					if( strcmp( 'message', $uri_segment[1] ) == 0  ) {
						if( count($_FILES) <= 0  ){
							$insertArray['modules'] = 'compose';
							$insertArray['activity'] = serialize( $_POST );

						} else if ( count($_FILES) > 0 ) {
							$insertArray['modules'] = 'compose-attachment';
							$insertArray['activity'] = serialize( $_POST );
						}
						/**
						 * since the customer won't get receiving activities so insert the activity manually
						 */
						$customerArr = $insertArray;
						$customerArr['user_id'] = $_POST['customer_id'];
						$this->activity_model->insert( $customerArr );
					}
					
				break;

				case 'put':				
					if( strcmp('users' , $uri_segment[1] ) == 0 ) {
						$meta = $this->input->post('meta');
						if( !empty($meta) &&  array_key_exists('avatar', $meta ) ) {

							$insertArray['activity'] = " %s updated profile image";
						} else if( array_key_exists('new_password', $_POST ) ){
							$insertArray['activity'] = " %s changed password";

						} else {
							/*$insertArray['activity'] = '%s updated %s profile';*/
						}
					} 
				break;
				
			}
		} else if(  strcmp( 'admin', $this->current_user->role) == 0 ) {
			
			switch ( $method ) {
				case 'post': 
				  	if ( strcmp( 'users', $uri_segment[1] ) ==0 ) {						
				  		$insertArray['activity'] = '%s created a new staff';
					}
				break;
				case 'put':
					if( strcmp('users' , $uri_segment[1] ) == 0 ) {
						$meta = $this->input->post('meta');
						if( !empty($meta) &&  array_key_exists('avatar', $meta ) ) {
							$insertArray['activity'] = " %s updated profile image";
						} else if( array_key_exists('new_password', $_POST ) ){
							$insertArray['activity'] = " %s changed password";
						} else {
							/*$insertArray['activity'] = '%s updated %s profile';*/
						}
					} 
				break;
			}	
		} else if( strcmp( 'customer' , $this->current_user->role ) == 0){
						
			switch ( $method ) {

				case 'post':
					if( strcmp('message', $uri_segment[1]) == 0 ){
						/*$insertArray['activity'] = 'A reply to new message has been sent to staff user.';*/
						$insertArray['activity'] = serialize( $_POST );

					} else if(  strcmp('payment', $uri_segment[1] ) == 0 && strcmp('balance_load', $uri_segment[2] ) == 0 ) {
						$insertArray['modules'] = $uri_segment[2];
						$insertArray['activity'] = '%s loaded balance of %s '. $_POST['load_amount']. ' via '.$_POST['payment_mode'];

					} else if(  strcmp('payment', $uri_segment[1] ) == 0 && strcmp('balance_deduct', $uri_segment[2] ) == 0 ) {
						$insertArray['modules'] = $uri_segment[2];						
						$insertArray['activity'] = '%s paid balance  of %s '. $_POST['load_amount']. ' for downloading attachment' ;

					} else if( strcmp( 'users', $uri_segment[1] ) == 0 && strcmp( 'subscribe', $uri_segment[2] ) == 0 ) {
						$insertArray['modules'] = $uri_segment[2];						
						$insertArray['activity'] = '%s are subscribed';
					}
				break;
				
				case 'get':

					if( strcmp('payment', $uri_segment[1] ) == 0 && strcmp('download', $uri_segment[2] ) == 0 ) {
						$insertArray['modules'] = "download";
						$insertArray['activity'] = "%s downloaded attachment|".$uri_segment[4];
						
					}

					break;
				case 'put':

					if( strcmp('users' , $uri_segment[1] ) == 0 ) {
						$meta = $this->input->post('meta');
						if( !empty($meta) &&  array_key_exists('avatar', $meta ) ) {
							$insertArray['activity'] = " %s updated profile image";
						} else if( array_key_exists('new_password', $_POST ) ){
							$insertArray['activity'] = " %s changed password";
						} else {
							/*$insertArray['activity'] = '%s updated %s profile';*/
						}
					} 
					break;
			}
		}

		if( isset($insertArray['activity'] )  && $insertArray['activity'] != '' ){
			$this->activity_model->insert( $insertArray );
		}

		/*if( $this->current_user ){

			

				switch ( $method ) {
					case 'get':
						//$insertArray['activity'] = sprintf( 'Fetched %s',$uri_segment[1] );
						break;
					case 'put':
						$insertArray['activity'] = sprintf( 'Updated %s',$uri_segment[1] );
						break;
					case 'post':
						$insertArray['activity'] = sprintf( 'Inserted %s',$uri_segment[1] );
						break;
					case 'delete':
						$insertArray['activity'] = sprintf( 'Deleted %s',$uri_segment[1] );
						break;
				}

				if( $method != 'get' ){
					$this->load->model('activity/activity_model','activity_model');
					$this->activity_model->insert( $insertArray );
				}
		}*/
	}

	public function uploader(){
		/**
		 * More info
		 * setting preferences for upload will get from upload.php in APPPATH/config folder
		 * https://www.codeigniter.com/user_guide/libraries/file_uploading.html
		*/

		$config_upload = $this->load->config('upload', TRUE );

		if( !is_dir( $config_upload['upload_path'] ) ) {
			@mkdir( $config_upload['upload_path'], 0777 );
			@chmod( $config_upload['upload_path'], 0777 );
		}

		$this->load->library( 'upload' );
		if ( $this->upload->do_upload( 'file' ) ) {
			return $this->upload->data();
		} else {
			return $this->upload->display_errors();
		}
	}

	protected function get_meta_by_id( $userId ){
		$this->load->model('users/user_model','user_model');
        $usermeta = $this->user_model->_meta_row( $userId );
        $meta = [];
        if( $usermeta ){
            foreach( $usermeta as $row ){
                $meta[ $row->key ] = $row->value;
            }
        }

        if( ! isset( $meta[ 'avatar' ] ) ){
            $meta[ 'avatar' ] =  $this->upload_base_url . 'default.png';
        } else {
            $meta[ 'avatar' ] =  $this->upload_base_url .  $meta[ 'avatar' ];
        }

        return $meta;
    }

    public function get_balance( $user_id = NULL ){

    	$this->load->model('users/user_model','user_model');
    	$this->load->model('payment/payment_model','payment_model');

    	$current_user = $this->current_user;
		$get_user = $this->user_model->get( array( 'id' => $user_id ));

		if( count( $get_user ) > 0 ) {
			$this->db->group_by('id');
			$get_transaction = $this->payment_model->get( array( 'user_id' => $user_id ),array( 'id' => 'DESC' ) );

			if( !is_null( $get_transaction ) ){
				$transaction = $get_transaction[0];
				return $transaction->balance;

			} else {
				return 0;
			}
		} else {
			return 0;
		}
    }

    public function human_readable_filesize($filename, $decimals = 2) {

    	$config_upload = $this->load->config('upload', TRUE );		

    	if ( !file_exists ( $config_upload['upload_path'] . $filename )  ) {    		
    		return '0B';
    	}
    	$bytes = filesize( $config_upload['upload_path'] . $filename );
	    $size = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
	    $factor = floor((strlen($bytes) - 1) / 3);
	    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}

	public function is_serialized( $data, $strict = true ) {
		// if it isn't a string, it isn't serialized.
		if ( ! is_string( $data ) ) {
			return false;
		}

		$data = trim( $data );
	 	if ( 'N;' == $data ) {
			return true;
		}

		if ( strlen( $data ) < 4 ) {
			return false;
		}

		if ( ':' !== $data[1] ) {
			return false;
		}

		if ( $strict ) {
			$lastc = substr( $data, -1 );
			if ( ';' !== $lastc && '}' !== $lastc ) {
				return false;
			}
		} else {
			$semicolon = strpos( $data, ';' );
			$brace     = strpos( $data, '}' );
			// Either ; or } must exist.
			if ( false === $semicolon && false === $brace )
				return false;
			// But neither must be in the first X characters.
			if ( false !== $semicolon && $semicolon < 3 )
				return false;
			if ( false !== $brace && $brace < 4 )
				return false;
		}
		$token = $data[0];
		
		switch ( $token ) {
			case 's' :
				if ( $strict ) {
					if ( '"' !== substr( $data, -2, 1 ) ) {
						return false;
					}
				} elseif ( false === strpos( $data, '"' ) ) {
					return false;
				}
				// or else fall through
			case 'a' :
			case 'O' :
				return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
			case 'b' :
			case 'i' :
			case 'd' :
				$end = $strict ? '$' : '';
				return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
		}
		return false;
	}



	public function __destruct() {
		parent::__destruct();
		$this->activity_moniter();
   }
}
