module.exports = function( grunt ){
	
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),	

		apidoc: {
			mailsman: {
		    	src: "./api/application/",
		    	dest: "./documentation/",
		    	template: 'apidocs',
		    	options: {
			    	debug: false,
			      	includeFilters: [ ".*\\.php$" ],
			      	excludeFilters: [ "node_modules/" ]
			    }
		  	}
		}

	});

	// grunt.loadNpmTasks( 'grunt-contrib-watch' );	
	grunt.loadNpmTasks('grunt-apidoc');   

    grunt.registerTask('default', ['apidoc'] );
}