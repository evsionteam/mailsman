module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compass: { // Task
            dist: { // Target
                options: { // Target options
                    sassDir: 'src/app/sass/',
                    cssDir: 'build/assets/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            }
        },

        concat: {

            options: {
                separator: "\n \n /*** New File ***/ "
            },

            vendor: {
                src: [
                    './src/assets/css/vendor/font-awesome.css',
                    './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
                    './node_modules/sweetalert/dist/sweetalert.css',
                    './src/assets/css/vendor/bootstrap.css'
                ],
                dest: './build/assets/css/vendor.css'
            },

            style: {
                src: [
                    './src/assets/css/vendor/font-awesome.css',
                    './build/assets/css/vendor.css',
                    './build/assets/css/main.css'
                ],
                dest: './build/assets/css/style.css'
            },

            lib: {
                src: [
                    './node_modules/jquery/dist/jquery.js',
                    './node_modules/underscore/underscore.js',
                    './node_modules/sweetalert/dist/sweetalert-dev.js',
                    './node_modules/jquery.nicescroll/jquery.nicescroll.js',
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap/modal.js',
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap/tooltip.js',
                    './src/assets/js/custom/classToggler.js',
                    './src/assets/js/main.js',

                    // Angular dependency
                    './node_modules/angular/angular.js',
                    './node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
                    './src/assets/js/vender/ng-img-crop.js',
                    './node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
                    './node_modules/ng-sweet-alert/ng-sweet-alert.js',
                    './node_modules/ng-file-upload/dist/ng-file-upload.js',
                    './node_modules/angular-resource/angular-resource.js',
                    './node_modules/angular-route/angular-route.js',
                    './node_modules/angular-resource/angular-resource.js',
                    './node_modules/angular-cookies/angular-cookies.js',
                    './node_modules/angular-utils-pagination/dirPagination.js',
                    './node_modules/angular-file-saver/dist/angular-file-saver.bundle.js',
                    './node_modules/angular-sanitize/angular-sanitize.js',
                    './src/assets/js/custom/angular-helper.js',
                ],
                dest: './build/assets/js/lib.js'
            },

            js: {
                src: [
                    // Concating mailsman app
                    './src/assets/js/custom/wrapper/start.js',
                    './src/app/min-safe/*.js',
                    './src/assets/js/custom/wrapper/end.js'
                ],
                dest: './build/assets/js/app.js'
            }

        },

        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './build/assets/css/',
                    src: ['style.css'],
                    dest: './build/assets/css/',
                    ext: '.min.css'
                }]
            }
        },

        uglify: {
            options: {
                report: 'gzip'
            },
            main: {
                src: ['./build/assets/js/lib.js', './build/assets/js/app.js'],
                dest: './build/assets/js/app.min.js'
            }
        },
        jshint: {
            ignore_warning: {
                options: {
                    '-W015': true,
                },
                src: [
                    './src/app/*.js',
                    './src/app/**/**/*.js',
                    '!./src/app/min-safe/*.js'
                ]
            },
        },
        copy: {
            img: {
                files: [{
                        expand: true,
                        flatten: true,
                        src: './src/app/**/*.html',
                        dest: './build/templates/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: './src/assets/fonts',
                        src: '**',
                        dest: './build/assets/fonts/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: './src/assets/img',
                        src: '**',
                        dest: './build/assets/img/',
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        cwd: './src/assets/svg',
                        src: '**',
                        dest: './build/assets/svg/',
                        filter: 'isFile'
                    }
                ]
            }
        },

        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            app: {
                files: {
                    './src/app/min-safe/app.js': ['./src/app/app.js', './src/app/**/*.js', './src/app/**/**/*.js', '!./src/app/min-safe/*.js'],
                }
            }
        },

        watch: {

            sass: {
                files: [
                    './src/app/sass/*.scss',
                    './src/app/sass/**/*.scss',
                    './src/app/sass/**/**/*.scss',
                    './src/app/components/**/*.scss',
                    './src/app/shared/directives/**/*.scss'
                ],
                tasks: ['css']
            },

            js: {
                files: [
                    './src/assets/js/*.js',
                    './src/assets/js/**/*.js',
                    './src/app/*.js',
                    './src/app/**/**/*.js',
                    '!./src/app/min-safe/*.js'
                ],
                tasks: ['js']
            },

            tpl: {
                files: [
                    './src/app/components/*.html',
                    './src/app/components/**/*.html',
                    './src/app/shared/directives/**/*.html'
                ],
                tasks: ['copy']
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    /* register grunt default task */
    grunt.registerTask('css', ['compass']);
    grunt.registerTask('js', ['jshint', 'ngAnnotate', 'concat:js']);
    grunt.registerTask('tpl', ['copy']);

    grunt.registerTask( 'default', ['compass', 'cssmin', 'jshint', 'ngAnnotate', 'concat', 'uglify', 'copy' ]);
}
