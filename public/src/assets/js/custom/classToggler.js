
// notification 
 +(function ( $ ) {  

    this.ClassToggler = function(param) {  

        this.animation  = param.animation,
        this.toggler    = param.toggler,
        this.className  = param.className,
        this.exceptions = param.exceptions;

        this.init = function () {
            var that = this;
            // for stop propagation 
            var stopToggler = this.implode(this.exceptions);
            if (typeof stopToggler !== 'undefined') {  
                $(document).on('click', stopToggler, function (e) {
                    e.stopPropagation();
                });
            }

            // for toggle class  
            var toggler = this.implode(this.toggler);
            if (typeof toggler !== 'undefined') {

                $(document).on('click touchstart', toggler, function (e) {
                    e.stopPropagation();
                    e.preventDefault();
                    console.log(toggler);
                    that.toggle();
                });
            }
        }

        //class toggler
        this.toggle = function () {
            var selectors = this.implode(this.animation);
            if (typeof selectors !== 'undefined') {
                $(selectors).toggleClass(this.className);
            }
        }

        // array selector maker
        this.implode = function (arr, imploder) {

            // checking arg is array or not
            if (!(arr instanceof Array)) {
                return arr;
            }
            // setting default imploder
            if (typeof imploder == 'undefined') {
                imploder = ',';
            }

            // making selector
            var data = arr;
            var ele = '';
            for (var j = 0; j < arr.length; j++) {
                ele += arr[j];
                if (j !== arr.length - 1) {
                    ele += imploder;
                }
            }
            data = ele;
            return data;
        }

    } //classToggler

})(jQuery);
