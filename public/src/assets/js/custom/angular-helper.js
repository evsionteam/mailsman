+(function() {
    'user strict';

    angular
        .module('ngHelper', [])
        .run(['$rootScope', '$sce', '$location', function($rootScope, $sce, $location) {

            // eg. <input type="text" ng-keypress = "onlyNum($event)" ng-model="num" />
            $rootScope.onlyNum = function($event) {

                var exceptions = [ 'Delete', 'Backspace', 'ArrowLeft', 'ArrowRight' ];

                if( exceptions.includes( $event.key ) ){
                    return;
                }

                if ( $event.charCode !== 46 && isNaN(String.fromCharCode($event.charCode))) {
                    $event.preventDefault();
                }
            }

            // eg. <div ng-bind-html="renderHtml( markups )"></div>
            $rootScope.renderHtml = function(html_code) {
                return $sce.trustAsHtml(html_code);
            }

            // eg. <div ng-click="redirect( 'dashboard' )"></div>
            $rootScope.redirect = function(slug) {
                $location.path(slug);
            }

            $rootScope.getDateObject = function(d) {
                return new Date(d.replace(/(.+) (.+)/, "$1T$2Z"));
            }

        }])
        .filter('capitalize', ['helper', function(helper) {

            return function(input) {

                return helper.capitalize(input);
            }
        }])
        .filter('numToMonth', ['helper', function(helper) {
            return function(input) {

                var input = parseInt(input);
                return helper.numToMonth(input);
            }
        }])
        .filter('ucwords', ['helper', function(helper) {

            return function(input) {

                return helper.ucwords(input);
            }
        }])
        .factory('helper', ['$filter', '$location', '$log', function($filter, $location, $log) {

            var helper = {}

            helper.isExist = function(input) {

                if (angular.isUndefined(input) || typeof input == 'undefined' || input == null || input == false) return false;

                return true;
            }

            helper.capitalize = function(input) {

                if (typeof input == 'string') {

                    return (angular.isString(input) && input.length > 0) ? input[0].toUpperCase() + input.substr(1).toLowerCase() : input;
                }

                $log.error("Not a string");
                return input;
            }

            helper.ucwords = function(input) {

                if (typeof input == 'string') {

                    var words = input.split(' ');

                    for (var i = 0; i < words.length; i++) {
                        words[i] = helper.capitalize(words[i]);
                    }

                    return words.join(' ');
                }

                console.error("Not a string");
                return input;
            }

            //eg.
            //	var array = [
            //					{"ID":3,"name":"yujesh"},
            // 					{"ID":4,"name":"ashok"}
            //				]
            //  helper.search( array, { "ID" : 4 } ) returns array[1]

            helper.search = function(searchIn, searchBy) {
                return ($filter('filter')(searchIn, searchBy))[0];
            }

            //eg.
            //	if is current url is http://localhost/app/#/login
            //	helper.getUriSegment(1) will return 'login'
            helper.getUriSegment = function(segment) {

                var data = $location.path().replace(/^\/|\/$/g, '').split('/');

                if (data[segment - 1]) {
                    return data[segment - 1];
                }

                return false;
            }

            helper.numToMonth = function(m) {

                var m = parseInt(m);

                if (isNaN(m) || m <= 0 || m >= 13) {
                    $log.error('Invalid input : ', m);
                    return;
                }
                var month = new Array();
                month[0] = "Jan";
                month[1] = "Feb";
                month[2] = "Mar";
                month[3] = "Apr";
                month[4] = "May";
                month[5] = "June";
                month[6] = "July";
                month[7] = "Aug";
                month[8] = "Sept";
                month[9] = "Oct";
                month[10] = "Nov";
                month[11] = "Dec";

                return month[m - 1];
            }

            helper.get12HourFormat = function(date) {

                var hours24 = date.getHours(); // retrieve current hours (in 24 mode)

                var dayMode = hours24 < 12 ? "AM" : "PM"; // if it's less than 12 then "am"

                var hours12 = hours24 <= 12 ? (hours24 == 0 ? 12 : hours24) : hours24 - 12;

                return hours12 + ':' + date.getMinutes() + '  ' + dayMode;
            }

            helper.hasClass = function(el, className) {
                if (el.classList)
                    return el.classList.contains(className)
                else
                    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
            }

            helper.addClass = function(el, className) {
                if (el.classList)
                    el.classList.add(className)
                else if (!helper.hasClass(el, className)) el.className += " " + className
            }

            helper.removeClass = function(el, className) {
                if (el.classList)
                    el.classList.remove(className)
                else if (helper.hasClass(el, className)) {
                    var reg = new RegExp('(\\s|^)' + className + '(\\s|$)')
                    el.className = el.className.replace(reg, ' ')
                }
            }

            helper.pop = function(arr, index) {

                if (arr.length > 0) {

                    if (!helper.isExist(index) || isNaN(index)) {
                        return arr.pop();
                    } else {
                        return arr.splice(index, 1);
                    }
                }
            }

            helper.transpose = function(a) {

                // Calculate the width and height of the Array
                var
                    w = a.length ? a.length : 0,
                    h = a[0] instanceof Array ? a[0].length : 0;

                // In case it is a zero matrix, no transpose routine needed.
                if (h === 0 || w === 0) {
                    return [];
                }

                /**
                 * @var {Number} i Counter
                 * @var {Number} j Counter
                 * @var {Array} t Transposed data is stored in this array.
                 */
                var i, j, t = [];

                // Loop through every item in the outer array (height)
                for (i = 0; i < h; i++) {

                    // Insert a new row (array)
                    t[i] = [];

                    // Loop through every item per item in outer array (width)
                    for (j = 0; j < w; j++) {

                        // Save transposed data.
                        t[i][j] = a[j][i];
                    }
                }

                return t;
            }

            return helper;
        }])
        .directive('toggleClass', ['helper', function(helper) {
            return {
                restrict: 'A',
                link: function(scope, el, attrs) {

                    var state = true;
                    el.bind('click', function() {

                        if (state) {
                            helper.removeClass(el, attrs.toggleClass);
                            state = !state;
                        } else {
                            helper.removeClass(el, attrs.toggleClass);
                            state = !state;
                        }

                    });
                }
            }
        }])
        .factory('Base64', function() {     /* jshint ignore:start */       
            var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';      
            return {        
                encode: function(input) {            
                    var output = "";            
                    var chr1, chr2, chr3 = "";            
                    var enc1, enc2, enc3, enc4 = "";            
                    var i = 0;              
                    do {                
                        chr1 = input.charCodeAt(i++);                
                        chr2 = input.charCodeAt(i++);                
                        chr3 = input.charCodeAt(i++);                  
                        enc1 = chr1 >> 2;                
                        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);                
                        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);                
                        enc4 = chr3 & 63;                  
                        if (isNaN(chr2)) {                    
                            enc3 = enc4 = 64;                
                        } else if (isNaN(chr3)) {                    
                            enc4 = 64;                
                        }                  
                        output = output +                     keyStr.charAt(enc1) +                     keyStr.charAt(enc2) +                     keyStr.charAt(enc3) +                     keyStr.charAt(enc4);                
                        chr1 = chr2 = chr3 = "";                
                        enc1 = enc2 = enc3 = enc4 = "";            
                    } while (i < input.length);              
                    return output;        
                },
                          decode: function(input) {            
                    var output = "";            
                    var chr1, chr2, chr3 = "";            
                    var enc1, enc2, enc3, enc4 = "";            
                    var i = 0;               // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
                                
                    var base64test = /[^A-Za-z0-9\+\/\=]/g;            
                    if (base64test.exec(input)) {                
                        window.alert("There were invalid base64 characters in the input text.\n" +                     "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +                     "Expect errors in decoding.");            
                    }            
                    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");              
                    do {                
                        enc1 = keyStr.indexOf(input.charAt(i++));                
                        enc2 = keyStr.indexOf(input.charAt(i++));                
                        enc3 = keyStr.indexOf(input.charAt(i++));                
                        enc4 = keyStr.indexOf(input.charAt(i++));                  
                        chr1 = (enc1 << 2) | (enc2 >> 4);                
                        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);                
                        chr3 = ((enc3 & 3) << 6) | enc4;                  
                        output = output + String.fromCharCode(chr1);                  
                        if (enc3 != 64) {                    
                            output = output + String.fromCharCode(chr2);                
                        }                
                        if (enc4 != 64) {                    
                            output = output + String.fromCharCode(chr3);                
                        }                  
                        chr1 = chr2 = chr3 = "";                
                        enc1 = enc2 = enc3 = enc4 = "";              
                    } while (i < input.length);              
                    return output;        
                }    
            };       /* jshint ignore:end */
        });
})()
