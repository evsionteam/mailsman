$(document).ready( function(){
        var currentDateTime,
            totalTime,
            passedTime,
            percentage,
            startDateTime = new Date(2017, 04, 30, 16, 00, 00, 00).getTime();
            endDateTime   = new Date(2017, 05, 01, 17, 25, 00, 00).getTime();

            function getPercentage (){
                currentDateTime = new Date().getTime();
                totalTime = endDateTime - startDateTime;
                passedTime = currentDateTime - startDateTime;
                percentage = (passedTime / totalTime) * 100;
                $('.line').css('width', percentage+'%');
                if (percentage >= 100 ){
                    clearInterval( progressStatus );
                    $('.line').css('width', '100%');
                }
            }

            var progressStatus = setInterval( getPercentage, 200 );


});
