app.directive( 'mmHeader', function( $rootScope, Const, Utils, NotificationSrvc, $timeout, helper ){

	return {
		restrict: 'E',
		replace: true,
		templateUrl: Const.templatePath + '/header.tpl.html',
		link: function( scope, ele, attrs ){

			$('[data-toggle="tooltip"]').tooltip('destroy');
			$('[data-toggle="tooltip"]').tooltip();

			scope.userInfo = $rootScope.user;
			scope.txt = $rootScope.txt;

			scope.logout = function(){
				Utils.logout();
			};

			scope.routes = {
				mails : '#' + Const.routes.mails
			};

			scope.notification = NotificationSrvc.getMeta();

			if( 'undefined' == typeof unSeenMailEventBinder ){

				unSeenMailEventBinder = true;

				$rootScope.$on( 'newMail', function(){

					scope.activeBell = true;

					$timeout(function(){
						scope.activeBell = false;
					},2000);
				});
			}

		}
	};

});
