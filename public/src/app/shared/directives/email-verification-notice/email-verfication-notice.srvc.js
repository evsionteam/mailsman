app.service( 'emailVerificationNoticeSrvc', function( Const, Utils, Resource, $rootScope ){
    var meta = {
        isSending : false,
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.resentVerification = function(){
        var onGet = function( response ){
                var type = 'error';
                if( 200 === response.status ){
                    type = 'success';
                }
                meta.toast({
                    message : $rootScope.txt[response.code],
                    type    : type,
                    time    : 3000,
                    clear   : true,
                });
                meta.isSending = false;
            },
            onError = function( reason ){
                meta.isSending = false;
            };

        meta.isSending = true;
        meta.toast({
            message : $rootScope.txt.TXT0166,
            type    : 'loader',
            time    : 9000000
        });
        Resource.user().get( {id: 'user_resent_verification'}, onGet, onError );
    };

});
