app.directive( 'emailVerificationNotice', function( Const, Utils, emailVerificationNoticeSrvc, $rootScope ){

    return{
        restrict    : 'E',
        replace     : false,
        scope       :{ },
        templateUrl : Const.templatePath + '/email-verification-notice.tpl.html',
        link        : function( scope, ele, attrs ){
                        scope.meta = emailVerificationNoticeSrvc.getMeta();
                        
                        scope.resentVerification =  function(){
                            emailVerificationNoticeSrvc.resentVerification();
                        };
                    }
    };

});
