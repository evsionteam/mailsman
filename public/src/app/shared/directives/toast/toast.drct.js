app.directive( 'toast', function( Const, $timeout, helper ) {

    var link = function( scope, ele, attrs ) {

        var defaults = {
            message   : 'Please set Message',
            show      : false,
            type      : false,
            time      : 5000,
            clear     : false
        };

        scope.data = angular.copy( defaults );
        scope.toast = function( param ){
            scope.data = angular.extend( defaults, param );

            if( scope.data.clear && angular.isDefined(scope.timer) ){
               $timeout.cancel(scope.timer);
            }

            if( 'undefined' == typeof param.show ){

                scope.data.show = true;
            }

            scope.timer = $timeout( function(){ scope.data = false; }, scope.data.time );
        };
    };

    return {
        restrict    : 'A',
        replace     : true,
        scope       : { toast: '=' },
        templateUrl : Const.templatePath + '/toast.tpl.html',
        link        : link
    };

});
