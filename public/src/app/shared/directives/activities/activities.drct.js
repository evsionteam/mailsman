app.directive('activities', function(Const, $timeout, helper, $rootScope, ActivitiesSrvc, TransactionsSrvc) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            options: '=config'
        },
        templateUrl: Const.templatePath + '/activities-drct.tpl.html',
        link: function(scope, ele, attrs) {

            scope.txt = $rootScope.txt;
            scope.userInfo = $rootScope.user;
            scope.meta = ActivitiesSrvc.getMeta();
            scope.meta.selectedUser = null;
            scope.meta.userId = null;
            scope.meta.currentPage = 1;
            scope.meta.perPage = Const.perPage;

            if (helper.isExist(scope.options.limit)) {
                ActivitiesSrvc.get(scope.options.limit);
            } else {
                scope.get = function(id, page) {
                    ActivitiesSrvc.setMeta('id', id);
                    ActivitiesSrvc.setMeta('page', page);
                    ActivitiesSrvc.get();
                };
                scope.get(0, 1);
            }
            scope.users = TransactionsSrvc.getMeta();

            /* search */
            scope.ischanged = false;
            scope.search = function(){
                scope.meta.isSearching = true;
                scope.ischanged = true;
                TransactionsSrvc.searchUser( scope.userEmail );
            };

            scope.isFocus = false;
            scope.selected = function(){
                scope.ischanged = true;
            };

            scope.disselected = function(){
                scope.ischanged = false;
            };

            scope.clear = function(){
                scope.users.searchedUser = [];
                scope.meta.userEmail = null;
            };

            scope.setEmail = function( id, email ){
                scope.meta.selectedUser = email;
                scope.userEmail = email;
                scope.meta.userId = id;
                ActivitiesSrvc.get();
                scope.meta.currentPage = 1;
                scope.clear();
            };

            scope.getAllUsers = function(){
              if( scope.meta.selectedUser ){
                scope.meta.selectedUser = null;
                scope.meta.userId = null;
                scope.get(0, 1);
              }
              scope.userEmail = null;
            };

        } //link

    }; //return

});
