app.directive( 'tableLoader', function( Const, $rootScope, Utils ){

	return {
		restrict    : 'E',
		replace     : true,
		scope      : {
			when  : '=',
			empty : '='
		},
		templateUrl : Const.templatePath + '/table-loader.tpl.html',
		link        : function( scope, ele, attrs ){

			scope.txt = $rootScope.txt;
			
		}
	};

});
