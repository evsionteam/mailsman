app.directive('sideNav', function( $rootScope, Const, Utils, helper, UserSrvc, MailSrvc, $location, NotificationSrvc ) {

var linkFn = function(scope, ele, attrs) {

    scope.siteName = $rootScope.optMeta.title;

    var segment = helper.getUriSegment(1),
        cookie  = Utils.getCookie(),
        menu    = [];

    scope.accoutName = $rootScope.txt.TXT0016;
    scope.selected   = '#/' + ((segment) ? segment : '');

    scope.meta = UserSrvc.getMeta();
    scope.role = $rootScope.user.role;
    scope.txt = $rootScope.txt;

    /* event trigger when setting is changed */
    scope.$on("optionChanged", function (evt, data) {
        scope.siteName = $rootScope.optMeta.title;
    });

    /* profileUrl */
    scope.profileUrl = '#' + Const.routes.profile;

    var commonMenu = [
        {
            id: 2,
            name: $rootScope.txt.TXT0013,
            icon: 'fa fa-user-circle',
            slug: '#' + Const.routes.profile
        },
        {
            id: 100,
            name: $rootScope.txt.TXT0015,
            icon: 'fa fa-thumb-tack',
            slug: '#' + Const.routes.activities
        }
    ],
    adminMenu = [
        {
            id   : 2,
            name : $rootScope.txt.TXT0051,
            icon : 'fa fa-users',
            slug : '#' + Const.routes.customers
        },
        {
            id   : 3,
            name : $rootScope.txt.TXT0084,
            icon : 'fa fa-user',
            slug : '#' + Const.routes.createStaff
        },
        {
            id   : 4,
            name : $rootScope.txt.TXT0085,
            icon : 'fa fa-book',
            slug : '#' + Const.routes.createMmaddress
        },
        {
            id   : 6,
            name : $rootScope.txt.TXT0125,
            icon : 'fa fa-list-alt',
            slug : '#' + Const.routes.service
        },
        {
            id   : 7,
            name : $rootScope.txt.TXT0044,
            icon : 'fa fa-cog',
            slug : '#' + Const.routes.setting
        },
        {
            id: 99,
            name: $rootScope.txt.TXT0182,
            icon: 'fa fa-money',
            slug: '#' + Const.routes.transactions
        }
    ],
    staffMenu = [
        {
            id   : 2,
            name : $rootScope.txt.TXT0051,
            icon : 'fa fa-book',
            slug : '#' + Const.routes.customers
        },
        {
            id: 3,
            name: $rootScope.txt.TXT0014,
            icon: 'fa fa-envelope-o',
            slug: '#' + Const.routes.mails
        }
    ],
    customerMenu = [
        {
            id: 1,
            name: $rootScope.txt.TXT0012,
            icon: 'fa fa-dashboard',
            slug: '#' + Const.routes.dashboard
        },
        {
            id: 3,
            name: $rootScope.txt.TXT0014,
            icon: 'fa fa-envelope-o',
            slug: '#' + Const.routes.mails
        },
        {
            id: 99,
            name: $rootScope.txt.TXT0182,
            icon: 'fa fa-money',
            slug: '#' + Const.routes.transactions
        }
    ];

    if( 'administrator' == cookie.role ){
        scope.menu = commonMenu.concat( adminMenu );
    }else if( 'staff' == cookie.role ){
        scope.menu = commonMenu.concat( staffMenu );
    }else{
        scope.menu = commonMenu.concat( customerMenu );
    }

    // social links
    scope.socialLinks = [
        {
            class: 'facebook',
            icon: 'fa fa-facebook',
            slug: 'www.facebook.com',
        },
        {
            class: 'twitter',
            icon: 'fa fa-twitter',
            slug: 'www.twitter.com',
        },
        {
            class: 'pinterest',
            icon: 'fa fa-pinterest-p',
            slug: 'www.pinterest.com'
        }
    ];

    scope.notification = NotificationSrvc.getMeta();
    // accout options
    scope.accoutOptions = [
        {
            name   : $rootScope.txt.TXT0017,
            status : 'all',
            notificationKey : 'total'
        },
        {
            name   : $rootScope.txt.TXT0018,
            status : 'unread',
            notificationKey : 'unread'
        },
        {
            name   : $rootScope.txt.TXT0019,
            status : 'pending',
            notificationKey : 'pending'
        },
        {
            name   : $rootScope.txt.TXT0183,
            status : 'unpaid',
            notificationKey : 'unpaid'
        },
        {
            name   : $rootScope.txt.TXT0020,
            status : 'completed',
            notificationKey : 'completed'
        }
    ];

    scope.goToMail = function( status ){
        MailSrvc.setMeta( 'status', status );
        MailSrvc.setMeta( 'isForecasting', false );
        MailSrvc.get();
        if( Const.routes.mails != '/' + segment ){
            $location.path( Const.routes.mails );
        }
    };
};
    return {
        restrict    : 'E',
        replace     : true,
        scope       : {},
        templateUrl : Const.templatePath + '/side-nav.tpl.html',
        link        : linkFn
      }; //return
});
