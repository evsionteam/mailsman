app.directive('mail', function( $rootScope, $http, Const, Utils, MailSrvc, NotificationSrvc, helper, $uibModal, $timeout, UserSrvc ) {

    return {

        restrict: 'E',
        replace: true,
        scope: {
            data: '=mailConfig'
        },
        templateUrl: Const.templatePath + '/mail.tpl.html',
        link: function(scope, ele, attrs) {

            var segment = helper.getUriSegment(1);

            var defaultCols = {
                'mail': true,
                'date': true,
                'status': true,
                'actions': true,
            };

            if ('undefined' == typeof scope.data.pagination) {
                scope.data.pagination = true;
            }

            // hide col options
            scope.col = angular.extend(defaultCols, scope.data.cols);
            scope.meta = MailSrvc.getMeta();

            scope.txt = $rootScope.txt;
            scope.user = $rootScope.user;
            scope.onlyNum = $rootScope.onlyNum;

            scope.getIndex = function(id, arr) {
                return Utils.findIndex('id', id, arr);
            };

            //Reset Previous data.
            scope.meta.mails = [];
            scope.meta.searchKey = null;
            scope.get = function(id, status, page) {

                MailSrvc.setMeta('status', status);
                MailSrvc.setMeta('page', page);
                MailSrvc.setMeta('id', id);
                MailSrvc.setMeta('isForeCasting', false);
                MailSrvc.setMeta('mails', []);
                MailSrvc.setMeta('totalRows', 0);
                MailSrvc.setMeta('perPage', Const.perPage );

                MailSrvc.get();
            };

            MailSrvc.setMeta('perPage', Const.perPage );
            MailSrvc.setMeta('isGetting', true );
            NotificationSrvc.get( true );

            scope.clear = function() {

                if (!helper.isExist(scope.meta.searchKey)) {

                    if (scope.meta.searchForm.$submitted) {
                        scope.get( 0, scope.meta.status, 1 );
                    }

                    scope.meta.searchForm.$setPristine();
                }
            };

            var mails = scope.meta.mails;

            if (scope.meta.services.length === 0) {
                MailSrvc.getServices();
            }

            scope.delete = function(id) {
                MailSrvc.delete(id);
            };

            scope.view = function(id) {};

            // Service Request
            var data = {},
                mailId = null,
                currentMail = null,
                serviceId = null,
                serviceObjKey = null;
            scope.service = {};

            scope.request = function( address ) {
                serviceObjKey = Object.keys(scope.service);
                mailId = serviceObjKey[0];
                serviceId = scope.service[mailId];
                currentMail = scope.meta.mails[Utils.findIndex('id', mailId, scope.meta.mails)];

                if( !helper.isExist(serviceId) ){
                    scope.meta.toast({
                        message : $rootScope.txt.ERR014,
                        type: 'error',
                        clear: true,
                        time: 2000
                    });
                    return;
                }

                var data = {
                    mail_id          : mailId,
                    group_id         : currentMail.group_id,
                    staff_id         : currentMail.sender_id,
                    customer_id      : currentMail.receiver_id,
                    message          : currentMail.message,
                    special_message  : serviceObjKey.length > 1 ? scope.service.sp['info' + mailId] : '',
                    status           : 'pending',
                    service_id       : serviceId,
                };

                /* sending user address when scan and deliver service selected */
                if( helper.isExist( address ) ){
                    data.userAddress = address;
                }

                MailSrvc.request(data);
                scope.service = {};
            };

            scope.downloadFile = function( mail, status, id ) {
                /* only show download info if message is unpaid or complete */
                if( status === 'unread' || status === 'pending'){
                    return;
                }
                var ctrl = function( $scope, $rootScope, $uibModalInstance, MailSrvc, UserSrvc, Resource, Utils, $window, $location, Const, meta ) {

                    $scope.cancel = function() {
                        $uibModalInstance.close('cancel');
                    };
                    console.log( id, 'test id');

                    $scope.mail = mail;
                    $scope.meta = MailSrvc.getMeta();

                    $scope.download = MailSrvc.download;

                    $scope.sweet = {};
                    $scope.sweet.option = {
                        title: $rootScope.txt.TXT0134,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $rootScope.txt.TXT0137,
                        cancelButtonText: $rootScope.txt.TXT0138,
                        closeOnConfirm: true,
                        closeOnCancel: true
                    };

                    $scope.payment = function() {
                        $scope.cancel();
                        meta.toast( { message:"Payment processing...", type: 'loader', time: 60000 } );

                        var onSuccess = function( response ){

                            var type = "error";
                            //201 for payment and 200 is already paid
                            if( 201 == response.status || 200 == response.status  ){

                                var i = Utils.findIndex('id', id, $scope.meta.mails);
                                meta.mails[ i ].status = 'completed';
                                if( $scope.meta.status != 'all' ){
                                    $scope.meta.mails.splice(i, 1);
                                } 

                                var cookie = Utils.getCookie();
                                cookie.balance = response.data.balance;
                                $rootScope.user.balance = response.data.balance;
                                Utils.setCookie( cookie );
                                mail.is_paid = '1';
                                type = "success";
                                $scope.download( response.data.attachment_id );
                            }

                           meta.toast({
                                type    : type,
                                message : $rootScope.txt[ response.code ],
                                clear   : true,
                                time    : 3000
                            });

                            $scope.cancel();
                        },
                        onError = function( reason ){

                            $scope.cancel();

                            meta.toast({
                                type    : 'error',
                                message : $rootScope.txt[ reason.data.code ],
                                clear   : true
                            });

                            Utils.log( [ 'Error on payment.', reason ], 'error' );
                        },
                        param = {
                            attachment_id : mail.attachment_id
                        };

                        Resource.balance().deduct( param, onSuccess, onError );
                    };
                };

                var modalInstance = $uibModal.open({
                    animation   : true,
                    templateUrl : 'download-file.html',
                    controller  : ctrl,
                    size        : 'md',
                    resolve     : {
                        meta: scope.meta
                    }
                });

                //Get mail Threads
                MailSrvc.getThread( mail.group_id );
            };

            scope.sendEstimation = MailSrvc.sendEstimation;

            scope.upload = MailSrvc.upload;

            scope.makeSeen = MailSrvc.makeSeen;

            scope.$on("$destroy", function() {
                if (helper.isExist(scope.meta.timer)) {
                    $timeout.cancel(scope.meta.timer);
                }
                Utils.log('Destroyed', 'info');
            });

            scope.$on("SendHomeAddress", function ( events, data ) {
                scope.request( data );
                // UserSrvc.update( data );
            });

            /* check selected element is scan and delivery */
            scope.isScanAndDeliver = function( data ){
                if( helper.isExist( data.has_delivery ) ){
                    scope.disabledSend = true;
                }else{
                    scope.disabledSend = false;
                }
            };

            scope.setHomeAddress = function(){

                var modalInstance = $uibModal.open({
                    animation     : true,
                    templateUrl   : Const.templatePath + '/home-address.tpl.html',
                    controller    : function( $scope, $uibModalInstance, MailSrvc, $rootScope, UserSrvc ){
                        $scope.userMeta = UserSrvc.getMeta();

                        /* fetching user information */
                        UserSrvc.getMeta();
                        $scope.sendRequest = function(){
                            $scope.userMeta.userInfo.meta.isHomeAddress = 1;
                            $rootScope.$broadcast( "SendHomeAddress", $scope.userMeta.userInfo.meta );
                            $uibModalInstance.close('cancel');
                        };

                        $scope.cancel = function(){
                            $uibModalInstance.close('cancel');
                        };


                    },
                    size : 'md'
                });
            };

            scope.$on("SendOtherAddress", function ( events, data ) {
                scope.request( data );
            });

            scope.setOtherAddress = function(){

                var modalInstance = $uibModal.open({
                    animation     : true,
                    templateUrl   : Const.templatePath + '/other-address.tpl.html',
                    controller    : function( $scope, $uibModalInstance, MailSrvc, $rootScope, UserSrvc ){
                        $scope.userOtherAddress = {};
                        $scope.sendRequest = function(){
                            $scope.userOtherAddress.isHomeAddress = 0;
                            $rootScope.$broadcast( "SendOtherAddress", $scope.userOtherAddress );
                            $uibModalInstance.close('cancel');
                            $scope.userOtherAddress = {};
                        };

                        $scope.cancel = function(){
                            $uibModalInstance.close('cancel');
                        };

                    },
                    size : 'md'
                });
            };

        }
    };

});
