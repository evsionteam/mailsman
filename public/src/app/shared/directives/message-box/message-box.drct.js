app.directive('message', function( Const, $timeout, helper) {

    return {
        restrict: 'E',
        replace: true,
        scope: {
            meta: '=status'
        },
        templateUrl: Const.templatePath + '/message-box.tpl.html',
        link: function(scope, ele, attrs) {
            ele.ready(function() {
                scope.meta.status = {
                    message : 'message is showing',
                    state   : false,
                    error   : false,
                    success : false,
                    time    : 5000,
                    init: function(param) {
                        if (helper.isExist(param)) {
                            if (helper.isExist(param.message)) {
                                scope.meta.status.message = param.message;
                            }
                            if (helper.isExist(param.success)) {
                                scope.meta.status.success = param.success;
                                scope.meta.status.error = false;
                            }
                            if (helper.isExist(param.error)) {
                                scope.meta.status.error = param.error;
                                scope.meta.status.success = false;
                            }
                        }
                        scope.meta.status.state = true;
                        $timeout(function() {
                            scope.meta.status.state = false;
                        }, scope.meta.time || 5000);
                    }
                };

            }); //element ready
        } //link
    }; //return

});
