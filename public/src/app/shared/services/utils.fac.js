app.factory('Utils', function( $rootScope, $cookies, $location, $log, Const, helper, $timeout, Resource ) {

    var utils = {};

    utils.pop = function( id ) {
        return data.filter(function(el) {
            return el.ID != ID;
        });
    };

    utils.findIndex = function( key, value , arr ){
      var obj = {};
          obj[key] = value;
          return _.findIndex( arr, obj );   
    };

    utils.log = function(data, key) {

        if (!helper.isExist(data) || !Const.scriptDebug) {
            return false;
        }

        switch (key) {
            case 'error':
                $log.error(data);
                break;

            case 'info':
                $log.info(data);
                break;

            case 'warning':
                $log.warn(data);
                break;

            default:
                $log.log(data);
                break;
        }
    };

    utils.getYears = function() {
        var date = new Date();
        var data = [];
        for (var i = 0; i < 20; i++) {
            data.push(date.getFullYear() + i);
        }

        return data;
    };

    utils.removeCookie = function() {
        $cookies.remove(Const.cookieVar);
    };

    utils.setCookie = function(data) {
        utils.removeCookie();
        $cookies.putObject(Const.cookieVar, JSON.stringify(data));
    };

    utils.logout = function() {
        $rootScope.$emit('logout');
        utils.removeCookie();
        Resource.setToken( '' );
        $location.path(Const.routes.login);

        utils.log('Logged out successfully.', 'info');
    };

    utils.getCookie = function() {
        var cookie = $cookies.getObject(Const.cookieVar);
        if (helper.isExist(cookie))
            return JSON.parse(cookie);
        else {
            utils.log('Cookie not found.', 'info');
        }
    };

    utils.getUsersRoute = function(){
        var r          = Const.routes,
            common     = [ r.login, r.recover, r.register, r.emailVerify, r.activities, r.profile, r.transactions ],
            rolesRoute = {
                'administrator' : [ r.setting, r.customers, r.createStaff, r.createMmaddress, r.service  ].concat( common ),
                'customer'      : [ r.dashboard, r.mails, r.payment ].concat( common ),
                'staff'         : [ r.customers, r.mails ].concat( common )
            };
        return rolesRoute;
    };

    utils.getKeyByValue = function( object, value ) {

        for( var key in object ){
            if( object[key] == value ){
                return key;
            }
        }

        return false;
    };

    return utils;
});
