app.service( 'MailSrvc', function( Utils, Const, Resource, MessageSrvc, Upload, $rootScope, $timeout, helper, $route, FileSaver, Blob ) {

    var defaults = {
                isGetting           : true,
                isGettingThread     : true,
                isDeleting          : false,
                isComposing         : false,
                isSendingEstimation : false,
                isSendingAttachment : false,
                isForecasting       : false,
                page                : 1,
                perPage             : Const.perPage,
                compose             : {},
                totalRows           : 0,
                status              : 'all',
                timer               : null,
                id                  : 0,
                mails               : [],
                thread              : [],
                services            : [],
                attachments         : {},
                searchKey           : null,
                tablePath           : Const.templatePath + '/mail-table.tpl.html',
            },
        meta  = {},
        _this = this;

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function( key, value ) {
        meta[key] = value;
    };

    this.get = function( forecasted ) {

        if( !helper.isExist( forecasted ) ){
            forecasted = false;
        }

        var onGet = function(response) {
            if (200 == response.status) {

                if( forecasted ){
                    if( response.data.length > 0 ){

                        angular.forEach( response.data, function( value, key ){

                            var index = Utils.findIndex( 'group_id', value.group_id, meta.mails );

                            if( index >= 0 && meta.mails[ index ].status != value.status ){

                                meta.mails[ index ].status        = value.status;
                                meta.mails[ index ].id            = value.id;
                                meta.mails[ index ].is_seen       = value.is_seen;
                                meta.mails[ index ].attachment_id = value.attachment_id;
                                meta.mails[ index ].service_id    = value.service_id;
                                meta.mails[ index ].charge        = value.charge;
                                meta.mails[ index ].service_cost  = value.service_cost;
                                meta.mails[ index ].is_paid       = value.is_paid;

                            }else{
                                //Prevent dublicate mail being pushed
                                var i = Utils.findIndex( 'id', value.id, meta.mails );

                                if( i == -1 ){

                                    meta.mails.push( value );
                                }
                            }
                        });
                    }else{
                        meta.mails = response.data;
                    }
                }else{

                    meta.mails     = response.data;
                    meta.totalRows = response.total_rows;
                }
            }

            meta.isGetting = false;
            meta.isFetchingMail  = false;

        },
        onGetError = function(reason) {
            meta.isGetting = false;
            meta.isFetchingMail  = false;

            Utils.log( [ 'Cannot get mails', reason ], 'error' );
        };

        if ( !meta.isFetchingMail ) {
            meta.isGetting = true;
            meta.isFetchingMail  = true;
            if( Const.routes.mails === $route.current.$$route.originalPath ){
              Resource.mail().get({
                id     : meta.id,
                status : meta.status,
                page   : meta.page,
                search : meta.searchKey,
              }, onGet, onGetError);
            }else{
                meta.perPage   = Const.totalRecentMails;
                meta.totalRows = Const.totalRecentMails;
                Resource.getRecentMail( Const.totalRecentMails ).then( onGet, onGetError );
            }
        }
    };

    this.getThread = function( group_id ){

        var onGetThread = function( response ){

            if( 200 == response.status ){

                meta.thread = response.data;

                var index_at_pending = 1,
                    //Push Service details into meta.thread
                    i = Utils.findIndex( 'id', meta.thread[ index_at_pending ].service_id, meta.services );

                meta.thread[ index_at_pending ].service = meta.services[ i ];

                //Pop service_id because it in service key ( This is not important )
                delete meta.thread[ index_at_pending ].service_id;

            }

            meta.isGettingThread = false;
        },
        onGetThreadError = function( reason ){

            Utils.log( [ 'Error on getting thread.', reason ], 'error' );
            meta.isGettingThread = false;
        };

        meta.isGettingThread = true;

        Resource.mail().getThread( { 'status' : group_id }, onGetThread, onGetThreadError );
    };

    this.delete = function(id) {

        var onDelete = function(response) {

                if (200 == response.status) {
                    var i = Utils.findIndex('id', id, meta.mails);
                    meta.mails.splice(i, 1);
                } else {

                }

                meta.isDeleting = false;

            },
            onDeleteError = function(reason) {
                Utils.log(['Error on Deleting Mail', reason], 'error');
                meta.isDeleting = false;
            };

        meta.isDeleting = true;
        Resource.mail().delete( { id: id }, onDelete, onDeleteError );
    };

    this.getServices = function() {

        var onGet = function(response) {

                if (200 == response.status) {

                    meta.services = response.data;
                }

                meta.isGettingService = false;

            },
            onGetError = function(reason) {
                Utils.log(['Error on Getting Services', reason], 'error');
                meta.isGettingService = false;
            };

        meta.isGettingService = true;
        Resource.service().query(onGet, onGetError);
    };

    this.request = function( data ){

        var onSave = function( response ){

                var type = 'error';

                if( 200 == response.data.status ){
                    var i = Utils.findIndex( 'id', data.mail_id, meta.mails );
                    meta.mails[ i ].status = 'pending';

                    if( meta.status != 'all' ){
                        meta.mails.splice( i, 1 );
                    }

                    type = 'success';
                }

                meta.toast( { message: MessageSrvc.get( response.data.code ), type: type } );
                meta.isComposing = false;
            },
            onSaveError = function( reason ){
                Utils.log( [ 'Error on service request', reason ], 'error' );
                meta.isComposing = false;
            };

        meta.isComposing = true;

        Resource.sendMail( data ).then( onSave, onSaveError );
    };

    this.compose = function( data ) {

        var onSave = function(response) {

                var type = 'error';

                if (200 == response.status) {
                    type = 'success';
                }
                meta.toast({
                    message : $rootScope.txt[response.data.code],
                    type    : type,
                    time    : 3000
                });

                meta.isComposing = false;
            },
            onSaveError = function(reason) {
                Utils.log( [ 'Error on Composing mail.', reason ], 'error' );
                meta.isComposing = false;
                meta.toast({
                    message : $rootScope.txt[reason.data.code],
                    type    : 'error',
                    time    : 3000
                });
            };

        meta.isComposing = true;

        Resource.sendMail( data ).then( onSave, onSaveError );

     };

    this.upload = function( file, errFiles, mailId ){

        meta.attachments[ mailId ]      = {};
        meta.attachments[ mailId ].file = file;
    };

    this.sendEstimation = function( mailId ){
        meta.isSendingEstimation = true;
        if( ( $rootScope.user.role == 'staff' ||  $rootScope.user.role == 'administrator' ) && meta.attachments[ mailId ] && meta.attachments[ mailId ].file ) {

            var mIndex = Utils.findIndex( 'id', mailId, meta.mails ),
                m      = meta.mails[ mIndex ],
                params = {
                           file        : meta.attachments[ mailId ].file,
                           charge      : m.service_cost,
                           staff_id    : $rootScope.user.id,
                           customer_id : m.receiver_id,
                           group_id    : m.group_id,
                           message     : m.message,
                           service_id  : m.service_id,
                           status      : 'unpaid'
                },
                onUpload = function( response ){

                    meta.isSendingEstimation = false;
                    var type = 'error';
                    if( 200 == response.data.status ){

                        m.status = 'unpaid';
                        type = 'success';

                        if(meta.status != 'all'){
                          meta.mails.splice( mIndex, 1 );
                        }
                    }

                    meta.toast( { message: $rootScope.txt[ response.data.code ], type: type } );
                },
                onUploadError = function( reason ){
                    meta.isSendingEstimation = false;
                   meta.toast( { message: $rootScope.txt.TXT0062, type: 'error' } );
                   Utils.log( [ 'Error on file upload', reason ], 'error' );
                },
                progress = function( e ){
                    meta.attachments[ mailId ].progress = Math.min( 100, parseInt( 100.0 * e.loaded / e.total ) );
                };

                Resource.sendMail( params ).then( onUpload, onUploadError, progress );
        }else{
            meta.toast( { message: $rootScope.txt.TXT0061, type: 'error' } );
            meta.isSendingEstimation = false;
        }
    };

    this.makeSeen = function( row ){

        var request = function(){
                row.is_seen = "1";

                $rootScope.$emit( 'mailSeen' );

                Resource.makeSeen().update( { 'id' : row.id } );
            };

        if( "0" == row.is_seen ){

            if( 'customer' ==  $rootScope.user.role ){

                if( 'unread' == row.status || 'completed' == row.status || 'unpaid' == row.status ){
                    request();
                }
            }else if( 'staff' ==  $rootScope.user.role ){

                if( 'pending' == row.status || 'unpaid' == row.status){
                    request();
                }
            }
        }

        return true;
    };

    this.download = function( attachment_id ){

        var onSuccess = function( response ){

                var data = new Blob( [ response.data ] ),
                    FileName = response.headers('Content-Disposition');

                if( FileName ){
                    var result = FileName.split(';')[1].trim().split('=')[1];

                    FileSaver.saveAs( data, result.replace(/"/g, '') );
                }   

                meta.isDownloading = false;

            },
            onError = function( reason ){
                Utils.log( [ 'Error on file download', reason ], 'error' );
                meta.isDownloading = false;
            };

        meta.isDownloading = true;
        Resource.download( attachment_id ).then( onSuccess, onError );
    };
});
