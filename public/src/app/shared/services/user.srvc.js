app.service('UserSrvc', function( $rootScope, Const, MessageSrvc, Resource, Utils, helper, $timeout ) {

    var that = this,
        defaults = {
            isGetting          : false,
            isGettingUsers     : false,
            isPasswordUpdating : false,
            currentAction      : 'add',
            isUpdating         : false,
            isprofileUpdating  : false,
            userInfo           : null,
            password           : { new: null, old: null },
            users              : [],
            isSaving           : false,
            page               : 1,
            perPage            : Const.perPage,
            totalRows          : null,
            savingRequestNo    : 0,
            unVerifiedUser     : null,
            searchKey          : null
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {

        if (!helper.isExist( meta.userInfo )) {
            this.get();
        }else if( meta.userInfo.mm_address === null || parseInt( meta.userInfo.is_subscribe ) === 0 ){

            this.get();
        }
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    //update user password
    this.updatePassword = function(id, data){
        var onUpdate = function( response ){
                var type = 'error';
                if( 200 === response.status ){
                    meta.password.new = null;
                    meta.password.old = null;
                    meta.changePasswordForm.$setPristine();
                    type = 'success';
                    $timeout(function(){
                        Utils.logout();
                    }, 2000);
                }
                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: type,
                    time: 2000,
                    clear: true
                });
                meta.isPasswordUpdating = false;
            },
            onError = function( reason ){
                meta.toast({
                    message: $rootScope.txt[reason.code],
                    type: 'error',
                    time: 2000,
                    clear: true
                });
                meta.isPasswordUpdating = false;
                Utils.log( [ 'Error on update password', reason ], 'error' );
            };
        meta.isPasswordUpdating = true;
        Resource.user().update( {id:id}, data, onUpdate, onError );
    };

    // delete after assign mmaddress to user
    this.getUnVerifiedUsers = function() {
        var onGet = function( response ) {
              if( '200' === response.status){
                meta.unVerifiedUser = response.data;
              }

              meta.isGettingUnVerifiedUsers = false;
            },
            oGetError = function( reason ) {
                Utils.log(['Error On Getting All unverified users', reason]);
                meta.isGettingUnVerifiedUsers = false;
            };

        meta.isGettingUnVerifiedUsers = true;
        Resource.user().unVerifiedCustomer( onGet, oGetError );
    };

    this.get = function() {
        var cookie = Utils.getCookie(),
        succeededResponse = function(response) {
            if (200 == response.status) {
                that.setMeta('userInfo', response.data);
                that.setMeta('oldUserInfo', angular.copy(response.data));

                $rootScope.user.is_subscriber = response.data.is_subscribe;
                $rootScope.user.balance = response.data.balance;

                cookie.is_subscriber = response.data.is_subscribe;
                cookie.balance = response.data.balance;
                Utils.setCookie( cookie );

            }
            meta.isGetting = false;
        },
        failedResponse = function(reason) {
            meta.isGetting = false;
            Utils.log( [ 'fail to load users', reason ], 'error' );
        };

        if (!meta.isGetting) {
            meta.isGetting = true;
            Resource.user().get({
                id: cookie.userId
            }, succeededResponse, failedResponse);
        }

    };

    this.setUser = function( response ){

        if (200 == response.status) {
            meta.users = response.data;
            meta.totalRows = response.total_rows;
        }else{

            meta.users     = [];
            meta.totalRows = 0;
        }

        meta.isGettingUsers = false;
    };

    this.setUserError = function( reason ){
        Utils.log(['Error On Getting All Users', reason]);
        meta.users     = [];
        meta.totalRows = 0;
        meta.isGettingUsers = false;
    };

    this.getCustomers = function() {

        meta.isGettingUsers = true;

        return Resource.user().allCustomer({page : meta.page, search : meta.searchKey}, this.setUser, this.setUserError ).$promise;
    };

    this.getStaffs = function(){

        meta.isGettingUsers = true;

        return Resource.user().allStaff({page : meta.page, search : meta.searchKey}, this.setUser, this.setUserError );
    };

    /* update function too ganeric so i make this */
    this.staffUpdate = function( data ){
        var onSuccess = function( response ){
                var type = 'error';
                if( 200 === response.status ){
                    type = 'success';
                    meta.users[Utils.findIndex('id', meta.staffId, meta.users)] = response.data;
                }
                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: type,
                    time: 2000,
                    clear: true
                });
                meta.currentAction = 'add';
            },
            onError = function( response ){
                Utils.log(['Error On Updating User', reason]);
                meta.toast({
                    message: $rootScope.txt[reason.code],
                    type: 'error',
                    time: 2000,
                    clear: true
                });
                meta.currentAction = 'add';
            };
        Resource.user().update({id: meta.staffId}, data, onSuccess, onError);
    };

    /* User Pofile Update */
    this.update = function(data) {

        var succeededResponse = function(response) {
                var type = 'error';
                if (200 == response.status) {
                    type = 'success';
                    if(helper.isExist(response.data.meta.avatar)) {
                        meta.userInfo.meta.avatar = response.data.meta.avatar;
                    }
                    angular.forEach(response.data.meta, function(value, key) {
                        meta.oldUserInfo.meta[key] = value;
                    });
                    meta.savingRequestNo = --meta.savingRequestNo;
                }

                // meta.isUpdating = false;
                if( helper.isExist(data.meta.avatar) ){
                  meta.isprofileUpdating = false;
                }else{
                  meta.isUpdating = false;
                }
                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: type,
                    time: 1000,
                    clear: true
                });
            },
            failedResponse = function(reason) {
                // meta.isUpdating = false;
                if( helper.isExist(data.meta.avatar) ){
                  meta.isprofileUpdating = false;
                }else{
                  meta.isUpdating = false;
                }
                Utils.log(['Error On Updating User', reason]);
                meta.toast({
                    message: $rootScope.txt[reason.code],
                    type: 'error',
                    time: 1000,
                    clear: true
                });
            };

          if( helper.isExist(data.meta.avatar) ){
            meta.isprofileUpdating = true;
          }else{
            meta.isUpdating = true;
          }
        Resource.user().update({
            id: meta.userInfo.id
        }, data, succeededResponse, failedResponse);
    };

    this.delete = function( userId ){
        meta.toast({
            type: 'loader',
            message: MessageSrvc.get('TXT0161'),
            time: 3600000,
            clear    : true
        });

        var onSuccess = function( response ){
                var type = 'error';
                if( 200 === response.status ){
                    meta.users.splice( Utils.findIndex('id', userId, meta.users), 1);
                    type = 'success';
                }
                meta.toast({
                    type: type,
                    message: MessageSrvc.get( response.code ),
                    time : 3000,
                    clear    : true
                });
            },
            onError = function( reason ){
                Utils.log( [ 'Error on update password', reason ], 'error' );
                meta.toast({
                    type: 'error',
                    message: MessageSrvc.get( reason.code ),
                    time : 3000,
                    clear    : true
                });
            };
        Resource.user().delete( {id: userId}, onSuccess, onError );
    };

    this.updateStatus = function( id, data ){
        var succeededResponse = function( response ){
        },
        failedResponse = function( reason ){
        };

        Resource.user().update({
            id : id
        }, data, succeededResponse, failedResponse);
    };

    this.create = function( data ){

        var onSuccess = function( response ){

            if( 201 == response.status ){

                meta.users.push( response.data );
                meta.toast({
                    type: 'success',
                    message: MessageSrvc.get(response.code),
                    clear    : true
                });

            }else{

                for( var i=0; i < response.data.length; i++ ){

                    if( 'email' === response.data[i].field){
                        meta.error.email = MessageSrvc.get( response.data[i].code );
                    }

                    if( 'reTypePassword' === response.data[i].field ){
                        meta.error.password = MessageSrvc.get( response.data[i].code );
                    }
                }
            }

            meta.isCreatingUser = false;
        };

        onError = function( reason ){
            Utils.log(['Error On Creating user.', reason]);
            meta.isCreatingUser = false;
        };

        meta.isCreatingUser = true;
        meta.error = {};

        return Resource.user().save( data, onSuccess, onError ).$promise;
    };

});
