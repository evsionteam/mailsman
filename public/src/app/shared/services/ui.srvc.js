app.service('UiSrvc', function( Const, MessageSrvc, Resource ) {

    this.init = function() {
        this.accoutStatusAnimation();
        this.sideNavAnimation();
        this.tableContentToggler();
        this.optionMenu();
        this.nicescroll();
        this.toogleElement();
        this.scrollTop();
        this.hideUserList();
    };

    this.hideUserList = function(){
      $(document).click( function(e){
        if(!$(e.target).is('#search-user')){
          $('.mm-search-user-list').hide();
        }
      });
      $(document).on( 'focus', '#search-user', function(){
        $('.mm-search-user-list').removeAttr('style');
      });
    };

    this.scrollTop = function(){
        $(document).on( 'click', '.scroll-top', function(){
            $('html, body').scrollTop( 0 );
        });
    };

    this.nicescroll = function(){
      $('body').niceScroll({
        cursorcolor: "#376cbf",
        cursorwidth: "7px",
        cursorborder: "none",
        cursorborderradius: "0",
        zindex: "99",
        background: "rgb(216, 216, 216)"
      });
    };

    this.toogleElement = function(){
        $(document).on('click', '.toggle-ele', function(){
            $(this).parent().toggleClass('open');
            if( $(this).attr('data-target') ){
                $($(this).attr('data-target')).slideToggle(  );
            }
        });
    };

    this.accoutStatusAnimation = function() {
        var downArrow = '#down-arrow';

        $(document).on('click', downArrow ,function(event) {
            event.preventDefault();
            $(this).toggleClass('open');
            $('#account-options').slideToggle();
        });
    };

    this.sideNavAnimation = function() {
        var menuConfig = {
            animation: ['#mm-main-content', '#mm-side-nav', '#mm-menu-icon'], //where class add element
            exceptions: ['.sc-board'], //stop propagation
            toggler: '#mm-menu-icon', //class toggle on click
            className: 'nav-close'
        };
        new ClassToggler(menuConfig).init();

    };

    this.tableContentToggler = function() {
        var tRow = '#mail-table tbody tr';
        $(document).on('click', tRow, function() {
            var dataTarget = $(this).attr('data-target');
            $(dataTarget)
                .slideToggle()
                .children()
                .slideToggle();
        });
    };

    this.optionMenu = function() {
        $optionMenu = $('.mm-options i');
        $optionMenu.on('click', function() {
            $(this).parent().toggleClass('active');
        });
    };

});
