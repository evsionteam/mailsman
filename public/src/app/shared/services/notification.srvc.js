app.service( 'NotificationSrvc', function( $rootScope, Utils, Resource, $timeout, Const, helper, MailSrvc ) {

    var defaults = {
                unseen     : 0,
                total      : 0,
                unread     : 0,
                pending    : 0,
                unpaid     : 0,
                completed  : 0,
                timer      : null,
                foreCasted : false
            },
        meta  = {},
        _this = this;

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function( key, value ) {
        meta[key] = value;
    };

    this.get = function( fetchMail ){

        var foreCast = function(){

            if( helper.isExist( meta.timer ) ){
                $timeout.cancel( meta.timer );
            }

            meta.timer = $timeout( function(){
                _this.get();
            },Const.foreCastTime);
        },
        onSuccess = function( response ){

            if( 200 == response.status ){

                meta.total     = response.data.total;
                meta.unread    = response.data.unread;
                meta.pending   = response.data.pending;
                meta.unpaid    = response.data.unpaid;
                meta.completed = response.data.completed;

                if( fetchMail ){
                    MailSrvc.get();
                }else if( meta.unseen < parseInt( response.data.unseen ) ){
                    MailSrvc.get( true );
                    $rootScope.$emit( 'newMail' );
                }

                meta.unseen = parseInt( response.data.unseen );
            }

            foreCast();
        },
        onError = function( reason ){
            Utils.log( [ 'Error on getting Unseen Count', reason ], 'error' );
            foreCast();
        };

        meta.foreCasted = true;
        Resource.unseenCount().query( onSuccess, onError );
    };

});
