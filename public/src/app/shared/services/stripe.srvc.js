app.service( 'StripeSrvc', function( $rootScope, helper, Const, Resource, Utils, $location, Base64 ){

	var defaults = {
            card : {
            	name      : null,
            	number    : null,
            	cvc       : null,
            	exp_month : null,
            	exp_year  : null
            },
            token          : null,
            isSubscribing  : false,
			isLoadFund     : false,
			isLoadingFund  : false,
			userToken      : null,
			loadAmount     : 0,
			sendEmail      : false,
	    },
	    meta  = {},
	    _this = this;

	this.initMeta = function() {

	    angular.forEach(defaults, function(value, key) {
	        meta[key] = value;
	    });
	};

	this.setPublishableKey = function( key ){

		if( 'undefined' == typeof Stripe ){
			console.error( 'Stripe.js not found.' );
			return;
		}

		Stripe.setPublishableKey( key );
	};

	this.initMeta();
	this.setPublishableKey( Const.publishableKey );

	this.getMeta = function() {
	    return meta;
	};

	this.setMeta = function( key, value ) {
	    meta[key] = value;
	};

	this.getExpYears = function(){

		var date = new Date(),
			year = date.getFullYear(),
		    years = [];

		for( i = 0; i<= 15; i++ ){
			years.push( year++ );
		}

		return years;
	};

	this.subscribe = function(response){

		meta.isLoadingFund = false;
		var onSuccess = function( response ){
				var type ='error';
				if( 200 == response.status ){
					type = 'success';
					Utils.setCookie({
                        token         : Base64.encode(response.data.token),
                        userId        : response.data.user_id,
                        role          : response.data.role,
                        is_subscriber : response.data.is_subscribe,
                        balance       : response.data.balance,
						is_verified   : response.data.is_verified,
                    });

					$location.path( Const.routes.dashboard );
				}

				meta.toast({
					type    : type,
					message : $rootScope.txt[ response.code ],
					clear   : true
				});
				meta.isSubscribing = false;
			},
			onError = function( reason ){
				Utils.log( [ 'Error on subscribe', reason ], 'error' );
				meta.toast({
					type    : 'error',
					message : 'Try Again Later.',
					clear   : true
				});

				meta.isSubscribing = false;
			};

		Resource.subscribe( meta.userToken ).save( {token: response.id,payment_mode : 'stripe', send_email : meta.sendEmail }, onSuccess, onError );

	};

	this.loadFund = function( card ){
		var onSuccess = function( response ){
			var type = 'error';
			if( 200 === response.status ){
				type = 'success';
				var cookie = Utils.getCookie();
				cookie.balance = response.data.balance;
				Utils.setCookie(cookie);
				$location.path(Const.routes.dashboard);
			}

			meta.isLoadingFund = false;
			meta.toast({
				type    : type,
				message : $rootScope.txt[ response.code ],
				clear   : true
			});

		},
		onError = function( reason ){
			meta.isLoadingFund = false;
			meta.toast({
				type    : 'error',
				message : $rootScope.txt[ reason.code ],
				clear   : true
			});
			Utils.log( [ 'Error on load fund', reason ], 'error' );
		};
		Resource.balance().load( {token: card.token, payment_mode : card.payment_mode, load_amount : meta.loadAmount }, onSuccess, onError );
	};

	this.responseHandler = function( status, response ) {

		if ( response.error ){ // Problem!
			//***** response.error.message *****//
			meta.toast({
				type    : 'error',
				message : response.error.message,
				clear   : true
			});

			meta.isSubscribing = false;

		} else { // Token was created!

			 if( meta.isLoadFund ) {
				  _this.loadFund( {token:response.id, payment_mode : 'stripe'} );
			 }else{
				 	_this.subscribe( response );
			 }

		}
	};

	this.getInvalidFields = function( data ){

		var param = angular.copy( data );

		param.exp_date  = [ param.exp_month, param.exp_year ];

		var keys           = Object.keys( param ),
			invalid_fields = [];

		for( var i in keys ){

			var key = keys[ i ];

			switch( key ){

				case 'name' :
					if( !helper.isExist( param[ key ] ) || '' === param[ key ] ){
						invalid_fields.push( key );
					}
				break;

				case 'number':

					if( ! Stripe.card.validateCardNumber( param[ key ] ) ){
						invalid_fields.push( key );
					}

				break;

				case 'cvc':

					if( ! Stripe.card.validateCVC( param[ key ] ) ){
						invalid_fields.push( key );
					}

				break;

				case 'exp_date':
					if( ! Stripe.card.validateExpiry( param[ key ][0], param[ key ][1] ) ){
						invalid_fields.push( key );
					}
				break;
			}
		}

		return invalid_fields;
	};

	this.createToken = function(){

		var param = {
				name      : meta.card.name,
				number    : meta.card.number,
				cvc       : meta.card.cvc,
				exp_month : meta.card.exp_month,
				exp_year  : meta.card.exp_year
			},
			invalid_fields = this.getInvalidFields( param );
		if( invalid_fields.length === 0 ){

			meta.isSubscribing = true;
			meta.isLoadingFund = true;
			Stripe.card.createToken( param, this.responseHandler );
		}

		return invalid_fields;
	};
});
