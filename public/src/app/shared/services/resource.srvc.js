app.service("Resource", function( $resource, $http, $rootScope, Const, Upload ) {

    this.headers = {
        'x-access-token': null,
        'Cache-Control': 'no-cache'
    };

    this.actions = {};

    this.setToken = function(newToken) {

        this.headers['x-access-token'] = newToken;

        this.actions = {
            query: {
                method: 'GET',
                headers: this.headers,
            },
            get: {
                method: 'GET',
                headers: this.headers,
            },
            save: {
                method: 'POST',
                headers: this.headers
            },
            update: {
                method: 'PUT',
                headers: this.headers,
            },
            delete: {
                method: 'DELETE',
                headers: this.headers,
            }
        };
    };

    this.getMessage = function() {
        return $resource(Const.apiPath + '/code');
    };

    this.user = function() {

        var act = {
                login: {
                    method: 'POST',
                    headers: this.headers,
                    params: {
                        id: 'login',
                        role: ''
                    }
                },
                forget: {
                    method: 'POST',
                    headers: this.headers,
                    params: {
                        id: 'forgot'
                    }
                },
                recover: {
                    method: 'POST',
                    headers: this.headers,
                    params: {
                        id: 'recover'
                    }
                },
                allCustomer: {
                    method: 'GET',
                    headers: this.headers,
                    params: {
                        id: 0,
                        role: 'customer'
                    }
                },
                allStaff:{
                    method: 'GET',
                    headers: this.headers,
                    params: {
                        id: 0,
                        role: 'staff'
                    }
                },
                // delete after assign mmaddress to user
                unVerifiedCustomer: {
                    method: 'GET',
                    headers: this.headers,
                    params: {
                        id: 0,
                        role: 'customer',
                        page: 'unverified'
                    }
                },
                userVerification : {
                    method   : 'PUT',
                    headers  : this.headers,
                    params   : {
                        id : 'user_verification'
                    }
                }
            },
            actions = angular.extend( act, this.actions );

        return $resource(Const.apiPath + '/user/:id/:role/:page/:search', {
            id     : '@id',
            role   : '@role',
            page   : '@page',
            search : '@search',
        }, actions);
    };

    this.mail = function() {

        var act = {
                get: {
                    method: 'GET',
                    headers: this.headers
                },
                getRecent : {
                  method: 'GET',
                  headers: this.headers,
                  params :{
                    id: 'recent'
                  }
                },
                getThread : {
                    method: 'GET',
                    headers : this.headers,
                    params  : {
                        id: 'message_thread'
                    }
                }
            }, //Declare your own methods here
            actions = angular.extend(act, this.actions);

        return $resource(Const.apiPath + '/message/:id/:status/:page/:search', {
            id     : '@id',
            status : '@status',
            page   : '@page',
            search : '@search'
        }, actions);
    };

    this.getRecentMail = function( how_many ){
      return this.mail().getRecent( { status: how_many } ).$promise;
    };

    this.makeSeen = function() {

        var act = {}, //Declare your own methods here
            actions = angular.extend(act, this.actions);
        return $resource(Const.apiPath + '/message_read/:id/', {
            id: '@id'
        }, actions);
    };

    this.unseenCount = function() {

        var act = {}, //Declare your own methods here
            actions = angular.extend(act, this.actions);
        return $resource(Const.apiPath + '/message_count/', {}, actions);
    };

    this.sendMail = function(data) {

        var param = {
            url     : Const.apiPath + '/message',
            headers : this.headers,
            data    : data
        };

        return Upload.upload(param);
    };

    this.attachment = function() {

        var act = {}, //Declare your own methods here
            actions = angular.extend(act, this.actions);

        return $resource(Const.apiPath + '/attachment/:id/', {
            id: '@id'
        }, actions);

    };

    this.service = function() {

        var act = {}, //Declare your own methods here
            actions = angular.extend(act, this.actions);

        return $resource(Const.apiPath + '/services/:id/', {
            id: '@id'
        }, actions);
    };

    this.options = function() {

        var act = {}, //Declare your own methods here
            actions = angular.extend(act, this.actions);

        return $resource(Const.apiPath + '/options/:id/', {
            id: '@id'
        }, actions);
    };

    this.activity = function() {

        var act = {
          getRecent  : {
              method : 'GET',
              headers : this.headers,
              params : {
                id: 'recent'
              }
          }
        }, //Declare your own methods here
            actions = angular.extend(act, this.actions);

        return $resource(Const.apiPath + '/activity/:id/:page/:userId', {
            id: '@id',
            user: '@userId'
        }, actions);
    };

    this.mmAddress = function(assign_to) {
        var act = {
                assign: {
                    method: 'PUT',
                    headers: this.headers,
                    params: {
                        id   : 'assign_user',
                        page : assign_to
                    }
                },
                // localhost/mailsman/api/mmaddress/0/1/0/1
                unusedMmaddress : {
                    method: 'GET',
                    headers: this.headers,
                    params : {
                        id       : 0,
                        page     : 1,
                        search   : 0,
                        unassign : 'unassign'
                    }
                }
            }, //Declare your own methods here
            actions = angular.extend(act, this.actions);
        return $resource(Const.apiPath + '/mmaddress/:id/:page/:search/:unassign', {
            id       : '@id',
            page     : '@page',
            search   : '@search',
            unassign : '@unassign'
        }, actions);
    };

    this.transactions = function(){
        var act ={},
            actions = angular.extend( act, this.actions );
            return $resource( Const.apiPath + '/transactions/:page/:userId/:search', {
                page   : '@page',
                userId : '@userId',
                search : '@search'
            }, actions );
    };

    this.subscribe = function( userToken ) {
            var act = {
                save: {
                    method: 'POST',
                    headers : {
                        'x-access-token': userToken,
                        'Cache-Control': 'no-cache'
                    }
                }
            }, //Declare your own methods here

            actions = angular.extend( this.actions, act );
        return $resource(Const.apiPath + '/subscription', {}, actions);
    };

    this.loadFund = function(){
        var act ={ },
            actions = angular.extend( this.actions, act );
            return $resource( Const.apiPath + '/balance/load', {}, actions );
    };

    this.balance = function(){

        var act = {
            load : {
                method  : 'POST',
                headers : this.headers,
                params  : {
                    slug : 'load'
                }
            },
            check : {
                method  : 'GET',
                headers : this.headers,
                params  : {
                    slug : 'check'
                }
            },
            deduct : {
                method  : 'POST',
                headers : this.headers,
                params  : {
                    slug : 'deduct'
                }
            }

        }, //Declare your own methods here
        actions = angular.extend( act, this.actions );

        return $resource(Const.apiPath + '/balance/:slug', {}, actions);
    };

    this.download = function( attachment_id ){

        var param = {
            url: Const.apiPath + '/download/' + $rootScope.user.id + '/'+ attachment_id,
            method  : 'GET',
            headers : this.headers,
            responseType : 'arraybuffer'
        };
        return $http( param );

    };

    this.paypal = function(){
        console.log('paypal resource');
        var act = {}; //Declare your own methods here

        actions = angular.extend( act, this.actions );
        return $resource( Const.apiPath +'/paypal/:status', {}, actions );
    };

});
