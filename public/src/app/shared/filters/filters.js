app.filter('mmCurrency', function ( OptionSrvc, $filter ) {
  return function ( input ) {

      var symbol =  OptionSrvc.getByKey('currency') || '$';
      return $filter( 'currency' )( input, symbol );

  };
});
