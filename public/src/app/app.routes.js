app.config(function($routeProvider, Const) {

    var setMessage = function($rootScope, $q, Resource, MessageSrvc, helper, Utils, OptionSrvc, $route, Const) {

        var optMeta = OptionSrvc.getMeta(),
            promises = [];
            $rootScope.optMeta = optMeta.options;

        p = false;
        p = helper.isExist($rootScope.txt) ? $rootScope.txt : Resource.getMessage().query().$promise;
        promises.push(p);

        p = helper.isExist(optMeta.options) ? optMeta.options : OptionSrvc.get().$promise;
        promises.push(p);

        return $q.all(promises).then(function( response ){

            /* setting options meta in to rootScope */
            if( !helper.isExist( $rootScope.optMeta) ){
                $rootScope.optMeta =  response[1].data;
            }

            /* setting static message */
            if( ! helper.isExist($rootScope.txt) ){
               MessageSrvc.set( response[0] );
               $rootScope.txt = MessageSrvc.query();
            }

            //***** Identify title for current route and bind to $rootScope.pageTitle *****//
            var currentRoute = $route.current.$$route.originalPath,
                key = false,
                title = OptionSrvc.getByKey('title');
            key = Utils.getKeyByValue(Const.routes, currentRoute);

            if (Const.title[key]) {
                $rootScope.heading = Const.title[key];
                title += ' | ' + Const.title[key];
            } else {
                Utils.log('Please set Title for this route( ' + key + ' ) in app.config.js', 'warning');
            }
            $rootScope.pageTitle = title;

            /* event trigger when setting is changed */
            $rootScope.$on("optionChanged", function (evt, data) {
                $rootScope.pageTitle = data.title+' | '+ Const.title[key];
            });
        });

    };

    $routeProvider
        .when(Const.routes.login, {
            templateUrl: Const.templatePath + '/login.tpl.html',
            controller: 'loginCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.register, {
            templateUrl: Const.templatePath + '/register.tpl.html',
            controller: 'registerCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.recover, {
            templateUrl: Const.templatePath + '/recover.tpl.html',
            controller: 'recoverCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.emailVerify + '/:code', {
            templateUrl: Const.templatePath + '/email-verify.tpl.html',
            controller: 'emailVerifyCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.dashboard, {
            templateUrl: Const.templatePath + '/dashboard.tpl.html',
            controller: 'dashboardCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.mails, {
            templateUrl: Const.templatePath + '/mails.tpl.html',
            controller: 'mailsCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.profile, {
            templateUrl: Const.templatePath + '/profile.tpl.html',
            controller: 'profileCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.transactions, { 
            templateUrl: Const.templatePath + '/transactions.tpl.html',
            controller: 'transactionsCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.activities, {
            templateUrl: Const.templatePath + '/activities.tpl.html',
            controller: 'activitiesCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.payment, {
            templateUrl: Const.templatePath + '/payment.tpl.html',
            controller: 'paymentCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.service, {
            templateUrl: Const.templatePath + '/service.tpl.html',
            controller: 'serviceCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.setting, {
            templateUrl: Const.templatePath + '/option.tpl.html',
            controller: 'optionCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.mmaddress, {
            templateUrl: Const.templatePath + '/mmaddress.tpl.html',
            controller: 'mmaddressCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.customers, {
            templateUrl: Const.templatePath + '/customers.tpl.html',
            controller: 'customersCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.createStaff, {
            templateUrl: Const.templatePath + '/create-staff.tpl.html',
            controller: 'createStaffCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.createMmaddress, {
            templateUrl: Const.templatePath + '/create-mmaddress.tpl.html',
            controller: 'createMmaddressCtrl',
            resolve: {
                Message: setMessage
            }
        })
        .when(Const.routes.paypalPayment, {
            templateUrl: Const.templatePath + '/paypal-payment.tpl.html',
            controller: 'paypalPaymentCtrl',
            resolve: {
                Message: setMessage
            }
        })
        // .when(Const.routes.paypalPayment+ '?:token', {
        //     templateUrl: Const.templatePath + '/paypal-verification.tpl.html',
        //     controller: 'paypalVerificationCtrl',
        //     resolve: {
        //         Message: setMessage
        //     }
        // })
        .otherwise({
            templateUrl: Const.templatePath + '/not-Found.tpl.html',
            controller: 'notFoundCtrl',
            resolve: {
                Message: setMessage
            }
        });

});
