var host = window.location.host, apiPath;

if( host == 'localhost' ){
	//Localhost
	apiPath = 'http://' + host + '/mailsman/api';
}else{
	apiPath = 'http://'+ host +'/api';
}

app.constant( 'Const', {
	apiPath        		: apiPath,
	downloadPath        : apiPath + '/download',
	scriptDebug    		: true,
	templatePath   		: 'build/templates',
	cookieVar      		: 'mailsman',
	totalRecentMails 	: 5,
	perPage        		: 10,
	foreCastTime        : 10000,
	publishableKey 		: 'pk_test_9Z8bZKnvOEUtSp5YZTh5uoxn',
	routes         		: {
		login     	 		: '/',
		register     		: '/register',
		recover      		: '/recover',
		dashboard    		: '/dashboard',
		mails        		: '/mails',
		activities   		: '/activities',
		profile      		: '/profile',
		service      		: '/services',
		payment    		    : '/payment',
		setting       		: '/setting',
		mmaddress    		: '/address',
		customers    		: '/customers',
		createStaff         : '/create-staff',
		createMmaddress     : '/create-mmaddress',
		emailVerify    	    : '/email-verify',
		paypalPayment    	: '/paypal-payment',
		transactions    	: '/transactions',
	},
	title : {
		login     		 : 'Login',
		register  		 : 'Register',
		recover   		 : 'Recover',
		dashboard 		 : 'Dashboard',
		mails     		 : 'Mails',
		activities		 : 'Activities',
		profile   		 : 'Profile',
		service   		 : 'Services',
		payment   		 : 'Payment',
		setting          : 'setting ',
		mmaddress 		 : 'Address',
		customers 		 : 'Customers',
		createStaff      : 'Create Staff',
		createMmaddress  : 'MM Address',
		emailVerify    	 : 'Email verify',
		paypalPayment    : 'Paypal Payment',
		transactions     : 'transactions',
	}
});
