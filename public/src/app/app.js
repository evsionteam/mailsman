var ngDeps = [
    'ngRoute',
    'ngHelper',
    'ngResource',
    'ngSanitize',
    'ngCookies',
    'ngImgCrop',
    'ngFileUpload',
    'ui.bootstrap',
    'ngFileSaver',
    'ng-sweet-alert',
    'angularUtils.directives.dirPagination',
];

var app = angular.module( 'mailsman', ngDeps );

// app init run once time when app initialize

app.run(function( $rootScope, Utils, helper, Const, $location, $timeout, Resource, Base64, UiSrvc, UserSrvc, OptionSrvc, NotificationSrvc  ){

    // init UI service
    UiSrvc.init();

    var notification = NotificationSrvc.getMeta();

    $rootScope.$on( 'mailSeen', function(){
        NotificationSrvc.setMeta( 'unseen', parseInt( notification.unseen ) - 1 );
    });

    $rootScope.$on('logout', function(){

        UserSrvc.initMeta();

        if( helper.isExist( notification.timer ) ){
            $timeout.cancel( notification.timer );
        }
    });

    $rootScope.$on( "$routeChangeStart", function(event, next, current) {

        var cookie = Utils.getCookie(),
            segment = helper.getUriSegment( 1 ),
            slug = '/' + ((segment) ? segment : '');

        if( helper.isExist( cookie ) ) {
            console.log( 'cookies Found');
            Resource.setToken( Base64.decode( cookie.token ) );

            var role       = cookie.role,
                usersRoute = Utils.getUsersRoute();

              $rootScope.user = {
                  id            : cookie.userId,
                  role          : role,
                  is_subscriber : cookie.is_subscriber,
                  balance       : cookie.balance
              };

            if( ! notification.foreCasted && 'administrator' != role ){
                NotificationSrvc.get();
            }

            var defaultRoute;


            if( 'customer' == role ){

                defaultRoute = Const.routes.dashboard;
                if( !_.contains(usersRoute.customer, slug)){
                    $location.path( defaultRoute );
                }

            }else if( 'administrator' == role ){

                defaultRoute = Const.routes.setting;
                if( !_.contains(usersRoute.administrator, slug) ){
                    $location.path( defaultRoute );
                }

            }else if( 'staff' == role ){

                defaultRoute = Const.routes.mails;
                if( ! _.contains(usersRoute.staff, slug) ){
                    $location.path( defaultRoute );
                }
            }

            if ( slug == Const.routes.login ) {
                console.log('default routes is login', defaultRoute );
                $location.path( defaultRoute );
            }

        } else {
            Utils.log( 'Cookie not found.', 'warning' );
            var exception = [ Const.routes.login, Const.routes.paypalPayment, Const.routes.register, Const.routes.recover, Const.routes.emailVerify, Const.routes.payment ];

            if ( !_.contains(exception, slug)) {
                $location.path( Const.routes.login );
            }
        }
    });

}); //End run
