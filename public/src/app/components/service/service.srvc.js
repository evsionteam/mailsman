app.service( 'ServiceSrvc', function( Const, Resource, $rootScope, Utils ) {

    var defaults = {
                isGetting     : false,
                isUpdating    : false,
                isAdding      : false,
                page          : 1,
                perPage       : Const.perPage,
                totalRows     : null,
                services      : [],
                serviceData   : {},
                currentAction : 'add',
                updateId      : null
            },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.initMeta();

    this.get = function(){
        var onGet = function(response) {

                if (200 == response.status) {
                    meta.services = response.data;
                }
                meta.isGetting = false;

            },
            onGetError = function(reason) {
                Utils.log(['Error on Getting Services', reason], 'error');
                meta.isGetting = false;
            };

        meta.isGetting = true;
        Resource.service().query(onGet, onGetError);
    };


    this.add = function(){

        var data = angular.copy( meta.serviceData );
        if( data.has_note ){
            data.has_note = 1;
        }else{
            data.has_note = 0;
        }

        var onSuccess = function( response ){

            var type = 'error';
                if( 201 === response.status ){
                    type = 'success';
                    meta.services.push( response.data );
                    meta.serviceData= {};
                    meta.serviceForm.$setPristine();
                }
                meta.isAdding = false;
                meta.toast({
                    message: $rootScope.txt[ response.code ],
                    clear : true,
                    type  : type,
                    time  : 2000
                });
            },
            onError = function( reason ){
                Utils.log( [ 'Error on saving service', reason ], 'error' );
                meta.isAdding = false;
                meta.toast({
                    message: $rootScope.txt[ reason.code ],
                    clear : true,
                    type  : 'error',
                    time  : 2000
                });
            };
        meta.isAdding = true;
        Resource.service().save( data, onSuccess, onError);
    };

    this.delete = function( id ){
        var onSuccess = function( response ){
                var type='error';
                if( 200 === response.status ){
                    type='success';
                    var index = Utils.findIndex( 'id', id, meta.services );
                    meta.services.splice( index, 1 );
                }
                meta.toast({
                    message: $rootScope.txt[ response.code ],
                    clear : true,
                    type  : type,
                    time  : 2000
                });
            },
            onError = function( reason ){
                meta.toast({
                    message: $rootScope.txt[ reason.code ],
                    clear : true,
                    type  : 'error',
                    time  : 2000
                });
                Utils.log(['Error on delete service', reason], 'error');
            };
        Resource.service().delete({id:id}, onSuccess, onError);
    };

    this.update = function(){
        var data = angular.copy( meta.serviceData );
        if( data.has_note ){
            data.has_note = 1;
        }else{
            data.has_note = 0;
        }
        var onUpdate = function( response ){
                var type = 'error';
                if( 200 === response.status ){
                    type = 'success';
                    meta.serviceData= {};
                    meta.serviceForm.$setPristine();
                    meta.currentAction = 'add';
                    meta.services[Utils.findIndex('id', meta.updateId, meta.services )] = response.data;
                }
                meta.toast({
                    message : $rootScope.txt[response.code],
                    type    : type,
                    time    : 2000,
                    clear   : true
                });
                meta.updateId = null;
                meta.isUpdating = false;
            },
            onError = function( reason ){
                meta.toast({
                    message : $rootScope.txt[reason.code],
                    type    : 'error',
                    time    : 2000,
                    clear   : true
                });
                meta.isUpdating = false;
                Utils.log(['Error on update service', reason], 'error');
            };
        meta.isUpdating = true;
        Resource.service().update( {id:meta.updateId}, data, onUpdate, onError );
    };
});
