app.controller( 'serviceCtrl', function( $scope, ServiceSrvc, Utils, $rootScope ) {

	$scope.sweet = {};
	$scope.sweet.option = {
		title: $rootScope.txt.TXT0134,
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: $rootScope.txt.TXT0135,
		cancelButtonText: $rootScope.txt.TXT0136,
		closeOnConfirm: true,
		closeOnCancel: true
	};

	$scope.meta = ServiceSrvc.getMeta();
	$scope.meta.ispaypal = false;
	ServiceSrvc.get();

	$scope.set = function( id ){
		$scope.meta.currentAction = 'update';
		var index = Utils.findIndex( 'id', id, $scope.meta.services );

		// setting id in meta
		$scope.meta.updateId = $scope.meta.services[index].id;

		// setting value in feild
		$scope.meta.serviceData.title = $scope.meta.services[index].title;
		$scope.meta.serviceData.description = $scope.meta.services[index].description;
		$scope.meta.serviceData.cost = parseFloat($scope.meta.services[index].cost);
		$scope.meta.serviceData.position = parseInt($scope.meta.services[index].position);
		$scope.meta.serviceData.has_note = $scope.meta.services[index].has_note === '1' ? true : false;

	};

	$scope.add = function(){
		if($scope.meta.currentAction === 'add' ){
			ServiceSrvc.add();
		}else if( $scope.meta.currentAction === 'update'){
			ServiceSrvc.update();
		}
	};

	$scope.delete = ServiceSrvc.delete;

});
