app.service('RegisterSrve', function($location, Const, Resource, Utils, Base64, $timeout, MessageSrvc, LoginSrvc, $rootScope ) {

    var meta = {
        isregistering       : false,
        isActive            : true,
        emailError          : false,
        passwordError       : false,
        ispaypal            : false,
        routes: {
            register        : '#' + Const.routes.register,
            recover         : '#' + Const.routes.recover,
            login           : '#' + Const.routes.login,
            emailVerify     : '#' + Const.routes.emailVerify,
            payment         : '#' + Const.routes.payment,
            paypalPayment   : '#' + Const.routes.paypalPayment,
        }
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.register = function(userData) {

        meta.isregistering = true;
        var succeededResponse = function(response) {

                meta.isregistering = false;

                if( 201 !== response.status ){

                  var length = response.data.length;

                  for(var i=0; i < length; i++){
                    if( 'email' === response.data[i].field){
                      meta.emailError = MessageSrvc.get( response.data[i].code );
                    }
                    if( 'password' === response.data[i].field ){
                      meta.passwordError = true;
                    }
                  }

                }
            },
            failedResponse = function(reason){

                meta.isregistering = false;

                meta.status.init({
                    message: $rootScope.txt.ERR008,
                    error: true
                });
                Utils.log(['Error on user register', reason], 'error');
            };
        console.log( userData, 'register service' ); 
        return Resource.user().save( userData, succeededResponse, failedResponse ).$promise ;
    };

});
