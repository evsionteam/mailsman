app.controller('registerCtrl', function( $rootScope, $scope, RegisterSrve, MessageSrvc, Utils, $filter, PaymentSrvc, StripeSrvc, Const, helper ) {

    // get meta
    $scope.meta = RegisterSrve.getMeta();

    $scope.sweet = {};
    $scope.sweet.option = {
        title: $rootScope.txt.TXT0134,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: $rootScope.txt.TXT0135,
        cancelButtonText: $rootScope.txt.TXT0136,
        closeOnConfirm: true,
        closeOnCancel: true
    };

	$scope.stripe = StripeSrvc.getMeta();
	$scope.years = StripeSrvc.getExpYears();
	$scope.card = {};

    // register function
    $scope.register = function( form ) {
        RegisterSrve.register( $scope.userInfo ).then( function( response ){
            if( 201 === response.status){
                StripeSrvc.setMeta( 'userToken', response.data.token );
                $scope.createToken( form );
            }

        });
    };

	$scope.subscriptionCost =  $filter('mmCurrency')( $rootScope.optMeta.subscription_cost );

	$scope.createToken = function( form ){

		StripeSrvc.setMeta( 'card', $scope.card );
        /* For send email or not */
        $scope.stripe.sendEmail = true;

		var error = StripeSrvc.createToken();

		angular.forEach( error, function( value, key ){
			if( value == 'exp_date' ){
				form.exp_month.$setValidity( 'required', false );
				form.exp_year.$setValidity( 'required', false );
			}else{
				form[ value ].$setValidity( 'required', false );
			}
		});

	};

    $scope.checkoutWithPaypal = function(){
        $scope.meta.ispaypal = !$scope.meta.ispaypal;
    };

});
