app.controller( 'createStaffCtrl', function( $scope, $rootScope, UserSrvc, helper ) {

	$scope.sweet = {};
    $scope.sweet.option = {
        title				: $rootScope.txt.TXT0134,
        type				: "warning",
        showCancelButton	: true,
        confirmButtonColor  : "#DD6B55",
        confirmButtonText	: $rootScope.txt.TXT0135,
        cancelButtonText	: $rootScope.txt.TXT0136,
        closeOnConfirm		: true,
        closeOnCancel		: true
    };

	/* for reset form */
	$scope.$watch('meta.currentAction', function() {
		if( $scope.meta.currentAction === 'add' ){
			$scope.staff ={};
		} 
    });

	$scope.meta = UserSrvc.getMeta();

	$scope.staff = {
		user_role : 'staff'
	};

	$scope.get = function(page) {
        UserSrvc.setMeta('page', page);
        UserSrvc.getStaffs();
    };

    $scope.get(1);

    $scope.clear = function(){
        if( ! helper.isExist( $scope.meta.searchKey ) ){
            if( $scope.searchForm.$submitted ){
                $scope.get(1);
            }
            $scope.searchForm.$setPristine();
         }
    };

    $scope.create = function(){
    	$scope.meta.error = {};
        $scope.staff.user_role = "staff";
        
    	UserSrvc.create( $scope.staff ).then( function( response ){
            if( 200 == response.status ){
                $scope.staff.email='';
                $scope.staff.password='';
                $scope.staff.reTypePassword='';
                $scope.staffForm.$setPristine();
            }
        });
    };

	$scope.delete = function( userId ){
		UserSrvc.delete( userId);
	};

	UserSrvc.setMeta( 'currentAction', 'add' );
	$scope.set = function( id, staffInfo ){
		UserSrvc.setMeta( 'currentAction', 'update' );
		UserSrvc.setMeta( 'staffId', id );
		$scope.staff.email = staffInfo.email;
	};

    $scope.update = function(){
		UserSrvc.staffUpdate( $scope.staff );
    };

});
