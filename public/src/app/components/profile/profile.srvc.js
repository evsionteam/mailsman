app.service( 'ProfileSrvc', function( Const, UserSrvc ) {

    var meta = {};

    this.initMeta = function(){
        meta = {
          usermeta    : UserSrvc.getMeta()
        };
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

});
