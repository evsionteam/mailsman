app.controller( 'profileCtrl', function( $rootScope, $scope, Utils, helper, ProfileSrvc, UserSrvc, $uibModal ) {

	ProfileSrvc.initMeta();

	$scope.meta = ProfileSrvc.getMeta();
	$scope.mailOptions = {
		title      : $rootScope.txt.TXT0037,
		search     : false,
		tab        : false,
		pagination : false,
		cols       : { date : false, actions : false }
	};

	/* For email verification notice */
	$scope.showNotice = false;
	var cookies = Utils.getCookie();
	if( cookies.is_verified != 1 && $rootScope.user.role === 'customer'){
		$scope.showNotice = true;
	}

	/* change password */
	$scope.changePasswrod = function(){
		UserSrvc.updatePassword( $rootScope.user.id, { old_password:$scope.meta.usermeta.password.old, new_password: $scope.meta.usermeta.password.new} );
	};

	/* save user info and update */
	$scope.update = function( key ){
			var data = { meta : null },
			meta = {};
			meta[key] = $scope.meta.usermeta.userInfo.meta[ key ];
			data.meta = meta;
			if( $scope.meta.usermeta.userInfo.meta[key] != $scope.meta.usermeta.oldUserInfo.meta[key] ){
			if( typeof $scope.meta.usermeta.userInfo.meta[ key ] !== 'undefined' || $scope.meta.usermeta.userInfo.meta[ key ] === ""){
				UserSrvc.update( data );
				$scope.meta.usermeta.savingRequestNo = ++$scope.meta.usermeta.savingRequestNo;
			}
			}else{
			Utils.log('value not changed');
			}
	};

	$scope.editProfilePic = function(){
		var modalInstance = $uibModal.open({
				animation: true,
				templateUrl: 'editProfilePic.html',
				controller: function ($scope, $rootScope, $uibModalInstance, UserSrvc ) {
						$scope.profilePic = '';
						$scope.croppedProfilePic = '';
						var handleFileSelect = function (evt) {
								var file = evt.currentTarget.files[0];
								var reader = new FileReader();
								reader.onload = function (evt) {
										$scope.$apply(function ($scope) {
												$scope.profilePic = evt.target.result;
										});
								};
								reader.readAsDataURL(file);
								$scope.cropArea = true;
						};

						$scope.cancel = function () {
								var data = { meta:{ 'avatar':$scope.croppedProfilePic } };
								$uibModalInstance.close('cancel');
								if ($scope.profilePic !== '') {
										UserSrvc.update( data );
								}
						};
						jQuery(document).on('change', '#fileInput', handleFileSelect);
				},
				size: 'md'
		});
	};

});
