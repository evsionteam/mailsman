app.service('AddressSrvc', function($rootScope, MessageSrvc, Const, Resource, Utils) {

    var defaults = {
            isGetting: false,
            isUpdating: false,
            isSaving: false,
            mmAddress: null,
            newMmAddress: null,
            currentAction: 'add',
            updateId: null,
            perPage: Const.perPage,
            page: 1,
            searchKey: null,
            totalRows: null,
            id: 0,
            data: [],
            user: null
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    /* Get all mm address */
    this.get = function() {

        var onGet = function(response) {
                if (200 == response.status) {
                    meta.data = response.data;
                    meta.totalRows = response.total_rows;
                }
                meta.isGetting = false;
            },
            onGetError = function(reason) {
                Utils.log(['Error on Getting Options', reason], 'error');
                meta.isGetting = false;
            };

        meta.isGetting = true;

        return Resource.mmAddress().get({
            id: meta.id,
            page: meta.page,
            search: meta.searchKey
        }, onGet, onGetError);
    };

    this.update = function(data) {
        var onUpdate = function(response) {
                if (200 == response.status) {
                    var index = Utils.findIndex('id', meta.updateId, meta.data);
                    meta.data[index].mm_address = data.mm_address;
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                    meta.currentAction = 'add';
                    // meta.updateId      = null;
                    meta.newMmAddress = null;
                }
                meta.isUpdating = false;
            },
            onUpdateError = function(reason) {
                meta.toast({
                    message: $rootScope.txt[reason.TXT0069],
                    type: 'success',
                    time: 3000
                });
                Utils.log(['Error on Updating Options', reason], 'error');
                meta.isUpdating = false;
            };

        meta.isUpdating = true;
        Resource.mmAddress().update({
            id: meta.updateId
        }, data, onUpdate, onUpdateError);
    };

    this.assign = function(data) {
        var onAssign = function(response) {
                var indexMmaddress   = Utils.findIndex('id', response.data.id, meta.data),
                    indexAssignEmail = Utils.findIndex('id', data.id, meta.userMeta.unVerifiedUser);

                    meta.data[indexMmaddress].has_assigned = 'yes';
                    meta.data[indexMmaddress].assigned_to_id = data.id;
                    meta.data[indexMmaddress].assigned_to_email = meta.userMeta.unVerifiedUser[indexAssignEmail].email;
                    meta.userMeta.unVerifiedUser.splice( indexAssignEmail, 1 );

                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 2000,
                        clear: true
                    });
            },
            onAssignError = function(reason) {
              meta.toast({
                  message: $rootScope.txt[reason.code],
                  type: 'error',
                  time: 2000,
                  clear: true
              });
              Utils.log(['Error on mmaddress unassign', reason], 'error');
            };
        // meta.toast({
        //     message: $rootScope.txt.TXT0070,
        //     type: 'loader',
        //     time: 30000
        // });
        Resource.mmAddress(meta.updateId).assign(data, onAssign, onAssignError);
    };


    this.unassign = function(mmaddressId, userId) {

        var onSuccess = function(response) {
           var indexMmaddress   = Utils.findIndex('id', mmaddressId, meta.data);
              meta.data[indexMmaddress].has_assigned = null;
              meta.data[indexMmaddress].assigned_to_email = null;
              meta.data[indexMmaddress].assigned_to_id = null;
              meta.userMeta.unVerifiedUser.push( response.data );

              meta.toast({
                  message: $rootScope.txt[response.code],
                  type: 'success',
                  time: 2000
              });
            },
            onError = function(reason) {
                meta.toast({
                    message: $rootScope.txt[reason.code],
                    type: 'error',
                    time: 2000
                });
            };

        // meta.toast({
        //     message: $rootScope.txt.TXT0073,
        //     type: 'loader',
        //     time: 30000
        // });

        Resource.mmAddress(mmaddressId).assign({
            id: userId,
            value: null
        }, onSuccess, onError);

    };


    /* Add mm address */
    this.add = function(data) {

        var onSave = function(response) {
                meta.isSaving = false;
                if (200 == response.status) {
                    meta.data.push({
                        "id": response.data.id.toString(),
                        "mm_address": response.data.mm_address,
                        "created_at": response.data.created_at,
                        "updated_at": response.data.updated_at,
                    });
                    meta.mmAddress = null;
                    meta.form.$setPristine();
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                } else {
                    meta.toast({
                        message: $rootScope.txt.TXT0062e,
                        type: 'error',
                        time: 10000
                    });
                }
            },
            onSaveError = function(reason) {
                meta.toast({
                    message: $rootScope.txt.TXT0062e,
                    type: 'error',
                    time: 10000
                });
                
            };

        meta.isSaving = true;
        Resource.mmAddress().save(data, onSave, onSaveError);
    };

    /* Add mm delete */
    this.delete = function(id) {
        var onDelete = function(response) {
                if (200 == response.status) {
                    meta.data.splice(Utils.findIndex('id', id, meta.data), 1);
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                }
            },
            onDeleteError = function(response) {
                meta.toast({
                    message: $rootScope.txt.TXT0068,
                    type: 'error',
                    time: 3000
                });
            };
        Resource.mmAddress().delete({
            id: id
        }, onDelete, onDeleteError);
    };

});
