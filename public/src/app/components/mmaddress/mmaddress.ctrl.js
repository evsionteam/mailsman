app.controller( 'mmaddressCtrl', function( $scope, AddressSrvc, UserSrvc, Utils, helper ){

	/* set meta value in scope */
	$scope.meta = AddressSrvc.getMeta();

	$scope.get = function( page ){
		AddressSrvc.setMeta( 'page', page );
		AddressSrvc.get();
	};
	$scope.get( 1 );
	AddressSrvc.setMeta( 'userMeta', UserSrvc.getMeta() );

	/* get all customers set in*/
	UserSrvc.getUnVerifiedUsers();

	/* Add mm address */
	$scope.add = function(){
		var data = { mm_address : $scope.meta.mmAddress, has_assigned: 'no' } ;
		AddressSrvc.add( data );
	};

  /* delete mm address */
	$scope.delete = function( id ){
		AddressSrvc.delete( id );
	};

	/* set value of edit mm addres in input field */
	$scope.set = function( id, OldMmAddress ){
		$scope.meta.newMmAddress = OldMmAddress;
		$scope.meta.currentAction = 'update';
		$scope.meta.updateId  = id;
	};

	/* update mm address */
	$scope.update = function(){
		var data = { mm_address : $scope.meta.newMmAddress } ;
		AddressSrvc.update( data );
	};

	/* assign user to mmaddress*/
	$scope.assign = function( mmaddressId ){
		if( helper.isExist( $scope.user.email[mmaddressId] ) ){
			AddressSrvc.setMeta( 'updateId', mmaddressId );
			AddressSrvc.assign( { id: $scope.user.email[mmaddressId], value: 'yes'} );
		}
	};

  /* unassign user to mmaddress*/
	$scope.unassign = AddressSrvc.unassign;

});
