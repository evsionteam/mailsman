app.service('TransactionsSrvc', function( Resource, Const, Utils){
    var defaults = {
        isGetting     : false,
        isSearching   : false,
        currentPage   : 1,
        perPage       : Const.perPage,
        transactions  : [],
        totalRows     : 0,
        searchedUser  : [],
        isGettingUsers: false,
        selectedUser  : null,
        userId        : null,
      },
      meta = {};

    this.initMeta = function() {
      angular.forEach( defaults, function( value, key ){
          meta[ key ] = value;
      });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.get = function(){
        var onSuccess = function( response ){
                if( 200 === response.status ){
                    meta.transactions = response.data;
                    meta.totalRows = response.total_rows.length > 0 ? response.total_rows : [];
                }
                meta.isGetting = false;
            },
            onError = function( reason ){
                Utils.log(['Error on loading transaction', reason]);
                meta.isGetting = false;
            };
            meta.transactions = [];
            meta.isGetting = true;
            Resource.transactions().get( {page : meta.currentPage, userId: meta.userId }, onSuccess, onError );

    };

    this.searchUser = function( userEmail ){
        var onSuccess = function( response ){
                meta.searchedUser = response.data;
                meta.searchedUser = response.status === 404 ? [] : response.data;
                meta.isSearching = false;
            },
            onError   = function( reason ){
                meta.isSearching = false;
            };
        meta.isSearching = true;
        meta.searchedUser = [];
        Resource.user().get( { search: userEmail, role: 'customer', id: 0, page: "no_page" }, onSuccess, onError );
    };

});
