app.controller( 'transactionsCtrl', function( $scope, TransactionsSrvc, $http, $timeout, paginationService, Utils, $rootScope ){

    $scope.meta = TransactionsSrvc.getMeta();
    $scope.meta.selectedUser = null;
    $scope.meta.userId = null;
    /* loading customer transection data */

    $scope.get = function( page ) {
        $scope.meta.currentPage = page;
        TransactionsSrvc.get();
    };

    $scope.meta.currentPage = 1;
    TransactionsSrvc.get();

    /* search */
    $scope.ischanged = false;
    $scope.search = function(){
        $scope.meta.isSearching = true;
        $scope.ischanged = true;
        TransactionsSrvc.searchUser( $scope.userEmail );
    };

    $scope.isFocus = false;
    $scope.selected = function(){
        $scope.ischanged = true;
    };

    $scope.disselected = function(){
        $scope.ischanged = false;
    };

    $scope.clear = function(){
        $scope.meta.searchedUser = [];
        $scope.userEmail = null;
    };

    $scope.setEmail = function( id, email ){
        $scope.meta.selectedUser = email;
        $scope.userEmail = email;
        $scope.meta.userId = id;
        TransactionsSrvc.get();
        $scope.meta.currentPage = 1 ;
    };

    $scope.getAllUsers = function(){
      if( $scope.meta.selectedUser ){
        $scope.meta.selectedUser = null;
        $scope.meta.userId = null;
        $scope.get(1);
      }
      $scope.userEmail = null;
    };

    /* For email verification notice */
  	$scope.showNotice = false;
  	var cookies = Utils.getCookie();
  	if( cookies.is_verified != 1 && $rootScope.user.role === 'customer'){
  		$scope.showNotice = true;
  	}

});
