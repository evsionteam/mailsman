app.controller('mailsCtrl', function( $rootScope, $scope, helper, $routeParams, MailSrvc, Utils ){

	$scope.mailOptions = {
		title  : $rootScope.txt.TXT0014,
		search : true,
		tab    : true
	};

	$scope.meta = MailSrvc.getMeta();

	/* For email verification notice */
	$scope.showNotice = false;
	var cookies = Utils.getCookie();
	if( cookies.is_verified != 1 && $rootScope.user.role === 'customer'){
		$scope.showNotice = true;
	}  

});
