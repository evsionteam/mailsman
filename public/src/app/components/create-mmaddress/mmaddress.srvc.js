app.service('createMmaddressSrvc', function($rootScope, MessageSrvc, Const, Resource, Utils) {

    var defaults = {
            isGetting: false,
            isUpdating: false,
            isSaving: false,
            mmAddress: null,
            newMmAddress: null,
            currentAction: 'add',
            updateId: null,
            perPage: Const.perPage,
            page: 1,
            searchKey: null,
            totalRows: null,
            id: 0,
            data: [],
            user: null
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    /* Get all mm address */
    this.get = function() {

        var onGet = function(response) {
                if (200 == response.status) {
                    meta.data = response.data;
                    meta.totalRows = response.total_rows;
                }
                meta.isGetting = false;
            },
            onGetError = function(reason) {
                Utils.log(['Error on Getting Options', reason], 'error');
                meta.isGetting = false;
            };

        meta.isGetting = true;

        return Resource.mmAddress().get({
            id: meta.id,
            page: meta.page,
            search: meta.searchKey
        }, onGet, onGetError);
    };

    this.update = function() {
        var onUpdate = function( response ) {
                if (200 == response.status) {
                    var index = Utils.findIndex('id', meta.updateId, meta.data);
                    meta.data[index] = meta.newMmAddress;
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                    meta.currentAction = 'add';
                    meta.updateId      = null;
                    meta.newMmAddress = null;
                }
                meta.isUpdating = false;
            },
            onUpdateError = function( reason ) {
                meta.toast({
                    message: $rootScope.txt[reason.TXT0069],
                    type: 'success',
                    time: 3000
                });
                Utils.log(['Error on Updating Options', reason], 'error');
                meta.isUpdating = false;
            };

        meta.isUpdating = true;
        Resource.mmAddress().update({
            id: meta.updateId
        }, meta.newMmAddress , onUpdate, onUpdateError);
    };


    /* Add mm address */
    this.add = function() {

        var onSave = function(response) {
                meta.isSaving = false;
                if (200 == response.status) {
                    meta.data.push( response.data );
                    meta.form.$setPristine();
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                    meta.mmaddress.address_line1 = meta.mmaddress.state = meta.mmaddress.phone = meta.mmaddress.address_line2 = meta.mmaddress.country = meta.mmaddress.city = meta.mmaddress.zip = null;
                } else {
                    meta.toast({
                        message: $rootScope.txt.TXT0062e,
                        type: 'error',
                        time: 10000
                    });
                }
            },
            onSaveError = function(reason) {
                meta.toast({
                    message: $rootScope.txt.TXT0062e,
                    type: 'error',
                    time: 10000
                });

                Utils.log(['Error in adding mmaddress', reason], 'error');
            };

        meta.isSaving = true;
        Resource.mmAddress().save( meta.mmaddress, onSave, onSaveError );
    };

    /* Add mm delete */
    this.delete = function(id) {
        var onDelete = function(response) {
                if (200 == response.status) {
                    meta.data.splice(Utils.findIndex('id', id, meta.data), 1);
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'success',
                        time: 3000
                    });
                }
            },
            onDeleteError = function(reason) {
                meta.toast({
                    message: $rootScope.txt.TXT0068,
                    type: 'error',
                    time: 3000
                });
                Utils.log(['Error on delete mmaddress', reason], 'error');
            };
        Resource.mmAddress().delete({
            id: id
        }, onDelete, onDeleteError);
    };

});
