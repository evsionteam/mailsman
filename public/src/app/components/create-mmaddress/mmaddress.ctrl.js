app.controller('createMmaddressCtrl', function( $scope, $rootScope, createMmaddressSrvc, UserSrvc, Utils, helper ) {

    $scope.sweet = {};
    $scope.sweet.option = {
        title: $rootScope.txt.TXT0134,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: $rootScope.txt.TXT0135,
        cancelButtonText: $rootScope.txt.TXT0136,
        closeOnConfirm: true,
        closeOnCancel: true
    };

    /* set meta value in scope */
    $scope.meta = createMmaddressSrvc.getMeta();
    $scope.meta.currentAction = 'add';

    $scope.get = function(page) {
        createMmaddressSrvc.setMeta('page', page);
        createMmaddressSrvc.get();
    };
    $scope.get(1);
    createMmaddressSrvc.setMeta('userMeta', UserSrvc.getMeta());


    /* Add mm address */
    $scope.add = createMmaddressSrvc.add;

    /* delete mm address */
    $scope.delete = function(id) {
        createMmaddressSrvc.delete(id);
    };

    /* set value of edit mm addres in input field */
    $scope.set = function(id, OldMmAddress) {
        $scope.meta.newMmAddress = angular.copy( OldMmAddress );
        $scope.meta.currentAction = 'update';
        $scope.meta.updateId = id;
    };

    /* update mm address */
    $scope.update = createMmaddressSrvc.update;

    /* search mmaddress*/
    $scope.showSearch = true;
    $scope.search = function() {
        $scope.showSearch = false;
        createMmaddressSrvc.search($scope.showSearch);
    };

    $scope.search = function() {
        $scope.meta.id = 0;
        $scope.get(1);
    };

    $scope.clear = function() {
        if (!helper.isExist($scope.meta.searchKey)) {

            if ($scope.mmaddress_search.$submitted) {
                $scope.get(1);
            }
            $scope.mmaddress_search.$setPristine();
        }
    };

});
