app.service('LoginSrvc', function( $location, Const, Resource, Utils, Base64, MessageSrvc, $rootScope ) {

    var meta = {
        isLoggingIn: false,
        routes: {
            register     : '#' + Const.routes.register,
            recover      : '#' + Const.routes.recover,
            login        : '#' + Const.routes.login,
        },
        isUserExist: false,
        isPasswordInvalid: false
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.login = function(userData) {
        meta.isLoggingIn = true;

        var succeededResponse = function(response) {

                meta.isLoggingIn = false;
                meta.loginForm.$setPristine(); /* for don't show form's validation error */
                if (200 == response.status){
                    Resource.setToken( response.data.token );
                    Utils.setCookie({
                        token         : Base64.encode(response.data.token),
                        userId        : response.data.user_id,
                        role          : response.data.role,
                        is_subscriber : response.data.is_subscribe,
                        balance       : response.data.balance,
                        is_verified   : response.data.is_verified,
                    });

                    if( 'customer' == response.data.role && '0' == response.data.is_subscribe ){
                        $location.path(Const.routes.payment);
                    }else{
                        if(response.data.role === 'administrator'){
                            $location.path(Const.routes.customers);
                        }else{
                            $location.path(Const.routes.dashboard);
                        }
                    }

                }else if( 400 === response.status){
                    meta.toast({
                        message: $rootScope.txt[response.code],
                        type: 'error',
                        time: 4000
                    });
                    Utils.log(['Error on login', response], 'error');
                }else{
                    Utils.log(['Error on login field', response], 'error');
                    if ( 'email' === response.data.field ) {
                        meta.isUserExist = MessageSrvc.get( response.code );
                    }else {
                        meta.isPasswordInvalid = MessageSrvc.get( response.code );
                    }
                }

            },
            failedResponse = function( reason ) {

                meta.isLoggingIn = false;
                meta.status.init({
                    message: $rootScope.txt.ERR008 ,
                    error: true
                });
                Utils.log( [ 'Error on Login', reason ], 'error' );

            };

        Resource.user().login( userData, succeededResponse, failedResponse );
    };

});
