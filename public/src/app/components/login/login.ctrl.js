app.controller( 'loginCtrl', function( $scope, LoginSrvc,  MessageSrvc, Utils ){

	$scope.meta = LoginSrvc.getMeta();

	$scope.login = function(){
		LoginSrvc.login( $scope.user );
	};

});
