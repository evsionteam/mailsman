app.service('PaypalPaymentSrvc', function( Const, Resource, $location, $window, $rootScope, $timeout, Utils, Base64 ) {

    var meta = {
        isregistering    : false,
        isActive         : true,
        sendEmail        : true,
        isLoadingFund    : false,
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.subscribe = function( response ){

		var onSuccess = function( response ){
				if( 200 == response.status ){
                    $window.location.href = response.data;
				}
			},
			onError = function( reason ){
				Utils.log( [ 'Error on subscribe', reason ], 'error' );
                meta.isregistering = false;
			};

        meta.isregistering = true;
		Resource.subscribe( meta.userToken ).save( {payment_mode : 'paypal', send_email : meta.sendEmail }, onSuccess, onError );
	};

    this.loadFund = function( amount ){
        var onSuccess = function( response ){
                if( 200 === response.status)
                    $window.location.href = response.data;
            },
            onError = function( reason ){
                Utils.log( ['Error on loading fund', reason] );
                meta.isLoadingFund = false;
            };
            meta.isLoadingFund = true;
            Resource.loadFund().save( { payment_mode : 'paypal', load_amount : amount }, onSuccess, onError );
    };

    this.proccess =  function( qs ){
        var onSuccess = function( response ){

            if( 200 === response.status ){
                if( qs.paid_for === 'subscribe'){
                    Utils.setCookie({
                        token         : Base64.encode(response.data.token),
                        userId        : response.data.user_id,
                        role          : response.data.role,
                        is_subscriber : response.data.is_subscribe,
                        balance       : response.data.balance,
                        is_verified   : response.data.is_verified,
                    });
                }else if( qs.paid_for === 'balance_load' ){
                    var cookie = Utils.getCookie();
    				cookie.balance = response.data.balance;
    				Utils.setCookie(cookie);
                }
                $location.path( Const.routes.dashboard );

            }else {
                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: 'error',
                    time: 4000
                });
                if( qs.paid_for === 'subscribe'){
                    $timeout(function () {
                        $location.path( Const.routes.login );
                    }, 4000 );
                    return;
                }

                $timeout(function () {
                    $location.path( Const.routes.dashboard );
                }, 4000 );
            }
        },
        onError = function( reason ){
            meta.toast({
                message: $rootScope.txt[reason.code],
                type: 'error',
                time: 4000
            });
            $timeout(function () {
                $location.path( Const.routes.dashboard );
            }, 4000 );
        };
        Resource.paypal().save( {status: 'success'},{ data : qs }, onSuccess, onError );
    };


});
