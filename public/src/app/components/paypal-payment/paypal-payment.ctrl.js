app.controller( 'paypalPaymentCtrl', function( $scope, PaypalPaymentSrvc, RegisterSrve, $location, Const, MessageSrvc ) {
    $scope.meta = PaypalPaymentSrvc.getMeta();
    $scope.registerMeta = RegisterSrve.getMeta();
    var qs = $location.search();

    if(  _.keys(qs).length > 0 ){
        qs.paid_for = 'subscribe';
        $scope.template = Const.templatePath + '/verify.tpl.html';
        PaypalPaymentSrvc.proccess( qs );
        console.log( qs );
    }else{
        $scope.template = Const.templatePath + '/paypal-register.tpl.html';
    }

    // register function
    $scope.pay = function() {
        RegisterSrve.register( $scope.meta.user ).then( function( response ){
            if( 201 === response.status){
                PaypalPaymentSrvc.setMeta( 'userToken', response.data.token );
                PaypalPaymentSrvc.subscribe();
            }else {
              var length = response.data.length;
              for(var i=0; i < length; i++){
                if( 'email' === response.data[i].field){
                  $scope.meta.emailError = MessageSrvc.get( response.data[i].code );
                }
                if( 'password' === response.data[i].field ){
                  $scope.meta.passwordError = true;
                }
              }
            }

        }, function( reason){
            $scope.meta.isregistering = false;
        });
    };
});
