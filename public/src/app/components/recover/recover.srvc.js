app.service('RecoverSrvc', function($location, Const, Utils, Resource, $timeout, MessageSrvc) {

    var meta = {};

    this.initMeta = function(){
      meta = {
            isRecovering  : false,
            haveCode      : false,
            isActive      : true,
            emailError    : false,
            codeError     : false,
            passwordError : false,
            message       : MessageSrvc.query(),
            extraForm     : Const.templatePath + '/extra-field.tpl.html',
            routes: {
              login: '#' + Const.routes.login
            }
      };
    };

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.forget = function(userData) {
        meta.isRecovering = true;
        var succeededResponse = function(response) {
                meta.isRecovering = false;
                Utils.log('succeededResponse', 'info');
                if (200 === response.status) {
                    Utils.log(response);
                    Utils.log(meta.message[response.data.code]);
                    meta.haveCode = true;
                    meta.status.init({
                        message: meta.message[response.data.code],
                        success: true
                    });
                } else {
                    meta.emailError = meta.message[response.data.code];
                }
            },
            failedResponse = function( reason ) {
                Utils.log(['Error on forget password', reason], 'error');
                meta.isRecovering = false;
                meta.status.init({
                    message: meta.message.TXT0011,
                    error: true
                });
            };
        Resource.user().forget(userData, succeededResponse, failedResponse);
    };

    this.recover = function(userData) {
        meta.isRecovering = true;
				Utils.log(userData);
        var succeededResponse = function(response) {
                Utils.log(response);
                meta.isRecovering = false;
                if (200 === response.status) {
                    meta.status.init({
                        message: meta.message[response.data.code],
                        succcess: true
                    });
										$timeout(function(){
											meta.haveCode = false;
											$location.path(Const.routes.login);
										},2000);
                } else {
                    Utils.log(['Error on recover', response], 'error');
                    var length = response.data.length;
                    for (var i = 0; i < length; i++) {
                        if ('email' === response.data[i].field) {
                            Utils.log('email Error');
                            meta.emailError = meta.message[response.data[i].code];
                        }
                        if ('token' === response.data[i].field) {
                            Utils.log('token error');
                            meta.codeError = meta.message[response.data[i].code];
                        }
                        if ('password' === response.data[i].field) {
                            Utils.log('password error');
                            Utils.log(response.data[i].code);
														meta.passwordError = meta.message[response.data[i].code];
                        }
                    }
                }
            },
            failedResponse = function(reason) {
                Utils.log(['Error on recover password', reason], 'error');
                meta.isRecovering = false;
                meta.status.init({
                    message: meta.message.TXT0011,
                    error: true
                });
            };
        Resource.user().recover(userData, succeededResponse, failedResponse);
    };

});
