app.controller('recoverCtrl', function( $scope, RecoverSrvc, Utils ) {

    RecoverSrvc.initMeta();
    $scope.meta = RecoverSrvc.getMeta();
    // recover function
    $scope.recover = function() {

        var objLength = Object.keys($scope.user).length;
        
        if ( 1 === objLength ) {
			RecoverSrvc.forget($scope.user);
        }else{
				if( $scope.user.password.length >= 8 ){
					RecoverSrvc.recover($scope.user);
				}else{
					Utils.log('check password length');
				}
        }
    };

});
