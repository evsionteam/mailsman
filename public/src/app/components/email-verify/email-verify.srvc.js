app.service('EmailVerifySrvc', function( Resource, $rootScope, $location, Const, $timeout, Utils, helper ){

    var defaults = {
            isEmailVerified     : false,
            errorOnVerification : false,
            email               : null,
            code                : null,
            routes              : {
                register        : '#' + Const.routes.register,
                login           : '#' + Const.routes.login,
            }
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.verify = function( verificationCode ){
        var onSuccess = function( response ){
                if( 200 === response.status){
                    meta.isEmailVerified = true;
                    var cookies = Utils.getCookie();
                    if( ! helper.isExist( cookies.is_verified ) ){
                        console.log('no');
                        cookies.is_verified = 1;
                        Utils.setCookie( cookies );
                    }
                    $timeout(function(){
                        $location.path( Const.routes.login );
                    },2000);
                }else{
                    meta.errorOnVerification = true;
                }
            },
            onError   = function( reason ){
                meta.errorOnVerification = true;
                meta.toast({
                    message : $rootScope.txt[reason.code],
                    type    : 'error',
                    clear   : true,
                    time    : 2000
                });
            };

        Resource.user().userVerification({verification_code: verificationCode}, onSuccess, onError);
    };

});
