app.controller( 'emailVerifyCtrl', function( $scope, EmailVerifySrvc, $routeParams ){

    $scope.meta = EmailVerifySrvc.getMeta();
    $scope.verify = EmailVerifySrvc.verify;
    $scope.meta.isEmailVerified = false;

    /* Requesting for verify email */
    EmailVerifySrvc.verify( $routeParams.code );

});
