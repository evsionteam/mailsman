app.controller( 'activitiesCtrl', function( $scope, ActivitiesSrvc, Utils, $rootScope, Const ) {

	$scope.options ={
		pagination : true
	};
	
	/* For email verification notice */
	$scope.showNotice = false;
	var cookies = Utils.getCookie();
	if( cookies.is_verified != 1 && $rootScope.user.role === 'customer'){
		$scope.showNotice = true;
	}

});
