app.service('ActivitiesSrvc', function(Const, Utils, Resource, helper) {

    var defaults = {
            isGetting    : true,
            currentPage         : 1,
            perPage      : Const.perPage,
            totalRows    : null,
            activities   : [],
            id           : 0,  
            selectedUser : null,
            userId       : null,
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.get = function(limit) {

        var onGet = function(response) {

                if (200 == response.status) {

                    meta.activities = response.data;
                    meta.totalRows = response.total_rows;
                }

                meta.isGetting = false;
            },
            onGetError = function(reason) {

                Utils.log(['Error on Getting Activites', reason], 'error');
                meta.isGetting = false;
            };

        meta.isGetting = true;
        if (helper.isExist(limit)) {
            meta.perPage = limit;
            meta.totalRows = limit;
            Resource.activity().getRecent({
                page: limit
            }, onGet, onGetError);
        } else {
            Resource.activity().get({
                id: meta.id,
                page: meta.currentPage,
                userId: meta.userId
            }, onGet, onGetError);
        }
    };

});
