app.controller( 'dashboardCtrl', function( $rootScope, $scope, DashboardSrvc, OptionSrvc, Utils ) {

	$scope.subscriptionCost = OptionSrvc.getByKey('subscription_cost');
	$scope.meta = DashboardSrvc.getMeta();
	$scope.mailOptions = { title : $rootScope.txt.TXT0037 , search : false, tab : false, pagination: false };

	/* For email verification notice */
	$scope.showNotice = false;
	var cookies = Utils.getCookie();
	if( cookies.is_verified != 1 && $rootScope.user.role === 'customer'){
		$scope.showNotice = true;
	}

});
