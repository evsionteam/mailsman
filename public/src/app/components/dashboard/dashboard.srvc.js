app.service('DashboardSrvc', function( $location, Const, MessageSrvc, MailSrvc, UserSrvc ) {

    var defaults = {
            routes: {
                payment: '#' + Const.routes.payment,
                profile : '#' + Const.routes.profile
            },
           usermeta : UserSrvc.getMeta()
      },
      meta = {};

    this.initMeta = function() {
      angular.forEach( defaults, function( value, key ){
          meta[ key ] = value;
      });
    };

    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

});
