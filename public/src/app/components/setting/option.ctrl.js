app.controller('optionCtrl', function( $scope, OptionSrvc, $rootScope ){

	$scope.meta = OptionSrvc.getMeta();

	$scope.update = function(){
		OptionSrvc.update();
	};

	$scope.reset = function(){
		OptionSrvc.resetMeta();
	};

	console.log( $rootScope );

});
