app.service('OptionSrvc', function(MessageSrvc, Resource, Utils, $rootScope ) {

    var defaults = {
            options: [],
            isGetting: false,
            isUpdating: false
        },
        meta = angular.copy(defaults);

    this.resetMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.getMeta = function() {

        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.get = function() {

        var onGet = function(response) {

                if (200 == response.status) {
                    meta.options = response.data;
                    meta.options.subscription_cost = parseInt( meta.options.subscription_cost || 0 );
                }
                meta.isGetting = false;

            },
            onGetError = function(reason) {
                Utils.log(['Error on Getting Options', reason], 'error');
                meta.isGetting = false;
            };

        meta.isGetting = true;
        return Resource.options().get({}, onGet, onGetError);
    };

    this.getByKey = function(key) {

        return meta.options[key];
    };

    this.update = function() {

        var onUpdate = function(response) {
                var msg = MessageSrvc.get(response.code),
                    type = 'error';

                if (200 == response.status) {
                    type = 'success';
                    $rootScope.optMeta = response.data;
                    /* event trigger when setting is changed */
                    $rootScope.$broadcast( "optionChanged", response.data );
                }

                meta.toast({
                    message: msg,
                    type: type
                });

                meta.isUpdating = false;
            },
            onUpdateError = function(reason) {

                Utils.log(['Error on Updating Options', reason], 'error');
                meta.isUpdating = false;
            };

        meta.isUpdating = true;
        Resource.options().update(meta.options, onUpdate, onUpdateError);
    };

});
