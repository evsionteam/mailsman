app.service('CustomersSrvc', function( Utils, Const, Resource, UserSrvc, $rootScope ) {

    var defaults = {
            usermeta         : UserSrvc.getMeta(),
            ischangingStatus : false,
            isAssigning      : false,
                routes       : {
                    mails: Const.routes.mails
                }
        },
        meta = {};

    this.initMeta = function() {
        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    // initialize meta value
    this.initMeta();

    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

    this.mmaddress = function(){
        var onSuccess = function( response){
                if( 200 === response.status ){
                    meta.mmaddress = response.data;
                }
            },
            onError = function( reason ){
                Utils.log(['Error on fetching unused mmaddress', reason], 'error');
            };
        Resource.mmAddress().get( onSuccess, onError );
    };

    this.updateStatus = function( id, data ){
        var onUpdate = function( response ){
                var type = 'error';
                if( 200 === response.status){
                    var indexOfUser = Utils.findIndex( 'id', id, meta.usermeta.users );
                    meta.usermeta.users[indexOfUser].is_active = data.is_active;
                    type="success";
                }
                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: type,
                    time: 2000,
                    clear: true
                });
                meta.ischangingStatus = false;

            },onError = function( reason ){
                meta.toast({
                    message: $rootScope.txt[reason.code],
                    type: 'error',
                    time: 2000,
                    clear: true
                });
                meta.ischangingStatus = false;
                Utils.log(['Error on Updating customer status', reason], 'error');
            };

        meta.toast({
            message : 'updating Status',
            type : 'loader',
            clear : true,
            time: 20000
        });
        meta.ischangingStatus = true;
        Resource.user().update({id:id}, data, onUpdate, onError);
    };

    this.assign = function( mmaddressId, data ) {
        var onAssign = function( response ) {
                var type = 'error';
                if( 200 === response.status){
                    type = 'success';
                }

                meta.toast({
                    message: $rootScope.txt[response.code],
                    type: type,
                    time: 2000,
                    clear: true
                });
                meta.isAssigning = false;
            },

            onAssignError = function( reason ) {
              meta.toast({
                  message: $rootScope.txt[reason.code],
                  type: 'error',
                  time: 2000,
                  clear: true
              });
              meta.isAssigning = false;
              Utils.log(['Error on Updating customer status', reason], 'error');
            };

        meta.isAssigning = true;
        meta.toast({
            message : 'mmaddress asigning',
            type    : 'loader',
            time    : 600000
        });
        Resource.mmAddress( mmaddressId ).assign( data, onAssign, onAssignError );
    };

});
