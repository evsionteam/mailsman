app.controller('customersCtrl', function($rootScope, $scope, CustomersSrvc, UserSrvc, Utils, MailSrvc, helper) {

    $scope.sweet = {};
    $scope.sweet.option = {
        title: $rootScope.txt.TXT0134,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: $rootScope.txt.TXT0109,
        cancelButtonText: $rootScope.txt.TXT0110,
        closeOnConfirm: true,
        closeOnCancel: true
    };
    $scope.meta = CustomersSrvc.getMeta();
    $scope.mail = MailSrvc.getMeta();

    $scope.get = function(page) {
        UserSrvc.setMeta('page', page);
        UserSrvc.getCustomers().then( function( response ){

            $scope.userId = {};

            angular.forEach( response.data , function( value, index){
                $scope.userId[value.id] = value.mm_id;
            });
        });
    };

    $scope.get(1);
    //getting free mmaddress
    CustomersSrvc.mmaddress();

    $scope.composeInfo = {
        id: null,
        email: null,
        visible: false,
        message: null,
        attachment: null
    };

    $scope.customerId = function(id, email) {
        $scope.composeInfo.id = id;
        $scope.composeInfo.email = email;
        $scope.composeInfo.visible = true;
    };

    $scope.isEmailExist = function(email) {
        var arrindex = Utils.findIndex('email', email, $scope.meta.usermeta.users);
        if (arrindex == -1) {
            $scope.invalidEmail = true;
            return;
        }
        $scope.invalidEmail = false;
        $scope.composeInfo.id = $scope.meta.usermeta.users[arrindex].id;
    };

    $scope.newMail = {
        'userId': null,
        'message': null,
        'attachment': null
    };

    /* search by mmaddress */
    $scope.searchMmaddress = function() {

        UserSrvc.setMeta('page', 1);
        UserSrvc.getCustomers();
    };

    $scope.clear = function() {

        if (!helper.isExist($scope.meta.usermeta.searchKey)) {

            if ($scope.mmaddress_search.$submitted) {
                $scope.get(1);
            }

            $scope.mmaddress_search.$setPristine();
        }
    };

    $scope.compose = function() {

        var index = Utils.findIndex('id', $scope.composeInfo.id, $scope.meta.usermeta.users),
            constomerInfo = $scope.meta.usermeta.users[index],
            data = {
                staff_id: $rootScope.user.id,
                customer_id: constomerInfo.id,
                group_id: 0,
                message: $scope.composeInfo.message,
                status: 'unread',
                charge: 0
                // attachment_id : '',
                // service_id    : '',
            };

        if (helper.isExist($scope.composeInfo.attachment)) {
            data.file = $scope.composeInfo.attachment;
        }

        MailSrvc.compose(data);

        $scope.composeInfo.visible = false;

        $scope.composeInfo.message = '';
        $scope.composeInfo.attachment = null;

        $scope.composeForm.$setPristine();
    };

    $scope.upload = function(file, invalidFiles) {

        $scope.composeInfo.attachment = file;
    };

    //set user status ['active', 'inactive']
    $scope.setStatus = function(id, currentStatus) {

        if ($scope.meta.ischangingStatus) {
            return;
        }
        var data;
        if ('0' === currentStatus) {
            data = {
                is_active: '1'
            };
        } else {
            data = {
                is_active: '0'
            };
        }
        CustomersSrvc.updateStatus(id, data);
    };

    //Assign mmaddress to user
    $scope.assign = function(mmaddressId, userId) {
        if (helper.isExist(mmaddressId)) {
            CustomersSrvc.assign(mmaddressId, {
                id: userId
            });
        }
    };

    //unAssigning mmaddress
    // $scope.unassing = CustomersSrvc.unassign;

});
