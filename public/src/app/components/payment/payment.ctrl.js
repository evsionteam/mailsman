app.controller('paymentCtrl', function($rootScope, $scope, PaymentSrvc, StripeSrvc, Const, Utils, helper, $filter, PaypalPaymentSrvc, $location) {

    $scope.sweet = {};
    $scope.sweet.option = {
        title: $rootScope.txt.TXT0134,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: $rootScope.txt.TXT0135,
        cancelButtonText: $rootScope.txt.TXT0136,
        closeOnConfirm: true,
        closeOnCancel: true
    };

    $scope.meta = PaymentSrvc.getMeta();
    $scope.stripe = StripeSrvc.getMeta();
    $scope.years = StripeSrvc.getExpYears();
    $scope.card = {};
    $scope.subscriptionCost = $filter('mmCurrency')($rootScope.optMeta.subscription_cost);
    var cookie = Utils.getCookie();

    // load stripe form
    $scope.loadStripeForm = function() {
        $scope.template = Const.templatePath + '/load-fund.tpl.html';
        $scope.stripe.isLoadFund = true;
    };
    $scope.loadStripeForm();

    $scope.loadWithPaypal = function() {
        $scope.template = Const.templatePath + '/load-with-paypal.html';
        $scope.stripe.isLoadFund = false;
        $scope.stripe.isSubscribing = false;
    };

    $scope.paypal = PaypalPaymentSrvc.getMeta();

    // Load fund through paypal
    $scope.pay = PaypalPaymentSrvc.loadFund;

    // proccessing paypal payment
    var qs = $location.search();
    if (Object.values(qs).length > 0) {
        qs.paid_for = 'balance_load';
        $scope.template = Const.templatePath + '/loading-fund-with-paypal.tpl.html';
        PaypalPaymentSrvc.proccess(qs);
    }

    $scope.createToken = function(form) {
        StripeSrvc.setMeta('card', $scope.card);
        var error = StripeSrvc.createToken();

        angular.forEach(error, function(value, key) {
            if (value == 'exp_date') {
                form.exp_month.$setValidity('required', false);
                form.exp_year.$setValidity('required', false);
            } else {
                form[value].$setValidity('required', false);
            }
        });

    };

});
