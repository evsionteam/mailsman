app.service('PaymentSrvc', function( StripeSrvc, Const  ) {

    var meta = {
        routes : {
            payment : '#'+ Const.routes.payment
        }
    };

    this.getMeta = function() {
        return meta; 
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

});
