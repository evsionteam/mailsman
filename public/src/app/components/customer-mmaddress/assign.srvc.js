app.service('AssignSrvc', function( $rootScope, Const, Resource, Utils ) {

    var defaults = {
        },
        meta = {};
    
    this.initMeta = function() {

        angular.forEach(defaults, function(value, key) {
            meta[key] = value;
        });
    };

    this.initMeta();
    
    this.getMeta = function() {
        return meta;
    };

    this.setMeta = function(key, value) {
        meta[key] = value;
    };

});
